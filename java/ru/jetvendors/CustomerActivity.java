package ru.jetvendors;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import ru.jetvendors.General.ContactFragment;
import ru.jetvendors.General.HelpFragment;
import ru.jetvendors.General.SettingFragment;
import ru.jetvendors.General.ShareFragment;
import ru.jetvendors.Messages.TalksFragment;
import ru.jetvendors.Notifications.NotificationsFragment;
import ru.jetvendors.Offers.AddMultiFragment;
import ru.jetvendors.Offers.AddSingleFragment;
import ru.jetvendors.Offers.EditFragment;
import ru.jetvendors.Profile.CustomerEditFragment;
import ru.jetvendors.SearchVendors.SearchVendorsFragment;

public class CustomerActivity extends BaseUserActivity
{
    private OnClickListener m_func_edit_profile = new OnClickListener()
    {
        public void onClick(View view)
        {
            CustomerActivity.this.fragmentForward(new CustomerEditFragment());
            ((DrawerLayout) CustomerActivity.this.findViewById(R.id.DrawerLayout)).closeDrawers();
        }
    };
    private OnNavigationItemSelectedListener m_func_on_menu_select_item = new OnNavigationItemSelectedListener()
    {
        public boolean onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.Messages:
                    fragmentForward(new TalksFragment());
                    break;
                case R.id.Notifications:
                    fragmentForward(new NotificationsFragment());
                    break;
                case R.id.OffersAdd:
                    fragmentForward(new AddSingleFragment());
                    break;
                case R.id.OffersMultiAdd:
                    fragmentForward(new AddMultiFragment());
                    break;
                case R.id.OffersOwn:
                    fragmentForward(new EditFragment());
                    break;
                case R.id.Vendors:
                    fragmentForward(new SearchVendorsFragment());
                    break;
                case R.id.Contact:
                    fragmentForward(new ContactFragment());
                    break;
                case R.id.Help:
                    fragmentForward(new HelpFragment());
                    break;
                case R.id.Setting:
                    fragmentForward(new SettingFragment());
                    break;
                case R.id.Share:
                    fragmentForward(new ShareFragment());
                    break;
                case R.id.Logout:
                    logout();
                    break;
                default:
                    return false;
            }
            ((DrawerLayout) CustomerActivity.this.findViewById(R.id.DrawerLayout)).closeDrawers();
            return true;
        }
    };


    protected OnNavigationItemSelectedListener getNavigationListener()
    {
        return this.m_func_on_menu_select_item;
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initView(R.layout.activity_customer);
        getNavigationHeaderView().findViewById(R.id.DrawerProfile).setOnClickListener(m_func_edit_profile);
        getNavigationListener().onNavigationItemSelected(((NavigationView) findViewById(R.id.NavigationView)).getMenu().findItem(R.id.Vendors));
    }
}

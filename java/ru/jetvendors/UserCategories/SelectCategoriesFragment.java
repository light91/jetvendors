package ru.jetvendors.UserCategories;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.R;
import ru.jetvendors.Data.FullCategoriesDB;
import ru.jetvendors.Offers.ViewListFragment;
import ru.jetvendors.Application;
import ru.jetvendors.UserCategories.Libs.CategoriesAdapter;
import ru.jetvendors.UserCategories.Libs.CategoriesListAdapter;
import ru.jetvendors.UserCategories.Libs.RequestManager;
import ru.jetvendors.VendorActivity;
import ru.jetvendors.VendorFirstStartActivity;

public class SelectCategoriesFragment extends Fragment
{
    private AlertDialog m_dialog;
    private OnClickListener m_func_on_add_category = new OnClickListener()
    {
        public void onClick(View view) {
            SelectCategoriesFragment.this.m_dialog.show();
        }
    };
    private OnClickListener m_func_on_done = new OnClickListener()
    {
        public void onClick(View view)
        {
            SelectCategoriesFragment.this.m_request_manager.update_vendor_categories(SelectCategoriesFragment.this.m_on_func_update_vendor_categories);
            if (SelectCategoriesFragment.this.getActivity() instanceof VendorFirstStartActivity) {
                SelectCategoriesFragment.this.startActivity(new Intent(SelectCategoriesFragment.this.getActivity(), VendorActivity.class));
            } else {
                ((BaseFragmentActivity) SelectCategoriesFragment.this.getActivity()).fragmentForward(new ViewListFragment());
            }
        }
    };
    private OnItemClickListener m_func_on_select_category = new OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Application.instance().getUserCats().add((int) id);
            ((CategoriesListAdapter) ((ListView) SelectCategoriesFragment.this.getView().findViewById(R.id.CategoriesList)).getAdapter()).notifyDataSetChanged();
            SelectCategoriesFragment.this.m_dialog.hide();
        }
    };
    private OnUpdateVendorCategories m_on_func_update_vendor_categories = new OnUpdateVendorCategories()
    {
        public void onSuccess()
        {
        }

        public void onError()
        {
        }
    };
    private RequestManager m_request_manager;

    public interface OnUpdateVendorCategories
    {
        void onError();
        void onSuccess();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if (getActivity() instanceof BaseUserActivity) {
            ((BaseUserActivity) getActivity()).setPageTitle("\u041c\u043e\u0438 \u0442\u043e\u0432\u0430\u0440\u044b");
        }
        FullCategoriesDB db = new FullCategoriesDB();
        db.getWritableDatabase();
        db.close();
        this.m_request_manager = new RequestManager();

        View view = inflater.inflate(R.layout.category_select_product_fragment, container, false);
        view.findViewById(R.id.AddCategory).setOnClickListener(this.m_func_on_add_category);
        ((ListView) view.findViewById(R.id.CategoriesList)).setAdapter(new CategoriesListAdapter(view.getContext()));
        Builder dialogBuilder = new Builder(inflater.getContext());
        View dialogView = inflater.inflate(R.layout.category_select_dialog, null);
        ((ListView) dialogView.findViewById(R.id.CategoriesList)).setAdapter(new CategoriesAdapter(inflater.getContext()));
        ((ListView) dialogView.findViewById(R.id.CategoriesList)).setOnItemClickListener(this.m_func_on_select_category);
        dialogBuilder.setView(dialogView);
        this.m_dialog = dialogBuilder.create();
        view.findViewById(R.id.Done).setOnClickListener(this.m_func_on_done);
        return view;
    }
}

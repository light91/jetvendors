package ru.jetvendors.UserCategories;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import ru.jetvendors.Application;
import ru.jetvendors.R;
import ru.jetvendors.Data.FullCategoriesDB;
import ru.jetvendors.Data.FullCategoriesDB.Category;

public class DialogSelectSubCategories extends Builder
{
    private AlertDialog m_dialog_select_category;
    OnItemClickListener m_func_select = new OnItemClickListener()
      {
        public void onItemClick(AdapterView<?> parent, View view, int position, long _id)
        {
            int id = ((AdapterItem) parent.getAdapter().getItem(position)).getId();
            int index = 0;
            while (index != DialogSelectSubCategories.this.m_ids.size() && ((Integer) DialogSelectSubCategories.this.m_ids.get(index)).intValue() != id)
            {
                index++;
            }
            Log.d("myLogs-index", "index " + index);
            if (index == DialogSelectSubCategories.this.m_ids.size())
            {
                DialogSelectSubCategories.this.m_ids.add(Integer.valueOf(id));
                ((TextView) view).setTextColor(DialogSelectSubCategories.this.getContext().getResources().getColor(R.color.item_color_active));
                return;
            }
            DialogSelectSubCategories.this.m_ids.remove(index);
            ((TextView) view).setTextColor(DialogSelectSubCategories.this.getContext().getResources().getColor(R.color.item_color_inactive));
        }
    };
    private List<Integer> m_ids;


    public DialogSelectSubCategories(Context context)
    {
        super(context);
    }

    public AlertDialog getDialog() {
        return this.m_dialog_select_category;
    }

    public List<Integer> getIds() {
        return this.m_ids;
    }

    public void createDialog(int pid, OnClickListener func_done)
    {
        this.m_ids = new ArrayList();
        List<AdapterItem> adapterData = fillData(pid);
        View view = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.category_select_dialog, null);
        view.findViewById(R.id.Done).setVisibility(View.VISIBLE);
        ((ListView) view.findViewById(R.id.CategoriesList)).setAdapter(new ArrayAdapter(getContext(), R.layout.category_dialog_item_2, adapterData));
        ((ListView) view.findViewById(R.id.CategoriesList)).setOnItemClickListener(m_func_select);
        view.findViewById(R.id.Done).setOnClickListener(func_done);
        setView(view);
        this.m_dialog_select_category = create();
    }

    private List<AdapterItem> fillData(int pid)
    {
        FullCategoriesDB db = new FullCategoriesDB();
        List<AdapterItem> adapterData = new ArrayList();
        int count = db.getCount(pid);
        for (int i = 0; i != count; i++) {
            Category category = db.get(i, pid);
            if (Application.instance().getUserCats().getById(category.id) == null) {
                adapterData.add(new AdapterItem(category.id, category.name));
            }
        }
        db.close();
        return adapterData;
    }

    class AdapterItem
    {
        private int m_id;
        private String m_name;

        public AdapterItem(int id, String name)
        {
            m_id = id;
            m_name = name;
        }

        public int getId() {
            return m_id;
        }

        public String toString() {
            return m_name;
        }
    }
}

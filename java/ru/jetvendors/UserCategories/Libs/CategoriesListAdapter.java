package ru.jetvendors.UserCategories.Libs;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;

import ru.jetvendors.Application;
import ru.jetvendors.R;
import ru.jetvendors.Data.FullCategoriesDB.Category;
import ru.jetvendors.UserCategories.DialogSelectSubCategories;

public class CategoriesListAdapter extends BaseAdapter
{
    private Context m_context;
    private DialogSelectSubCategories m_dialog;
    private OnClickListener m_func_on_add_create = new OnClickListener()
    {
        public void onClick(View v)
        {
            int pid = (Integer) v.getTag();
            m_dialog = new DialogSelectSubCategories(m_context);
            m_dialog.createDialog(pid, m_func_on_add_done);
            m_dialog.getDialog().show();
        }
    };
    private OnClickListener m_func_on_add_done = new OnClickListener()
    {
        public void onClick(View v) {
            List<Integer> ids = CategoriesListAdapter.this.m_dialog.getIds();
            for (int index = 0; index != ids.size(); index++) {
                Application.instance().getUserCats().add(((Integer) ids.get(index)).intValue());
            }
            CategoriesListAdapter.this.m_dialog.getDialog().hide();
            CategoriesListAdapter.this.m_dialog = null;
            CategoriesListAdapter.this.notifyDataSetChanged();
        }
    };
    private OnClickListener m_func_on_close_tag =  new OnClickListener()
    {
        public void onClick(View v) {
            Log.d("myLogs-close", "getTag " + ((Integer) v.getTag()).intValue());
            Application.instance().getUserCats().remove(((Integer) v.getTag()).intValue());
            CategoriesListAdapter.this.notifyDataSetChanged();
        }
    };


    public CategoriesListAdapter(Context context)
    {
        m_context = context;
    }

    public int getCount() {
        return Application.instance().getUserCats().getCount();
    }

    public int getSubcategoriesCount(int pid) {
        return Application.instance().getUserCats().getCount(pid);
    }

    public Object getItem(int position) {
        return Application.instance().getUserCats().get(position);
    }

    public Object getItem(int pid, int position) {
        return Application.instance().getUserCats().get(pid, position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View contentView, ViewGroup parent)
    {
        View view = ((LayoutInflater) Application.instance().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.category_select_product_item, parent, false);
        Category category = (Category) getItem(position);
        ((TextView) view.findViewById(R.id.CategoryName)).setText(category.name);
        view.findViewById(R.id.Add).setOnClickListener(this.m_func_on_add_create);
        view.findViewById(R.id.Add).setTag(Integer.valueOf(category.id));
        view.findViewById(R.id.CloseTag).setOnClickListener(this.m_func_on_close_tag);
        view.findViewById(R.id.CloseTag).setTag(Integer.valueOf(category.id));
        ViewGroup listView = (ViewGroup) view.findViewById(R.id.SubcategoryList);
        for (int i = 0; i != getSubcategoriesCount(category.id); i++) {
            Category subCategory = (Category) getItem(category.id, i);
            View subCategoryView = ((LayoutInflater) Application.instance().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.category_sub_select_product_item, null, false);
            ((TextView) subCategoryView.findViewById(R.id.TagName)).setText(subCategory.name);
            subCategoryView.findViewById(R.id.CloseTag).setOnClickListener(this.m_func_on_close_tag);
            subCategoryView.findViewById(R.id.CloseTag).setTag(Integer.valueOf(subCategory.id));
            listView.addView(subCategoryView);
        }
        return view;
    }
}

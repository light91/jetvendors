package ru.jetvendors.UserCategories.Libs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import ru.jetvendors.R;
import ru.jetvendors.Data.FullCategoriesDB;
import ru.jetvendors.Data.FullCategoriesDB.Category;

public class CategoriesAdapter extends BaseAdapter {
    private Context m_context;
    private FullCategoriesDB m_db;
    private int m_pid;

    public CategoriesAdapter(Context context) {
        this(context, -1);
    }

    public CategoriesAdapter(Context context, int pid) {
        this.m_db = new FullCategoriesDB();
        this.m_context = context;
        this.m_pid = pid;
    }

    protected void finalize() throws Throwable {
        this.m_db.close();
        super.finalize();
    }

    public int getCount() {
        return this.m_db.getCount(this.m_pid);
    }

    public long getItemId(int position) {
        return (long) getItem(position).id;
    }

    public Category getItem(int position) {
        return this.m_db.get(position, this.m_pid);
    }

    public View getView(int position, View contentView, ViewGroup parent) {
        View view = contentView;
        if (view == null) {
            view = ((LayoutInflater) this.m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.category_dialog_item, parent, false);
        }
        ((TextView) view.findViewById(R.id.textView1)).setText(getItem(position).name);
        return view;
    }
}

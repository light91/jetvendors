package ru.jetvendors.UserCategories.Libs;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class TableViewGroup extends ViewGroup
{
    private static final int UNEVEN_GRID_PENALTY_MULTIPLIER = 10;
    private int mMaxChildHeight;
    private int mMaxChildWidth;

    public TableViewGroup(Context context) {
        super(context, null);
        this.mMaxChildWidth = 0;
        this.mMaxChildHeight = 0;
    }

    public TableViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        this.mMaxChildWidth = 0;
        this.mMaxChildHeight = 0;
    }

    public TableViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mMaxChildWidth = 0;
        this.mMaxChildHeight = 0;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        this.mMaxChildWidth = 0;
        this.mMaxChildHeight = 0;
        int childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.UNSPECIFIED);
        int childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(heightMeasureSpec), MeasureSpec.UNSPECIFIED);
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE)
            {
                child.measure(widthMeasureSpec, heightMeasureSpec);
                this.mMaxChildWidth += child.getMeasuredWidth();
                if (this.mMaxChildWidth > widthMeasureSpec) {
                    this.mMaxChildWidth = child.getMeasuredWidth();
                    this.mMaxChildHeight += child.getMeasuredHeight();
                }
                Log.d("mylogs-size", " getMeasuredHeight " + child.getMeasuredHeight() + " getMeasuredWidth " + child.getMeasuredWidth());
                Log.d("mylogs-size", " mMaxChildHeight " + this.mMaxChildHeight + " mMaxChildWidth " + this.mMaxChildWidth);
                getChildAt(i).measure(child.getMeasuredWidth(), child.getMeasuredHeight());
            }
        }
        setMeasuredDimension(widthMeasureSpec, resolveSize(this.mMaxChildHeight, heightMeasureSpec));
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int i;
        int width = r - l;
        int height = b - t;
        int count = getChildCount();
        int visibleCount = 0;
        for (i = 0; i < count; i++)
        {
            if (getChildAt(i).getVisibility() != GONE)
            {
                visibleCount++;
            }
        }
        if (visibleCount != 0) {
            int left = 0;
            int top = 0;
            for (i = 0; i < count; i++) {
                View child = getChildAt(i);
                if (child.getVisibility() != GONE)
                {
                    Log.d("mylogs-size-onLayout", " getMeasuredWidth " + child.getMeasuredWidth() + " getMeasuredHeight " + child.getMeasuredHeight());
                    if (child.getMeasuredWidth() + left > width)
                    {
                        left = 0;
                        top += child.getMeasuredHeight();
                    }
                    child.layout(left, top, child.getMeasuredWidth() + left, child.getMeasuredHeight() + top);
                    Log.d("mylogs-size-onLayout", " left " + left + " top " + top);
                    left += child.getMeasuredWidth();
                }
            }
        }
    }
}

package ru.jetvendors.UserCategories.Libs;

import android.util.Log;
import android.util.Pair;

import ru.jetvendors.Application;
import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;
import ru.jetvendors.UserCategories.SelectCategoriesFragment.OnUpdateVendorCategories;

public class RequestManager extends BaseAsyncRequestManager
{
    private static final String host_api = "http://www.qazaqtv.kz/JetVendors/api.php";

    class ResponseObjectUpdateVendorCategories extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
            Log.d("myLogs", "data: " + data());
        }
    }

    public void update_vendor_categories(OnUpdateVendorCategories onUpdateVendorCategories)
    {
        Log.d("myLogs", "update_vendor_categories");
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "update_vendor_categories");
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        int count = Application.instance().getUserCats().getCount();
        int arrayIndex = 0;
        for (int index = 0; index != count; index++)
        {
            int pid = Application.instance().getUserCats().get(index).id;
            int arrayIndex2 = arrayIndex + 1;
            requestData.addData("categories[" + arrayIndex + "]", String.valueOf(pid));
            int subCount = Application.instance().getUserCats().getCount(pid);
            int subIndex = 0;
            arrayIndex = arrayIndex2;
            while (subIndex != subCount)
            {
                arrayIndex2 = arrayIndex + 1;
                requestData.addData("categories[" + arrayIndex + "]", String.valueOf(Application.instance().getUserCats().get(pid, subIndex).id));
                subIndex++;
                arrayIndex = arrayIndex2;
            }
        }
        requestData.setUrl(host_api);
        requestData.setObject(onUpdateVendorCategories);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectUpdateVendorCategories())});
        addRequest(currentRequest);
    }
}

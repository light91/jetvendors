package ru.jetvendors.UserCategories.Libs;

import java.util.List;
import ru.jetvendors.Data.FullCategoriesDB;
import ru.jetvendors.Data.FullCategoriesDB.Category;

public class CategoriesSpecial
{
    public static String getCatsText(List<Integer> cats, String subCatsDivider, String catsDivider, String stringDivider)
    {
        String text = new String();
        FullCategoriesDB db = new FullCategoriesDB();
        for (int index = 0; index != cats.size(); index++)
        {
            Category category = db.getById(cats.get(index));
            if (index > 0)
            {
                if (db.getById(cats.get(index)).pid == -1)
                {
                    text = text + stringDivider;
                }
                else if (db.getById(cats.get(index - 1)).pid == -1)
                {
                    text = text + catsDivider;
                }
                else
                {
                    text = text + subCatsDivider;
                }
            }
            text = text + category.name;
        }
        db.close();
        return text;
    }
}

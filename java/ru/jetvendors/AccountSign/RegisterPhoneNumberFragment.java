package ru.jetvendors.AccountSign;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import ru.jetvendors.AccountSign.Libs.CountriesFlagAdapter;
import ru.jetvendors.AccountSign.Libs.RequestsManager;
import ru.jetvendors.ExternLibs.NotifyOperationComplete;
import ru.jetvendors.R;
import ru.jetvendors.Data.CountriesData;
import ru.jetvendors.Data.CountriesData.Country;
import ru.jetvendors.StartActivity;

public class RegisterPhoneNumberFragment extends Fragment
{
    private OnClickListener m_func_next_listener = new OnClickListener()
    {
        public void onClick(View view) {
            String phoneNumber = ((EditText) getView().findViewById(R.id.phoneNumber)).getText().toString();
            Spinner spinner = (Spinner) getView().findViewById(R.id.Country);
            getArguments().putInt("cid", ((Country) spinner.getAdapter().getItem(spinner.getSelectedItemPosition())).id);
            getArguments().putString("phone", phoneNumber);
            RequestsManager.instance().check_phone_number(phoneNumber, m_func_on_phone_number_check);
            Toast.makeText(getView().getContext(), "\u043f\u0440\u043e\u0432\u0435\u0440\u043a\u0430 \u043d\u043e\u043c\u0435\u0440\u0430 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430..", 0).show();
        }
    };
    private NotifyOperationComplete m_func_on_phone_number_check = new NotifyOperationComplete()
    {
        @Override
        public void onNotify(Object result)
        {
            if ((boolean) result)
            {
                Fragment fragment = new RegisterSmsFragment();
                fragment.setArguments(getArguments());
                ((StartActivity) getActivity()).fragmentForward(fragment);
            }
            else
            {
                Toast.makeText(getView().getContext(), "\u0434\u0430\u043d\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430 \u0443\u0436\u0435 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f..", 1).show();
            }
        }
    };


    public interface OnPhoneNumberCheck
    {
        void onInvalidNumber();
        void onValidNumber();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.account_sign_phone_number_fragment, container, false);
        ((Spinner) view.findViewById(R.id.Country)).setAdapter(new CountriesFlagAdapter(inflater.getContext(), new CountriesData()));
        view.findViewById(R.id.Next).setOnClickListener(this.m_func_next_listener);
        return view;
    }
}

package ru.jetvendors.AccountSign.Libs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import ru.jetvendors.R;
import ru.jetvendors.Data.CitiesData;
import ru.jetvendors.Data.CitiesData.City;

public class CitiesAdapter extends BaseAdapter {
    private int m_cid;
    private Context m_context;
    private CitiesData m_data;

    public CitiesAdapter(Context context, CitiesData data, int cid) {
        this.m_context = context;
        this.m_data = data;
        this.m_cid = cid;
    }

    public int getCount() {
        return this.m_data.getCountByCid(this.m_cid);
    }

    public City getItem(int i) {
        return this.m_data.getById(this.m_cid, i);
    }

    public long getItemId(int i) {
        return (long) getItem(i).id;
    }

    public View getView(int position, View contentView, ViewGroup parent) {
        return getView(contentView, parent, getItem(position).name);
    }

    private View getView(View contentView, ViewGroup parent, String text) {
        View view = contentView;
        if (view == null) {
            Context context = this.m_context;
            Context context2 = this.m_context;
            view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listex_item, parent, false);
        }
        ((TextView) view.findViewById(R.id.name)).setText(text);
        return view;
    }

    protected Context getContext() {
        return this.m_context;
    }
}

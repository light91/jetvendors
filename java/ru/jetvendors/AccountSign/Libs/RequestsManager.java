package ru.jetvendors.AccountSign.Libs;

import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import com.vk.sdk.api.model.VKScopes;
import ru.jetvendors.AccountSign.RecoveryPasswordFragment.OnRecoveryPassword;
import ru.jetvendors.AccountSign.RegisterCustomerFragment.OnRegisterResult;
import ru.jetvendors.AccountSign.RegisterPhoneNumberFragment.OnPhoneNumberCheck;
import ru.jetvendors.AccountSign.RegisterSmsFragment.OnSmsCodeCheck;
import ru.jetvendors.AccountSign.RegisterSmsFragment.OnSmsCodeRefresh;
import ru.jetvendors.AccountSign.RegisterSmsFragment.OnSmsCodeSend;
import ru.jetvendors.AccountSign.SignInFragment.OnLoginListener;
import ru.jetvendors.Application;
import ru.jetvendors.BuildConfig;
import ru.jetvendors.Application.AccountType;
import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;
import ru.jetvendors.ExternLibs.NotifyOperationComplete;

public class RequestsManager extends BaseAsyncRequestManager {
    private static final String host_api = "http://www.qazaqtv.kz/JetVendors/api.php";
    private static RequestsManager m_instance;

    class ResponseObjectCheckCode extends ResponseObject {
        ResponseObjectCheckCode() {
            super();
        }

        public void doResponse(Request requestData) {
            OnSmsCodeCheck onSmsCodeCheck = (OnSmsCodeCheck) requestData.getObject();
            if (data().equals("1")) {
                onSmsCodeCheck.onValid();
            } else {
                onSmsCodeCheck.onInvalid();
            }
        }
    }

    class ResponseObjectCheckPhoneNumber extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
            NotifyOperationComplete onPhoneNumberCheck = (NotifyOperationComplete) requestData.getObject();
            boolean success = data().equals("1");
            onPhoneNumberCheck.onNotify(success);
        }
    }

    class ResponseObjectLogin extends ResponseObject {
        ResponseObjectLogin() {
            super();
        }

        public void doResponse(Request requestData) {
            Log.d("myLogs", "ResponseObjectLogin  account " + data());
            OnLoginListener onRegisterResult = (OnLoginListener) requestData.getObject();
            JsonData jsonListData = (JsonData) getDecodeData().getObject().get(0);
            if (jsonListData.getObject() == null) {
                onRegisterResult.onError();
                return;
            }
            Application.instance().onLogin(jsonListData);
            Log.d("myLogs", "ResponseObjectLogin  onSuccess");
            onRegisterResult.onSuccess();
        }
    }

    class ResponseObjectRecoveryPassword extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
            NotifyOperationComplete onRecoveryPassword = (NotifyOperationComplete) requestData.getObject();
            boolean success = data().equals("1");
            onRecoveryPassword.onNotify(success);
        }
    }

    class ResponseObjectRefreshCode extends ResponseObject {
        ResponseObjectRefreshCode() {
            super();
        }

        public void doResponse(Request requestData) {
            OnSmsCodeRefresh onSmsCodeRefresh = (OnSmsCodeRefresh) requestData.getObject();
            if (data().equals("0")) {
                onSmsCodeRefresh.onSend();
            } else {
                onSmsCodeRefresh.onError(data());
            }
        }
    }

    class ResponseObjectRegister extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
            NotifyOperationComplete onRegisterResult = (NotifyOperationComplete) requestData.getObject();
            boolean success = data().equals("1");
            onRegisterResult.onNotify(success);
        }
    }

    class ResponseObjectRequestCode extends ResponseObject {
        ResponseObjectRequestCode() {
            super();
        }

        public void doResponse(Request requestData) {
            OnSmsCodeSend onSmsCodeSend = (OnSmsCodeSend) requestData.getObject();
            if (data().equals("0")) {
                onSmsCodeSend.onSend();
            } else {
                onSmsCodeSend.onError(data());
            }
        }
    }

    public static RequestsManager instance() {
        if (m_instance == null) {
            m_instance = new RequestsManager();
        }
        return m_instance;
    }

    private RequestsManager() {
    }

    public void check_phone_number(String phone, NotifyOperationComplete onPhoneNumberCheck) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "check_phone_number");
        requestData.addData("phone", phone);
        requestData.setUrl(host_api);
        requestData.setObject(onPhoneNumberCheck);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectCheckPhoneNumber())});
        addRequest(currentRequest);
    }

    public void request_code(String phone, OnSmsCodeSend onSmsCodeSend) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "request_code");
        requestData.addData("phone", phone);
        requestData.setUrl(host_api);
        requestData.setObject(onSmsCodeSend);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectRequestCode())});
        addRequest(currentRequest);
    }

    public void refresh_code(String phone, OnSmsCodeRefresh onSmsCodeRefresh) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "refresh_code");
        requestData.addData("phone", phone);
        requestData.setUrl(host_api);
        requestData.setObject(onSmsCodeRefresh);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectRefreshCode())});
        addRequest(currentRequest);
    }

    public void check_code(String phone, String code, OnSmsCodeCheck onSmsCodeCheck) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "check_code");
        requestData.addData("phone", phone);
        requestData.addData("code", code);
        requestData.setUrl(host_api);
        requestData.setObject(onSmsCodeCheck);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectCheckCode())});
        addRequest(currentRequest);
    }

    public void register(Bundle bundle, NotifyOperationComplete onRegisterResult)
    {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "register");
        requestData.addData("accountType", String.valueOf(bundle.getInt("accountType")));
        requestData.addData("code", bundle.getString("code"));
        requestData.addData("cid", String.valueOf(bundle.getInt("cid")));
        requestData.addData("tid", bundle.getString("tid"));
        requestData.addData("phone", bundle.getString("phone"));
        requestData.addData("name", bundle.getString("name"));
        requestData.addData("family", bundle.getString("family"));
        requestData.addData(VKScopes.EMAIL, bundle.getString(VKScopes.EMAIL));
        requestData.addData("password", bundle.getString("password"));
        if (bundle.getInt("accountType") == AccountType.customer.ordinal())
            requestData.addData("company_name", bundle.getString("company_name"));

        requestData.setUrl(host_api);
        requestData.setObject(onRegisterResult);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectRegister())});
        addRequest(currentRequest);
    }

    public void login(String phone, String password, int cid, OnLoginListener onLoginListener) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "login");
        requestData.addData("phone", phone);
        requestData.addData("password", password);
        requestData.addData("cid", String.valueOf(cid));
        requestData.setUrl(host_api);
        requestData.setObject(onLoginListener);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectLogin())});
        addRequest(currentRequest);
    }

    public void recovery_password(String phone, NotifyOperationComplete onRecoveryPassword) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "recovery_password");
        requestData.addData("phone", phone);
        requestData.setUrl(host_api);
        requestData.setObject(onRecoveryPassword);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectRecoveryPassword())});
        addRequest(currentRequest);
    }
}

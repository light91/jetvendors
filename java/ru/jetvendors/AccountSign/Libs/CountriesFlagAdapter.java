package ru.jetvendors.AccountSign.Libs;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import ru.jetvendors.R;
import ru.jetvendors.Data.CountriesData;
import ru.jetvendors.Data.CountriesData.Country;

public class CountriesFlagAdapter extends BaseAdapter {
    private Context m_context;
    private CountriesData m_data;

    public CountriesFlagAdapter(Context context, CountriesData data) {
        this.m_context = context;
        this.m_data = data;
    }

    public int getCount() {
        return this.m_data.getCount();
    }

    public Country getItem(int i) {
        return this.m_data.getCountry(i);
    }

    public long getItemId(int i) {
        return (long) getItem(i).id;
    }

    public View getView(int position, View contentView, ViewGroup parent) {
        Country country = getItem(position);
        return getView(contentView, parent, country.previewRes, country.number);
    }

    public View getDropDownView(int position, View contentView, ViewGroup parent) {
        Country country = getItem(position);
        return getView(contentView, parent, country.previewRes, country.name);
    }

    private View getView(View contentView, ViewGroup parent, int bitmapRes, String text) {
        View view = contentView;
        if (view == null) {
            Context context = this.m_context;
            Context context2 = this.m_context;
            view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.adapter_country_flag_item, parent, false);
        }
        ((TextView) view.findViewById(R.id.name)).setText(text);
        ((ImageView) view.findViewById(R.id.flag)).setImageBitmap(BitmapFactory.decodeResource(this.m_context.getResources(), bitmapRes));
        return view;
    }

    protected Context getContext() {
        return this.m_context;
    }
}

package ru.jetvendors.AccountSign.Libs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import ru.jetvendors.R;
import ru.jetvendors.Data.KindOfBisnessData;
import ru.jetvendors.Data.KindOfBisnessData.KindOfBisness;

public class KindOfBisnessAdapter extends BaseAdapter {
    private Context m_context;
    private KindOfBisnessData m_data;

    public KindOfBisnessAdapter(Context context, KindOfBisnessData data) {
        this.m_context = context;
        this.m_data = data;
    }

    public int getCount() {
        return this.m_data.getCount();
    }

    public Object getItem(int i) {
        return this.m_data.getItem(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int position, View contentView, ViewGroup viewGroup) {
        KindOfBisness bisness = (KindOfBisness) getItem(position);
        View view = contentView;
        if (view == null) {
            Context context = getContext();
            getContext();
            view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listex_item, viewGroup, false);
        }
        ((TextView) view.findViewById(R.id.name)).setText(bisness.name);
        return view;
    }

    public View getDropDownView(int position, View contentView, ViewGroup viewGroup) {
        KindOfBisness bisness = (KindOfBisness) getItem(position);
        View view = contentView;
        if (view == null) {
            Context context = getContext();
            getContext();
            view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listex_item, viewGroup, false);
        }
        ((TextView) view.findViewById(R.id.name)).setText(bisness.name);
        return view;
    }

    protected Context getContext() {
        return this.m_context;
    }
}

package ru.jetvendors.AccountSign.Libs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import ru.jetvendors.R;
import ru.jetvendors.Data.CountriesData;
import ru.jetvendors.Data.CountriesData.Country;

public class CountriesAdapter extends CountriesFlagAdapter {
    public CountriesAdapter(Context context, CountriesData data) {
        super(context, data);
    }

    public View getView(int position, View contentView, ViewGroup viewGroup) {
        Country country = getItem(position);
        View view = contentView;
        if (view == null) {
            Context context = getContext();
            getContext();
            view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listex_item, viewGroup, false);
        }
        ((TextView) view.findViewById(R.id.name)).setText(country.name);
        return view;
    }
}

package ru.jetvendors.AccountSign;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import ru.jetvendors.AccountSign.Libs.CountriesFlagAdapter;
import ru.jetvendors.AccountSign.Libs.RequestsManager;
import ru.jetvendors.Application;
import ru.jetvendors.R;
import ru.jetvendors.CustomerActivity;
import ru.jetvendors.Data.CountriesData;
import ru.jetvendors.Application.AccountType;
import ru.jetvendors.StartActivity;
import ru.jetvendors.VendorActivity;
import ru.jetvendors.VendorFirstStartActivity;

public class SignInFragment extends Fragment {
    private OnClickListener m_func_on_click_login_listener;
    private OnClickListener m_func_on_click_password_recovery_listener;
    private OnLoginListener m_func_on_login_listener;

    /* renamed from: ru.jetvendors.AccountSign.SignInFragment.1 */
    class C03051 implements OnClickListener {
        C03051() {
        }

        public void onClick(View v) {
            Spinner spinner = (Spinner) SignInFragment.this.getView().findViewById(R.id.Country);
            RequestsManager.instance().login(((EditText) SignInFragment.this.getView().findViewById(R.id.PhoneNumber)).getText().toString(), ((EditText) SignInFragment.this.getView().findViewById(R.id.Password)).getText().toString(), (int) ((Spinner) SignInFragment.this.getView().findViewById(R.id.Country)).getSelectedItemId(), SignInFragment.this.m_func_on_login_listener);
        }
    }

    /* renamed from: ru.jetvendors.AccountSign.SignInFragment.2 */
    class C03062 implements OnClickListener {
        C03062() {
        }

        public void onClick(View v) {
            ((StartActivity) SignInFragment.this.getActivity()).fragmentForward(new RecoveryPasswordFragment());
        }
    }

    public interface OnLoginListener {
        void onError();

        void onSuccess();
    }

    /* renamed from: ru.jetvendors.AccountSign.SignInFragment.3 */
    class C05363 implements OnLoginListener {
        C05363() {
        }

        public void onSuccess() {
            Class runActivityClass;
            SignInFragment.this.getView().findViewById(R.id.ErrorList).setVisibility(8);
            int customerValue = AccountType.customer.ordinal();
            if (Application.instance().getUser().getAccountType() == AccountType.customer) {
                runActivityClass = CustomerActivity.class;
            } else {
                runActivityClass = Application.instance().getUserCats().getCount() != 0 ? VendorActivity.class : VendorFirstStartActivity.class;
            }
            SignInFragment.this.startActivity(new Intent(SignInFragment.this.getActivity(), runActivityClass));
        }

        public void onError() {
            SignInFragment.this.getView().findViewById(R.id.ErrorList).setVisibility(0);
        }
    }

    public SignInFragment() {
        this.m_func_on_click_login_listener = new C03051();
        this.m_func_on_click_password_recovery_listener = new C03062();
        this.m_func_on_login_listener = new C05363();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Application.instance().init(inflater.getContext());
        View view = inflater.inflate(R.layout.account_sign_in_fragment, container, false);
        ((Spinner) view.findViewById(R.id.Country)).setAdapter(new CountriesFlagAdapter(inflater.getContext(), new CountriesData()));
        view.findViewById(R.id.SignIn).setOnClickListener(this.m_func_on_click_login_listener);
        view.findViewById(R.id.ForgodPassword).setOnClickListener(this.m_func_on_click_password_recovery_listener);
        return view;
    }
}

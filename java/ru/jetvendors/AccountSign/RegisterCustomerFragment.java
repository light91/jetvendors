package ru.jetvendors.AccountSign;

import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.vk.sdk.api.model.VKScopes;

import ru.jetvendors.AccountSign.Libs.CitiesAdapter;
import ru.jetvendors.AccountSign.Libs.RequestsManager;
import ru.jetvendors.R;
import ru.jetvendors.Data.CitiesData;
import ru.jetvendors.Data.CitiesData.City;
import ru.jetvendors.StartActivity;

public class RegisterCustomerFragment extends BaseRegisterPerson
{
    public interface OnRegisterResult {
        void onError(String str);

        void onSuccess();
    }

    protected Bundle writeInput()
    {
        Bundle bundle = super.writeInput();
        bundle.putString("company_name", ((EditText) getView().findViewById(R.id.CompanyName)).getText().toString());
        return bundle;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.findViewById(R.id.CompanyNameLayout).setVisibility(View.VISIBLE);
        return view;
    }
}

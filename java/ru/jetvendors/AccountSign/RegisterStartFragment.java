package ru.jetvendors.AccountSign;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import ru.jetvendors.R;
import ru.jetvendors.Application.AccountType;
import ru.jetvendors.StartActivity;

public class RegisterStartFragment extends Fragment {
    private OnClickListener m_func_customer_listener;
    private OnClickListener m_func_provider_listener;

    /* renamed from: ru.jetvendors.AccountSign.RegisterStartFragment.1 */
    class C03031 implements OnClickListener {
        C03031() {
        }

        public void onClick(View view) {
            Bundle bundle = new Bundle();
            bundle.putInt("accountType", AccountType.provider.ordinal());
            Fragment fragment = new RegisterPhoneNumberFragment();
            fragment.setArguments(bundle);
            ((StartActivity) RegisterStartFragment.this.getActivity()).fragmentForward(fragment);
        }
    }

    /* renamed from: ru.jetvendors.AccountSign.RegisterStartFragment.2 */
    class C03042 implements OnClickListener {
        C03042() {
        }

        public void onClick(View view) {
            Bundle bundle = new Bundle();
            bundle.putInt("accountType", AccountType.customer.ordinal());
            Fragment fragment = new RegisterPhoneNumberFragment();
            fragment.setArguments(bundle);
            ((StartActivity) RegisterStartFragment.this.getActivity()).fragmentForward(fragment);
        }
    }

    public RegisterStartFragment() {
        this.m_func_provider_listener = new C03031();
        this.m_func_customer_listener = new C03042();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_sign_register_start_fragment, container, false);
        view.findViewById(R.id.Provider).setOnClickListener(this.m_func_provider_listener);
        view.findViewById(R.id.Customer).setOnClickListener(this.m_func_customer_listener);
        return view;
    }
}

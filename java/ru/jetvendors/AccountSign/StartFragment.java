package ru.jetvendors.AccountSign;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import ru.jetvendors.R;
import ru.jetvendors.StartActivity;

public class StartFragment extends Fragment
{
    private OnClickListener m_func_signin_listener = new OnClickListener()
    {
        public void onClick(View view)
        {
            ((StartActivity) StartFragment.this.getActivity()).fragmentForward(new SignInFragment());
        }
    };
    private OnClickListener m_func_signup_listener = new OnClickListener()
    {
        public void onClick(View view)
        {
            ((StartActivity) StartFragment.this.getActivity()).fragmentForward(new RegisterStartFragment());
        }
    };


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.account_sign_start_fragment, container, false);
        view.findViewById(R.id.SignIn).setOnClickListener(m_func_signin_listener);
        view.findViewById(R.id.SignUp).setOnClickListener(m_func_signup_listener);
        return view;
    }
}

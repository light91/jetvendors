package ru.jetvendors.AccountSign;

import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.vk.sdk.api.model.VKScopes;

import ru.jetvendors.AccountSign.Libs.CitiesAdapter;
import ru.jetvendors.AccountSign.Libs.RequestsManager;
import ru.jetvendors.Application;
import ru.jetvendors.Data.CitiesData;
import ru.jetvendors.ExternLibs.NotifyOperationComplete;
import ru.jetvendors.R;
import ru.jetvendors.StartActivity;

/**
 * Created by Light on 30.07.2016.
 */
public class BaseRegisterPerson extends Fragment
{
    private View.OnClickListener m_func_on_next_listener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (checkValidInputData())
            {
                Bundle bundle = writeInput();
                RequestsManager.instance().register(bundle, m_func_on_register_result);
            }
        }
    };
    private View.OnClickListener m_func_on_read_specification_listener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            new AlertDialog.Builder(getView().getContext()).setTitle(R.string.UserSpecificationTitle).setMessage(R.string.UserSpecificationMessage);
        }
    };
    public NotifyOperationComplete m_func_on_register_result = new NotifyOperationComplete()
    {
        @Override
        public void onNotify(Object result)
        {
            if ((boolean) result)
                ((StartActivity) getActivity()).fragmentForward(new SignInFragment());
        }
    };

    protected Bundle writeInput()
    {
        Spinner townView = (Spinner) getView().findViewById(R.id.Town);
        CitiesData.City citiesData = (CitiesData.City) townView.getAdapter().getItem(townView.getSelectedItemPosition());
        getArguments().putString(VKScopes.EMAIL, ((EditText) getView().findViewById(R.id.Email)).getText().toString());
        getArguments().putString("family", ((EditText) getView().findViewById(R.id.Family)).getText().toString());
        getArguments().putString("name", ((EditText) getView().findViewById(R.id.Name)).getText().toString());
        getArguments().putString("password", ((EditText) getView().findViewById(R.id.Password)).getText().toString());
        getArguments().putString("tid", String.valueOf(citiesData.id));
        return getArguments();
    }


    private void hideValidationMessage()
    {
        getView().findViewById(R.id.CompanyNameValidate).setVisibility(View.GONE);
        getView().findViewById(R.id.NameValidate).setVisibility(View.GONE);
        getView().findViewById(R.id.FamilyValidate).setVisibility(View.GONE);
        getView().findViewById(R.id.EmailValidate).setVisibility(View.GONE);
        getView().findViewById(R.id.PasswordValidate).setVisibility(View.GONE);
        getView().findViewById(R.id.PasswordCheckValidate).setVisibility(View.GONE);
        getView().findViewById(R.id.PasswordCheckMatchValidate).setVisibility(View.GONE);
        getView().findViewById(R.id.UserSpecificationValidate).setVisibility(View.GONE);
    }


    private boolean checkValidInputData()
    {
        boolean validate = true;

        hideValidationMessage();
        if (getView().findViewById(R.id.CompanyNameLayout).getVisibility() == View.VISIBLE)
        {
            if (((EditText) getView().findViewById(R.id.CompanyName)).getText().length() == 0)
            {
                getView().findViewById(R.id.CompanyNameValidate).setVisibility(View.VISIBLE);
                validate = false;
            }
        }

        if (((EditText) getView().findViewById(R.id.Name)).getText().length() == 0)
        {
            getView().findViewById(R.id.NameValidate).setVisibility(View.VISIBLE);
            validate = false;
        }
        if (((EditText) getView().findViewById(R.id.Family)).getText().length() == 0)
        {
            getView().findViewById(R.id.FamilyValidate).setVisibility(View.VISIBLE);
            validate = false;
        }
        if (((EditText) getView().findViewById(R.id.Email)).getText().length() == 0)
        {
            getView().findViewById(R.id.EmailValidate).setVisibility(View.VISIBLE);
            validate = false;
        }
        if (((EditText) getView().findViewById(R.id.Password)).getText().length() == 0)
        {
            getView().findViewById(R.id.PasswordValidate).setVisibility(View.VISIBLE);
            validate = false;
        }
        if (((EditText) getView().findViewById(R.id.PasswordCheck)).getText().length() == 0)
        {
            getView().findViewById(R.id.PasswordCheckValidate).setVisibility(View.VISIBLE);
            validate = false;
        }
        String passwordFirst = ((EditText) getView().findViewById(R.id.Password)).getText().toString();
        if (!passwordFirst.equals(((EditText) getView().findViewById(R.id.PasswordCheck)).getText().toString()))
        {
            getView().findViewById(R.id.PasswordCheckMatchValidate).setVisibility(View.VISIBLE);
            validate = false;
        }
        if (!((CheckBox) getView().findViewById(R.id.UserSpecification)).isChecked())
        {
            getView().findViewById(R.id.UserSpecificationValidate).setVisibility(View.VISIBLE);
            validate = false;
        }
        return validate;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.account_sign_register_personal_fragment, container, false);
        ((TextView) view.findViewById(R.id.ReadUserSpecification)).setText(Html.fromHtml(getString(R.string.ReadUserSpecification)));
        ((Spinner) view.findViewById(R.id.Town)).setAdapter(new CitiesAdapter(inflater.getContext(), new CitiesData(), getArguments().getInt("cid")));
        view.findViewById(R.id.ReadUserSpecification).setOnClickListener(m_func_on_read_specification_listener);
        view.findViewById(R.id.Next).setOnClickListener(m_func_on_next_listener);
        return view;
    }

    public static BaseRegisterPerson getInstance(Bundle bundle)
    {
        BaseRegisterPerson fragment;
        if (bundle.getInt("accountType") == Application.AccountType.customer.ordinal())
            fragment = new RegisterCustomerFragment();
        else
            fragment = new RegisterProviderFragment();

        fragment.setArguments(bundle);
        return fragment;
    }
}

package ru.jetvendors.AccountSign;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import ru.jetvendors.AccountSign.Libs.RequestsManager;
import ru.jetvendors.ExternLibs.NotifyOperationComplete;
import ru.jetvendors.R;
import ru.jetvendors.StartActivity;

public class RecoveryPasswordFragment extends Fragment
{
    private NotifyOperationComplete m_func_on_recovery_password = new NotifyOperationComplete()
    {
        @Override
        public void onNotify(Object result)
        {
            if ((boolean) result)
            {
                Toast.makeText(RecoveryPasswordFragment.this.getView().getContext(), "\u041d\u043e\u0432\u044b\u0439 \u043f\u0430\u0440\u043e\u043b\u044c \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d \u043d\u0430 \u0443\u043a\u0430\u0437\u0430\u043d\u043d\u044b\u0439 \u043d\u043e\u043c\u0435\u0440", 1).show();
                ((StartActivity) RecoveryPasswordFragment.this.getActivity()).fragmentForward(new SignInFragment());
            }
            else
            {
                Toast.makeText(RecoveryPasswordFragment.this.getView().getContext(), "\u0414\u0430\u043d\u043d\u044b\u0439 \u0442\u0435\u043b\u0435\u0444\u043e\u043d \u043d\u0435 \u0437\u0430\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0438\u0440\u043e\u0432\u0430\u043d", 1).show();
            }
        }
    };
    private OnClickListener m_func_send_listener = new OnClickListener()
    {
        public void onClick(View view)
        {
            RequestsManager.instance().recovery_password(((EditText) getView().findViewById(R.id.PhoneNumber)).getText().toString(), m_func_on_recovery_password);
        }
    };

    public interface OnRecoveryPassword
    {
        void onError();
        void onSuccess();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.account_sign_recovery_password_fragment, container, false);
        view.findViewById(R.id.Send).setOnClickListener(m_func_send_listener);
        return view;
    }
}

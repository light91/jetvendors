package ru.jetvendors.AccountSign;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import ru.jetvendors.AccountSign.Libs.RequestsManager;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.R;
import ru.jetvendors.Application.AccountType;
import ru.jetvendors.StartActivity;

public class RegisterSmsFragment extends Fragment {
    private OnClickListener m_func_change_listener;
    private OnClickListener m_func_check_listener;
    private OnSmsCodeCheck m_func_on_sms_code_check;
    private OnSmsCodeRefresh m_func_on_sms_code_refresh;
    private OnSmsCodeSend m_func_on_sms_code_send;
    private OnClickListener m_func_retry_listener;

    /* renamed from: ru.jetvendors.AccountSign.RegisterSmsFragment.4 */
    class C03004 implements OnClickListener {
        C03004() {
        }

        public void onClick(View view) {
            ((StartActivity) RegisterSmsFragment.this.getActivity()).fragmentBack();
        }
    }

    /* renamed from: ru.jetvendors.AccountSign.RegisterSmsFragment.5 */
    class C03015 implements OnClickListener {
        C03015() {
        }

        public void onClick(View view) {
            RequestsManager.instance().refresh_code(RegisterSmsFragment.this.getArguments().getString("phone"), RegisterSmsFragment.this.m_func_on_sms_code_refresh);
        }
    }

    /* renamed from: ru.jetvendors.AccountSign.RegisterSmsFragment.6 */
    class C03026 implements OnClickListener {
        C03026() {
        }

        public void onClick(View view) {
            RequestsManager.instance().check_code(RegisterSmsFragment.this.getArguments().getString("phone"), ((EditText) RegisterSmsFragment.this.getView().findViewById(R.id.SmsCode)).getText().toString(), RegisterSmsFragment.this.m_func_on_sms_code_check);
        }
    }

    public interface OnSmsCodeCheck {
        void onInvalid();
        void onValid();
    }

    public interface OnSmsCodeRefresh {
        void onError(String str);
        void onSend();
    }

    public interface OnSmsCodeSend {
        void onError(String str);
        void onSend();
    }

    /* renamed from: ru.jetvendors.AccountSign.RegisterSmsFragment.1 */
    class C05331 implements OnSmsCodeCheck {
        C05331() {
        }

        public void onValid()
        {
            getArguments().putString("code", ((EditText) RegisterSmsFragment.this.getView().findViewById(R.id.SmsCode)).getText().toString());

            Fragment fragment;
            fragment = BaseRegisterPerson.getInstance(getArguments());
            ((BaseFragmentActivity) getActivity()).fragmentForward(fragment);
        }

        public void onInvalid() {
            Toast.makeText(RegisterSmsFragment.this.getView().getContext(), "\u0432\u044b \u0432\u0432\u0435\u043b\u0438 \u043d\u0435\u0432\u0435\u0440\u043d\u044b\u0439 \u043a\u043e\u0434, \u043f\u043e\u0432\u0442\u043e\u0440\u0438\u0442\u0435 \u043f\u043e\u043f\u044b\u0442\u043a\u0443..", 1).show();
        }
    }

    /* renamed from: ru.jetvendors.AccountSign.RegisterSmsFragment.2 */
    class C05342 implements OnSmsCodeSend {
        C05342() {
        }

        public void onSend() {
            Toast.makeText(RegisterSmsFragment.this.getView().getContext(), "\u041f\u0440\u043e\u0432\u0435\u0440\u043e\u0447\u043d\u044b\u0439 \u043a\u043e\u0434 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d", 1);
        }

        public void onError(String error) {
            Toast.makeText(RegisterSmsFragment.this.getView().getContext(), "\u041e\u0448\u0438\u0431\u043a\u0430 \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0438 \u043f\u0440\u043e\u0432\u0435\u0440\u043e\u0447\u043d\u043e\u0433\u043e \u043a\u043e\u0434\u0430", 1);
        }
    }

    /* renamed from: ru.jetvendors.AccountSign.RegisterSmsFragment.3 */
    class C05353 implements OnSmsCodeRefresh {
        C05353() {
        }

        public void onSend() {
            Toast.makeText(RegisterSmsFragment.this.getView().getContext(), "\u041d\u043e\u0432\u044b\u0439 \u043f\u0440\u043e\u0432\u0435\u0440\u043e\u0447\u043d\u044b\u0439 \u043a\u043e\u0434 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d", 1);
        }

        public void onError(String error) {
            Toast.makeText(RegisterSmsFragment.this.getView().getContext(), "\u041e\u0448\u0438\u0431\u043a\u0430 \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0438 \u043f\u0440\u043e\u0432\u0435\u0440\u043e\u0447\u043d\u043e\u0433\u043e \u043a\u043e\u0434\u0430", 1);
        }
    }

    public RegisterSmsFragment() {
        this.m_func_on_sms_code_check = new C05331();
        this.m_func_on_sms_code_send = new C05342();
        this.m_func_on_sms_code_refresh = new C05353();
        this.m_func_change_listener = new C03004();
        this.m_func_retry_listener = new C03015();
        this.m_func_check_listener = new C03026();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_sign_sms_fragment, container, false);
        view.findViewById(R.id.Change).setOnClickListener(this.m_func_change_listener);
        view.findViewById(R.id.Retry).setOnClickListener(this.m_func_retry_listener);
        view.findViewById(R.id.Check).setOnClickListener(this.m_func_check_listener);
        RequestsManager.instance().request_code(getArguments().getString("phone"), this.m_func_on_sms_code_send);
        return view;
    }
}

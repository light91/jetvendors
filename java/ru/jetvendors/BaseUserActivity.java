package ru.jetvendors;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.ExternLibs.NotifyDataCountChangedListener;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.Messages.Libs.MessagesManager;
import ru.jetvendors.Messages.TalksFragment;
import ru.jetvendors.Notifications.Libs.EventsManager;
import ru.jetvendors.Notifications.NotificationsFragment;

public abstract class BaseUserActivity extends BaseFragmentActivity
{
    private OnClickListener m_func_click_messages = new  OnClickListener()
    {
        public void onClick(View v) {
            BaseUserActivity.this.fragmentForward(new TalksFragment());
        }
    };
    private OnClickListener m_func_click_notices = new OnClickListener()
    {
        public void onClick(View v)
        {
            BaseUserActivity.this.fragmentForward(new NotificationsFragment());
        }
    };
    private NotifyDataCountChangedListener m_func_notify_messages_count_changed = new NotifyDataCountChangedListener()
    {
        public void onDataCountChanged(int count)
        {
            Log.d("myLogs-notify", "m_func_notify_messages_count_changed");
            ((TextView) BaseUserActivity.this.findViewById(R.id.MessageCount)).setText(String.valueOf(count));
            BaseUserActivity.this.findViewById(R.id.MessageCount).setVisibility(count != 0 ? View.VISIBLE : View.GONE);
        }
    };
    private NotifyDataCountChangedListener m_func_notify_notices_count_changed = new NotifyDataCountChangedListener()
    {
        public void onDataCountChanged(int count)
        {
            Log.d("myLogs-notify", "m_func_notify_notices_count_changed");
            ((TextView) BaseUserActivity.this.findViewById(R.id.NoticeCount)).setText(String.valueOf(count));
            BaseUserActivity.this.findViewById(R.id.NoticeCount).setVisibility(count != 0 ? View.VISIBLE : View.GONE);
        }
    };

    protected abstract OnNavigationItemSelectedListener getNavigationListener();


    public void updateOwnUserInfo()
    {
        UserInfoBase userOwn = Application.instance().getUser();

        BitmapManager.instance().setImageView((ImageView) getNavigationHeaderView().findViewById(R.id.HeaderAvatar), String.valueOf(userOwn.getId()), userOwn.getLastTimeUpdate());
        ((TextView) getNavigationHeaderView().findViewById(R.id.FullName)).setText(userOwn.getName() + " " + userOwn.getFamily());
        ((TextView) getNavigationHeaderView().findViewById(R.id.Town)).setText(userOwn.getTown());
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        MessagesManager.instance().setNotifyDataCountChangedListener(m_func_notify_messages_count_changed);
        EventsManager.instance().getNoticesManager().setNotifyDataCountChangedListener(m_func_notify_notices_count_changed);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        m_func_notify_messages_count_changed.onDataCountChanged(MessagesManager.instance().getCount());
        m_func_notify_notices_count_changed.onDataCountChanged(EventsManager.instance().getNoticesManager().getCount());
    }

    protected void initView(int layoutId)
    {
        setContentView(layoutId);

        Toolbar toolbar = (Toolbar) findViewById(R.id.ActionBar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.action_bar_top, null, true));
        actionBar.getCustomView().findViewById(R.id.Messages).setOnClickListener(this.m_func_click_messages);
        actionBar.getCustomView().findViewById(R.id.Notices).setOnClickListener(this.m_func_click_notices);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        ((NavigationView) findViewById(R.id.NavigationView)).setNavigationItemSelectedListener(getNavigationListener());
        updateOwnUserInfo();
    }

    protected View getNavigationHeaderView()
    {
        return ((NavigationView) findViewById(R.id.NavigationView)).getHeaderView(0);
    }

    public void setPageTitle(String title)
    {
        ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.Page)).setText(title);
    }

    protected void logout()
    {
        Application.instance().onLogout();
        activityBack();
    }
}

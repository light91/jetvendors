package ru.jetvendors.Messages;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;

import ru.jetvendors.Application;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Messages.Libs.TalksAdapter;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;

public class TalksFragment extends Fragment
{
    private OnItemClickListener m_func_talk_select = new OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("interlocutor", (int) id);
            Fragment messagesFragment = new TalkMessagesFragment();
            messagesFragment.setArguments(bundle);
            ((BaseFragmentActivity) getActivity()).fragmentForward(messagesFragment);
        }
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0420\u0430\u0437\u0433\u043e\u0432\u043e\u0440\u044b");
        View view = inflater.inflate(R.layout.talk_list_fragment, container, false);
        ((ListView) view.findViewById(R.id.Talks)).setAdapter(new TalksAdapter(inflater.getContext(), Application.instance().getMessages()));
        ((ListView) view.findViewById(R.id.Talks)).setOnItemClickListener(m_func_talk_select);
        ((ListView) view.findViewById(R.id.Talks)).setEmptyView(view.findViewById(R.id.Talks));
        return view;
    }

    public void onStart()
    {
        super.onStart();
        NotifyDataSetChanged dataSetChanged = new NotifyDataSetChanged((BaseAdapter) ((ListView) getView().findViewById(R.id.Talks)).getAdapter());
        Application.instance().getMessages().setNotifyDataSetChangeListener(dataSetChanged);
    }

    public void onStop()
    {
        super.onStop();
        Application.instance().getMessages().setNotifyDataSetChangeListener(null);
    }
}

package ru.jetvendors.Messages;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.sql.Timestamp;
import java.util.Date;

import ru.jetvendors.Application;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.BuildConfig;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.Messages.Libs.Message;
import ru.jetvendors.Messages.Libs.MessagesAdapter;
import ru.jetvendors.Messages.Libs.MessagesManager;
import ru.jetvendors.Messages.Libs.RequestManager;
import ru.jetvendors.Messages.Libs.Talk;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.Profile.CustomerViewFragment;
import ru.jetvendors.Profile.VendorViewFragment;
import ru.jetvendors.Application.AccountType;

public class TalkMessagesFragment extends Fragment {
    private OnClickListener m_func_send;
    private RequestManager m_request_manager;
    private int m_uid;

    /* renamed from: ru.jetvendors.Messages.TalkMessagesFragment.1 */
    class C03171 implements OnClickListener {
        final /* synthetic */ Talk val$talk;

        C03171(Talk talk) {
            this.val$talk = talk;
        }

        public void onClick(View v) {
            Fragment fragment;
            if (UsersManager.instance().getById(TalkMessagesFragment.this.getUid()).getAccountType() == AccountType.customer) {
                fragment = CustomerViewFragment.createInstance(this.val$talk.getId());
            } else {
                fragment = VendorViewFragment.createInstance(this.val$talk.getId());
            }
            ((BaseFragmentActivity) TalkMessagesFragment.this.getActivity()).fragmentForward(fragment);
        }
    }

    /* renamed from: ru.jetvendors.Messages.TalkMessagesFragment.2 */
    class C03182 implements OnClickListener {
        C03182() {
        }

        public void onClick(View view) {
            String text = ((EditText) TalkMessagesFragment.this.getView().findViewById(R.id.MessageText)).getText().toString();
            MessagesManager talks = Application.instance().getMessages();
            Message message = new Message();
            message.setTime(new Timestamp(new Date().getTime()));
            message.setSender(Application.instance().getUserExtra().getUid());
            message.setReceiver(TalkMessagesFragment.this.getUid());
            message.setText(text);
            talks.addMessage(TalkMessagesFragment.this.getUid(), message);
            TalkMessagesFragment.this.m_request_manager.send_message(Application.instance().getUserExtra().getSid(), message);
            int adapterSize = ((ListView) TalkMessagesFragment.this.getView().findViewById(R.id.Messages)).getAdapter().getCount() - 1;
            ((ListView) TalkMessagesFragment.this.getView().findViewById(R.id.Messages)).smoothScrollToPosition(adapterSize);
            ((ListView) TalkMessagesFragment.this.getView().findViewById(R.id.Messages)).setSelection(adapterSize);
            ((EditText) TalkMessagesFragment.this.getView().findViewById(R.id.MessageText)).setText(BuildConfig.FLAVOR);
        }
    }

    public interface OnSendMessage {
        void onError();

        void onSuccess();
    }

    class OwnNotifyDataSetChangedListener extends NotifyDataSetChanged {
        public OwnNotifyDataSetChangedListener(BaseAdapter adapter) {
            super(adapter);
        }

        public void notifyDataSetChange() {
            super.notifyDataSetChange();
            TalkMessagesFragment.this.updateInterlocutorInfo();
        }
    }

    public TalkMessagesFragment() {
        this.m_func_send = new C03182();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0420\u0430\u0437\u0433\u043e\u0432\u043e\u0440");
        this.m_uid = getArguments().getInt("interlocutor");
        this.m_request_manager = new RequestManager();
        MessagesManager talks = Application.instance().getMessages();
        if (talks.getById(getUid()) == null) {
            talks.addEmpty(getUid());
        }
        Talk talk = talks.getById(getUid());
        View view = inflater.inflate(R.layout.talk_messgaes_list_fragment, container, false);
        view.findViewById(R.id.Send).setOnClickListener(this.m_func_send);
        ((ListView) view.findViewById(R.id.Messages)).setAdapter(new MessagesAdapter(inflater.getContext(), talk));
        view.findViewById(R.id.Profile).setOnClickListener(new C03171(talk));
        return view;
    }

    public void onStart() {
        super.onStart();
        MessagesManager.instance().setReading(getUid());
        Application.instance().getMessages().setNotifyDataSetChangeListener(new OwnNotifyDataSetChangedListener((BaseAdapter) ((ListView) getView().findViewById(R.id.Messages)).getAdapter()));
        updateInterlocutorInfo();
    }

    public void onStop() {
        super.onStop();
        Application.instance().getMessages().setNotifyDataSetChangeListener(null);
    }

    public void updateInterlocutorInfo()
    {
        UserInfoBase userOwn = UsersManager.instance().getById(getUid());
        BitmapManager.instance().setImageView((ImageView) getView().findViewById(R.id.Avatar), String.valueOf(userOwn.getId()), userOwn.getLastTimeUpdate());
        ((TextView) getView().findViewById(R.id.FullName)).setText(UsersManager.instance().getById(getUid()).getFullName());
    }

    private int getUid() {
        return this.m_uid;
    }
}

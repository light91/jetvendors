package ru.jetvendors.Messages.Libs;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.SimpleDateFormat;

import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.Messages.Libs.Message;
import ru.jetvendors.Messages.Libs.MessagesManager;
import ru.jetvendors.Messages.Libs.Talk;
import ru.jetvendors.R;
import ru.jetvendors.Data.UserInfoBase;

public class TalksAdapter extends BaseAdapter {
    Context m_context;
    MessagesManager m_data;

    public TalksAdapter(Context context, MessagesManager data) {
        this.m_context = context;
        this.m_data = data;
    }

    public int getCount() {
        return this.m_data.getCount();
    }

    public Talk getItem(int position) {
        return this.m_data.get(position);
    }

    public long getItemId(int position) {
        return (long) getItem(position).getId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Talk talk = getItem(position);
        if (convertView == null) {
            convertView = ((LayoutInflater) this.m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.talk_list_item, parent, false);
        }
        UserInfoBase user = talk.getInterlocutor();
        Message message = talk.getLastMessage();
        String fullName = String.valueOf(user.getFullName());
        String time = new SimpleDateFormat("dd.MM.yyyy HH:mm ").format(message.getTime());
        String messagesCount = String.valueOf(talk.getNewMessagesCount());
        BitmapManager.instance().setImageView((ImageView) convertView.findViewById(R.id.Avatar), String.valueOf(user.getId()), user.getLastTimeUpdate());
        ((TextView) convertView.findViewById(R.id.FullName)).setText(fullName);
        ((TextView) convertView.findViewById(R.id.Time)).setText(time);
        ((TextView) convertView.findViewById(R.id.MessagesCount)).setText(messagesCount);
        return convertView;
    }
}

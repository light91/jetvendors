package ru.jetvendors.Messages.Libs;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ru.jetvendors.ExternLibs.NotifyDataCountChangedListener;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.Application;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class MessagesManager {
    private static MessagesManager m_instance;
    private List<Talk> m_data;
    private NotifyDataCountChangedListener m_notify_data_count_changed_listener;
    private NotifyDataSetChanged m_notify_data_set_change_listener;
    private RequestManager m_request_manager;

    public static MessagesManager instance() {
        if (m_instance == null) {
            m_instance = new MessagesManager();
        }
        return m_instance;
    }

    private MessagesManager() {
        this.m_data = new ArrayList();
        this.m_request_manager = new RequestManager();
    }

    public void onLogin() {
        this.m_request_manager.get_own_talks_with_message(Application.instance().getUserExtra().getSid());
    }

    public void onLogout() {
        setNotifyDataSetChangeListener(null);
        setNotifyDataCountChangedListener(null);
        this.m_data.clear();
    }

    public void setNotifyDataSetChangeListener(NotifyDataSetChanged notifyDataSetChanged) {
        this.m_notify_data_set_change_listener = notifyDataSetChanged;
        UsersManager.instance().setNotifyDataSetChangeListener(notifyDataSetChanged);
    }

    public Talk get(int num) {
        return (Talk) this.m_data.get(num);
    }

    public Talk getById(int id) {
        for (Talk item : this.m_data) {
            if (item.getId() == id) {
                return item;
            }
        }
        this.m_request_manager.get_message(Application.instance().getUserExtra().getSid(), id);
        return null;
    }

    public int getCount() {
        return this.m_data.size();
    }

    private void add(Talk talk) throws NullPointerException {
        if (getById(talk.getId()) != null) {
            Log.d("myLogs", "try duplicate");
            throw new NullPointerException();
        }
        this.m_data.add(talk);
        notifyDataSetChanged();
    }

    public void addEmpty(int uid) {
        Talk talk = new Talk();
        talk.setId(uid);
        add(talk);
    }

    public void addMessage(Message message) {
        addMessage(message.getSender() != Application.instance().getUserExtra().getUid() ? message.getSender() : message.getReceiver(), message);
    }

    public void addMessage(int uid, Message message) {
        getById(uid).add(message);
    }

    public void remove(int id) {
        this.m_data.remove(id);
        notifyDataSetChanged();
    }

    public void removeMessage(int id, Message message) {
        getById(id).remove(message);
        notifyDataSetChanged();
    }

    public void update(int id, Talk updateItem) {
        getById(id).update(updateItem);
        notifyDataSetChanged();
    }

    public void updateMessage(Message oldMessage, Message updateMessage) {
        oldMessage.update(updateMessage);
        notifyDataSetChanged();
    }

    private void notifyDataSetChanged() {
        if (this.m_notify_data_set_change_listener != null) {
            this.m_notify_data_set_change_listener.notifyDataSetChange();
        }
        notifyDataCountChanged();
    }

    public static MessagesManager initByJson(JsonData jsonListData) {
        MessagesManager manager = instance();
        manager.m_data.clear();
        Iterator i$ = jsonListData.getObject().iterator();
        while (i$.hasNext()) {
            manager.add(Talk.initByJson((JsonData) i$.next()));
        }
        return manager;
    }

    public void setReading(int id) {
        getById(id).setReading();
        notifyDataCountChanged();
    }

    public void setNotifyDataCountChangedListener(NotifyDataCountChangedListener notifyDataCountChangedListener) {
        this.m_notify_data_count_changed_listener = notifyDataCountChangedListener;
    }

    private void notifyDataCountChanged() {
        if (this.m_notify_data_count_changed_listener != null) {
            int newCount = 0;
            for (Talk talk : this.m_data) {
                newCount += talk.getNewMessagesCount();
            }
            this.m_notify_data_count_changed_listener.onDataCountChanged(newCount);
        }
    }
}

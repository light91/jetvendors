package ru.jetvendors.Messages.Libs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class Talk {
    private Message m_last_message;
    private List<Message> m_messages;
    private int m_uid;

    public Talk() {
        this.m_messages = new ArrayList();
    }

    public void setId(int id) {
        this.m_uid = id;
    }

    public int getId() {
        return this.m_uid;
    }

    public int getCount() {
        return this.m_messages.size();
    }

    public int getNewMessagesCount() {
        int count = 0;
        for (Message message : this.m_messages) {
            if (message.getSender() == this.m_uid && !message.getIsRead()) {
                count++;
            }
        }
        return count;
    }

    public Message get(int index) {
        if (index < 0 || index >= this.m_messages.size()) {
            return null;
        }
        return (Message) this.m_messages.get(index);
    }

    public int getIndex(Message message) {
        for (int index = 0; index != getCount(); index++) {
            if (((Message) this.m_messages.get(index)).equals(message)) {
                return index;
            }
        }
        return -1;
    }

    public void update(Talk talk) {
        this.m_last_message = talk.m_last_message;
        this.m_messages = talk.m_messages;
        this.m_uid = talk.m_uid;
    }

    public void add(Message message) {
        this.m_messages.add(message);
    }

    private void remove(int index) {
        this.m_messages.remove(index);
    }

    public void remove(Message message) {
        for (int index = 0; index != this.m_messages.size(); index++) {
            if (((Message) this.m_messages.get(index)).equals(message)) {
                this.m_messages.remove(index);
                return;
            }
        }
    }

    public Message getLastMessage() {
        if (this.m_messages.size() == 0) {
            return null;
        }
        return (Message) this.m_messages.get(this.m_messages.size() - 1);
    }

    public void setReading() {
        for (int index = 0; index != this.m_messages.size(); index++) {
            ((Message) this.m_messages.get(index)).setIsRead(true);
        }
    }

    public UserInfoBase getInterlocutor() {
        return UsersManager.instance().getById(this.m_uid);
    }

    public static Talk initByJson(JsonData messageObject) {
        Talk talk = new Talk();
        Iterator i$ = messageObject.getObject().iterator();
        while (i$.hasNext()) {
            Message message = new Message((JsonData) i$.next());
            talk.m_uid = Integer.valueOf(messageObject.getKey()).intValue();
            talk.add(message);
        }
        return talk;
    }
}

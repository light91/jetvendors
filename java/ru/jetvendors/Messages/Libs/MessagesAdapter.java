package ru.jetvendors.Messages.Libs;

import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.SimpleDateFormat;

import ru.jetvendors.R;
import ru.jetvendors.Application;

public class MessagesAdapter extends BaseAdapter {
    private Context m_context;
    private Talk m_talk;

    public MessagesAdapter(Context context, Talk talk) {
        this.m_context = context;
        this.m_talk = talk;
    }

    public int getCount() {
        return this.m_talk.getCount();
    }

    public Message getItem(int position) {
        return this.m_talk.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((LayoutInflater) this.m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.talk_messgaes_list_item, parent, false);
        }
        Message message = getItem(position);
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.Format);
        if (Application.instance().getUserExtra().getUid() == message.getSender()) {
            linearLayout.setBackgroundDrawable(this.m_context.getResources().getDrawable(R.drawable.oval_sender_background));
            ((LinearLayout) convertView).setGravity(GravityCompat.START);
        } else {
            linearLayout.setBackgroundDrawable(this.m_context.getResources().getDrawable(R.drawable.oval_receiver_background));
            ((LinearLayout) convertView).setGravity(GravityCompat.END);
        }
        String time = new SimpleDateFormat("dd.MM.yyyy HH:mm ").format(message.getTime());
        ((TextView) convertView.findViewById(R.id.Message)).setText(message.getText());
        ((TextView) convertView.findViewById(R.id.Time)).setText(time);
        return convertView;
    }
}

package ru.jetvendors.Messages.Libs;

import android.util.Pair;
import java.util.Iterator;

import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class RequestManager extends BaseAsyncRequestManager {
    private static final String host_api = "http://qazaqtv.kz/JetVendors/api.php";

    class ResponseObjectGetMessages extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
            Iterator i$ = ((JsonData) getDecodeData().getObject().get(0)).getObject().iterator();
            while (i$.hasNext()) {
                MessagesManager.instance().addMessage(new Message((JsonData) i$.next()));
            }
        }
    }

    class ResponseObjectGetOwnTalks extends ResponseObject
    {
        public void doResponse(Request requestData) {
            MessagesManager.initByJson((JsonData) getDecodeData().getObject().get(0));
        }
    }

    class ResponseObjectSendMessage extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
            Message message = (Message) requestData.getObject();
            if (!data().isEmpty())
            {
                MessagesManager talks = MessagesManager.instance();
                Talk talk = talks.getById(message.getReceiver());
                Message updateMessage = new Message();
                updateMessage.update(message);
                updateMessage.setId(Integer.valueOf(data()).intValue());
                message.setId(Integer.valueOf(data()).intValue());
                talks.updateMessage(message, updateMessage);
            }
        }
    }

    class ResponseObjectSetTalkRead extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
        }
    }

    public void get_own_talks_with_message(int sid)
    {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_own_talks_with_message");
        requestData.addData("sid", String.valueOf(sid));
        requestData.setUrl(host_api);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetOwnTalks())});
        addRequest(currentRequest);
    }

    public void send_message(int sid, Message message)
    {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "send_message");
        requestData.addData("sid", String.valueOf(sid));
        requestData.addData("receiver", String.valueOf(message.getReceiver()));
        requestData.addData("text", String.valueOf(message.getText()));
        requestData.setUrl(host_api);
        requestData.setObject(message);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectSendMessage())});
        addRequest(currentRequest);
    }

    public void set_talk_read(int sid, int interlocutor)
    {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "set_talk_read");
        requestData.addData("sid", String.valueOf(sid));
        requestData.addData("receiver", String.valueOf(interlocutor));
        requestData.setUrl(host_api);
        currentRequest.execute(new Pair[] { new Pair(requestData, new ResponseObjectSetTalkRead()) });
        addRequest(currentRequest);
    }

    public void get_message(int sid, int id)
    {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_message");
        requestData.addData("sid", String.valueOf(sid));
        requestData.addData("id", String.valueOf(id));
        requestData.setUrl(host_api);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetMessages())});
        addRequest(currentRequest);
    }
}

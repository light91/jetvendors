package ru.jetvendors.Messages.Libs;

import android.util.Log;
import java.sql.Timestamp;
import java.util.Iterator;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class Message {
    private int m_id;
    private boolean m_is_read;
    private int m_receiver;
    private int m_sender;
    private String m_text;
    private Timestamp m_time;

    public Message(JsonData messageObject)
    {
        initByJson(messageObject);
    }

    public Message()
    {

    }

    public void update(Message message) {
        this.m_id = message.m_id;
        this.m_time = message.m_time;
        this.m_sender = message.m_sender;
        this.m_receiver = message.m_receiver;
        this.m_text = message.m_text;
        this.m_is_read = message.m_is_read;
    }

    public void setId(int id) {
        this.m_id = id;
    }

    public void setSender(int sender) {
        this.m_sender = sender;
    }

    public void setReceiver(int receiver) {
        this.m_receiver = receiver;
    }

    public void setText(String text) {
        this.m_text = text;
    }

    public void setTime(Timestamp time) {
        this.m_time = time;
    }

    public void setIsRead(boolean isRead) {
        this.m_is_read = isRead;
    }

    public int getId() {
        return this.m_id;
    }

    public int getSender() {
        return this.m_sender;
    }

    public int getReceiver() {
        return this.m_receiver;
    }

    public String getText() {
        return this.m_text;
    }

    public Timestamp getTime() {
        return this.m_time;
    }

    public boolean getIsRead() {
        return this.m_is_read;
    }

    public void initByJson(JsonData messageObject) {
        boolean z = true;
        Iterator i$ = messageObject.getObject().iterator();
        while (i$.hasNext()) {
            JsonData messageItem = (JsonData) i$.next();
            Log.d("myLogs - 1 ", "messageItem.getKey() " + messageItem.getKey());
            String key = messageItem.getKey();
            switch (key)
            {
                case "id":
                    this.m_id = Integer.valueOf(messageItem.getValue()).intValue();
                    break;
                case "receiver":
                    this.m_receiver = Integer.valueOf(messageItem.getValue()).intValue();
                    break;
                case "sender":
                    this.m_sender = Integer.valueOf(messageItem.getValue()).intValue();
                    break;
                case "text":
                    this.m_text = messageItem.getValue();
                    break;
                case "time":
                    this.m_time = Timestamp.valueOf(messageItem.getValue());
                    break;
                case "is_read":
                    this.m_is_read = Boolean.valueOf(messageItem.getValue()).booleanValue();
                    break;
                default:
                    break;
            }
        }
        String str = "myLogs - 1 ";
        StringBuilder append = new StringBuilder().append("null?");
        if (getTime() != null) {
            z = false;
        }
        Log.d(str, append.append(z).toString());
    }
}

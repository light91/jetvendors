package ru.jetvendors.Notifications.Libs;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.SpannableStringBuilder;
import android.view.View.OnClickListener;
import java.sql.Timestamp;
import java.util.Iterator;

import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public abstract class BaseNotifications implements Parcelable
{
    public static final Creator<BaseNotifications> CREATOR = new Creator<BaseNotifications>()
    {
        public BaseNotifications createFromParcel(Parcel source)
        {
            int itemType = source.readInt();
            BaseNotifications notification = BaseNotifications.createInstance(itemType);
            notification.m_item_type = itemType;
            notification.m_id = source.readInt();
            notification.m_uid = source.readInt();
            notification.m_item_id = source.readInt();
            notification.m_event = source.readInt();
            notification.m_time = Timestamp.valueOf(source.readString());
            return null;
        }

        public BaseNotifications[] newArray(int size)
        {
            return null;
        }
    };
    private int m_event;
    private int m_id;
    private int m_item_id;
    private int m_item_type;
    private Timestamp m_time;
    private int m_uid;


    public abstract SpannableStringBuilder getBody(OnClickListener onClickListener);

    public int getId()
    {
        return m_id;
    }

    protected void setUid(int uid) {
        m_uid = uid;
    }

    public int getUid() {
        return m_uid;
    }

    public int getItemType() {
        return m_item_type;
    }

    public int getItemId() {
        return m_item_id;
    }

    public int getEvent() {
        return m_event;
    }

    public Timestamp getTime() {
        return m_time;
    }

    public static BaseNotifications createInstance(int itemType)
    {
        BaseNotifications notification = null;
        switch (itemType) {
            case ItemTouchHelper.ACTION_STATE_IDLE /*0*/:
                notification = new NotificationsAccount();
                break;
            case ItemTouchHelper.UP /*1*/:
                notification = new NotificationsMessages();
                break;
            case ItemTouchHelper.DOWN /*2*/:
                notification = new NotificationsOffers();
                break;
        }
        notification.m_item_type = itemType;
        return notification;
    }

    public void initByJson(JsonData jsonData)
    {
        Iterator i$ = jsonData.getObject().iterator();
        while (i$.hasNext())
        {
            JsonData jsonItem = (JsonData) i$.next();
            String key = jsonItem.getKey();
            switch (key)
            {
                case "id":
                    m_id = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                case "item_type":
                    m_item_type = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                case "item_id":
                    m_item_id = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                case "notification":
                    m_event = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                case "time":
                    m_time = Timestamp.valueOf(jsonItem.getValue());
                    break;
                default:
                    break;
            }
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getItemType());
        dest.writeInt(getId());
        dest.writeInt(getUid());
        dest.writeInt(getItemId());
        dest.writeInt(getEvent());
        dest.writeString(getTime().toString());
    }
}

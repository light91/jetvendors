package ru.jetvendors.Notifications.Libs;

import android.os.Bundle;
import android.os.Parcelable;
import com.vk.sdk.api.model.VKScopes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ru.jetvendors.ExternLibs.NotifyDataCountChangedListener;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Data.OffersManager;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class NotificationsManager {
    private int m_new_count;
    private List<BaseNotifications> m_notifications;
    private NotifyDataCountChangedListener m_notify_data_count_changed_listener;
    private NotifyDataSetChanged m_notify_data_set_change_listener;
    private Map<Integer, OffersManager> m_offers;

    public NotificationsManager() {
        this.m_new_count = 0;
        this.m_offers = new HashMap();
        this.m_notifications = new ArrayList();
    }

    public void setNotifyDataCountChangedListener(NotifyDataCountChangedListener notifyDataCountChangedListener) {
        this.m_notify_data_count_changed_listener = notifyDataCountChangedListener;
    }

    private void notifyDataCountChanged() {
        if (this.m_notify_data_count_changed_listener != null) {
            this.m_notify_data_count_changed_listener.onDataCountChanged(this.m_new_count);
        }
    }

    public void setReading() {
        this.m_new_count = 0;
        notifyDataCountChanged();
    }

    public void setNotifyDataSetChangeListener(NotifyDataSetChanged notifyDataSetChanged) {
        this.m_notify_data_set_change_listener = notifyDataSetChanged;
        UsersManager.instance().setNotifyDataSetChangeListener(notifyDataSetChanged);
    }

    private void notifyDataSetChanged() {
        if (this.m_notify_data_set_change_listener != null) {
            this.m_notify_data_set_change_listener.notifyDataSetChange();
        }
    }

    public void updateByJson(int itemType, JsonData jsonItem)
    {
        BaseNotifications notification = BaseNotifications.createInstance(itemType);
        notification.initByJson(jsonItem);
        int index = getIndexById(notification.getItemId());
        if (notification.getEvent() == 0) {
            if (index < 0) {
                add(notification);
            }
        } else if (notification.getEvent() == 1) {
            if (index >= 0) {
                remove(index);
            }
        } else if (notification.getEvent() == 2 && index >= 0) {
            update(index, notification);
        }
    }

    private void add(BaseNotifications notification) {
        this.m_notifications.add(notification);
        this.m_new_count++;
        notifyDataCountChanged();
        notifyDataSetChanged();
    }

    private void remove(int index) {
        this.m_notifications.remove(index);
        this.m_new_count--;
        notifyDataCountChanged();
        notifyDataSetChanged();
    }

    private void update(int index, BaseNotifications notification) {
        this.m_notifications.set(index, notification);
        notifyDataSetChanged();
    }

    public void onSavedInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("new_count", this.m_new_count);
        savedInstanceState.putParcelableArray(VKScopes.NOTIFICATIONS, (Parcelable[]) this.m_notifications.toArray(new Parcelable[this.m_notifications.size()]));
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.m_new_count = savedInstanceState.getInt("new_count");
        for (Parcelable parcel : savedInstanceState.getParcelableArray(VKScopes.NOTIFICATIONS)) {
            add((BaseNotifications) parcel);
        }
    }

    public BaseNotifications get(int index) {
        return (BaseNotifications) this.m_notifications.get(index);
    }

    public int getIndexById(int id) {
        for (int index = 0; index != this.m_notifications.size(); index++) {
            if (((BaseNotifications) this.m_notifications.get(index)).getItemId() == id) {
                return index;
            }
        }
        return -1;
    }

    public int getCount() {
        return this.m_notifications.size();
    }

    private void loadOffers(int uid) {
        OffersManager offersManager = new OffersManager();
        offersManager.setNotifyDataSetChanged(this.m_notify_data_set_change_listener);
        Bundle bundle = new Bundle();
        bundle.putString("uid", String.valueOf(uid));
        offersManager.requestItems(bundle);
        this.m_offers.put(Integer.valueOf(uid), offersManager);
    }

    public OffersManager getOffers(int uid) {
        if (this.m_offers.containsKey(Integer.valueOf(uid))) {
            return (OffersManager) this.m_offers.get(Integer.valueOf(uid));
        }
        loadOffers(uid);
        return null;
    }
}

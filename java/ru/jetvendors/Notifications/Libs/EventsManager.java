package ru.jetvendors.Notifications.Libs;

import android.os.Bundle;
import android.util.Log;
import java.util.Iterator;

import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class EventsManager
{
    static final int EVENT_ADD = 0;
    static final int EVENT_REMOVE = 1;
    static final int EVENT_UPDATE = 2;
    static final int NOTIFICATION_ACCOUNT = 0;
    static final int NOTIFICATION_MESSAGES = 1;
    static final int NOTIFICATION_OFFERS = 2;
    private static EventsManager m_instance;
    private NotificationsManager m_messages_manager;
    private NotificationsManager m_notices_manager;
    private RequestManager m_request_manager;

    private EventsManager() {
        m_notices_manager = new NotificationsManager();
        m_messages_manager = new NotificationsManager();
        m_request_manager = new RequestManager();
    }

    public void onLogout() {
        m_notices_manager = new NotificationsManager();
        m_messages_manager = new NotificationsManager();
        m_request_manager = new RequestManager();
    }

    public static EventsManager instance() {
        if (m_instance == null) {
            m_instance = new EventsManager();
        }
        return m_instance;
    }

    public void updateByJson(JsonData jsonData)
    {
        Log.d("myLogsaa", "updateByJson");
        Iterator it = jsonData.getObject().iterator();
        while (it.hasNext())
        {
            JsonData jsonItem = (JsonData) it.next();
            Log.d("myLogsaa", "updateByJson 1");
            int itemType = -1;
            Iterator i$ = jsonItem.getObject().iterator();
            while (i$.hasNext())
            {
                JsonData notificationItem = (JsonData) i$.next();
                if (notificationItem.getKey().equals("item_type"))
                    itemType = Integer.valueOf(notificationItem.getValue()).intValue();
            }

            switch (itemType)
            {
                case NOTIFICATION_MESSAGES:
                    m_messages_manager.updateByJson(itemType, jsonItem);
                    break;
                default:
                    m_notices_manager.updateByJson(itemType, jsonItem);
                    break;
            }
        }
    }

    public NotificationsManager getMessagesManager() {
        return this.m_messages_manager;
    }

    public NotificationsManager getNoticesManager() {
        return this.m_notices_manager;
    }

    public void checkNotifications(int sid) {
        this.m_request_manager.get_events(sid);
    }

    public void onSavedInstanceState(Bundle savedInstanceState)
    {
        m_messages_manager.onSavedInstanceState(savedInstanceState);
        m_notices_manager.onSavedInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        m_messages_manager.onRestoreInstanceState(savedInstanceState);
        m_notices_manager.onRestoreInstanceState(savedInstanceState);
    }
}

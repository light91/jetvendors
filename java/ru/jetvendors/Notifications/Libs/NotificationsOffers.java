package ru.jetvendors.Notifications.Libs;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.View.OnClickListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ru.jetvendors.BuildConfig;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersManager;
import ru.jetvendors.Data.OffersProductItem;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;
import ru.jetvendors.UserCategories.Libs.CategoriesSpecial;

public class NotificationsOffers extends BaseNotifications {
    public void initByJson(JsonData jsonData) {
        super.initByJson(jsonData);
        OffersItem offer = null;
        Iterator i$ = jsonData.getObject().iterator();
        while (i$.hasNext()) {
            JsonData jsonItem = (JsonData) i$.next();
            if (jsonItem.getKey().equals("item")) {
                offer = new OffersItem();
                offer.addArray(jsonItem);
            }
        }
        setUid(offer.getExtra().getUid());
    }

    public SpannableStringBuilder getBody(OnClickListener onClick) {
        UserInfoBase userInfo = UsersManager.instance().getById(getUid());
        OffersManager offersManager = EventsManager.instance().getNoticesManager().getOffers(getUid());
        String preLink = "\u0417\u0430\u043a\u0430\u0437\u0447\u0438\u043a ";
        Spanned link = Html.fromHtml("<font color='#123456'>" + userInfo.getFullName() + "</font>");
        String afterLink = " \u0437\u0430\u043a\u0430\u0437\u0430\u043b \u0442\u043e\u0432\u0430\u0440, \u043a\u043e\u0442\u043e\u0440\u044b\u0439 \u0443 \u0432\u0430\u0441 \u0435\u0441\u0442\u044c \u0432 \u043d\u0430\u043b\u0438\u0447\u0438\u0438: ";
        Log.d("mylogs-Notifications", "NotificationsOffers 1");
        String offers = BuildConfig.FLAVOR;
        if (offersManager != null) {
            List<Integer> list = new ArrayList();
            for (int offerIndex = 0; offerIndex != offersManager.getCount(); offerIndex++) {
                Log.d("mylogs-Notifications", "NotificationsOffers 0");
                OffersItem offer = (OffersItem) offersManager.get(offerIndex);
                for (int index = 0; index != offer.getCount(); index++) {
                    OffersProductItem product = (OffersProductItem) offer.get(index);
                    list.add(Integer.valueOf(product.getCatId()));
                    list.add(Integer.valueOf(product.getSubCatId()));
                    Log.d("mylogs-Notifications", "NotificationsOffers 2");
                }
            }
            offers = CategoriesSpecial.getCatsText(list, ",", "|", "|");
        }
        SpannableStringBuilder textBuilder = new SpannableStringBuilder();
        textBuilder.append(preLink);
        textBuilder.append(link);
        textBuilder.append(afterLink);
        textBuilder.append("\n");
        textBuilder.append(offers);
        return textBuilder;
    }
}

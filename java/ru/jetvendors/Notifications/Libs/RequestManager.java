package ru.jetvendors.Notifications.Libs;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class RequestManager extends BaseAsyncRequestManager {
    private static final String host_api = "http://qazaqtv.kz/JetVendors/api.php";

    /* renamed from: ru.jetvendors.Notifications.Libs.RequestManager.1 */
    class C03241 extends Thread {
        final /* synthetic */ Handler val$handler;
        final /* synthetic */ Request val$requestData;

        /* renamed from: ru.jetvendors.Notifications.Libs.RequestManager.1.1 */
        class C03231 implements Runnable {
            final /* synthetic */ ResponseObject val$response;

            C03231(ResponseObject responseObject) {
                this.val$response = responseObject;
            }

            public void run() {
                try {
                    this.val$response.doResponse(C03241.this.val$requestData);
                } catch (NullPointerException e) {
                    Log.d("myLogs", "onPostExecute: nullPointerException");
                }
            }
        }

        C03241(Request request, Handler handler) {
            this.val$requestData = request;
            this.val$handler = handler;
        }

        public void run() {
            ResponseObject response = new ResponseObjectGetOffer();
            response.data(RequestObject.doRequest(this.val$requestData.getUrl(), this.val$requestData.getData(), this.val$requestData.getFilePath(), this.val$requestData.getFileName(), "image/jpeg"));
            this.val$handler.post(new C03231(response));
        }
    }

    class ResponseObjectGetOffer extends ResponseObject {
        ResponseObjectGetOffer() {
            super();
        }

        public void doResponse(Request requestData) {
            EventsManager.instance().updateByJson((JsonData) getDecodeData().getObject().get(0));
        }
    }

    public void get_events(int sid) {
        Log.d("myLogs-AsyncRequest", "1 ");
        Handler handler = new Handler(Looper.getMainLooper());
        Log.d("myLogs-AsyncRequest", "11");
        Log.d("myLogs-AsyncRequest", "2");
        Request requestData = new Request(false);
        requestData.addData("action", "get_events");
        requestData.addData("sid", String.valueOf(sid));
        requestData.setUrl(host_api);
        Log.d("myLogs-AsyncRequest", "3");
        new C03241(requestData, handler).start();
    }
}

package ru.jetvendors.Notifications.Libs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import ru.jetvendors.R;

public class NotificationsAdapter extends BaseAdapter {
    private Context m_context;
    private OnClickListener m_func_select_user;

    /* renamed from: ru.jetvendors.Notifications.Libs.NotificationsAdapter.1 */
    class C03211 implements OnClickListener {
        C03211() {
        }

        public void onClick(View v) {
        }
    }

    public NotificationsAdapter(Context context) {
        this.m_func_select_user = new C03211();
        this.m_context = context;
    }

    public int getCount() {
        return EventsManager.instance().getNoticesManager().getCount();
    }

    public BaseNotifications getItem(int position) {
        return EventsManager.instance().getNoticesManager().get(position);
    }

    public long getItemId(int position) {
        return (long) EventsManager.instance().getNoticesManager().get(position).getId();
    }

    public View getView(int position, View contentView, ViewGroup parent) {
        if (contentView == null) {
            contentView = ((LayoutInflater) this.m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.notifications_item, parent, false);
        }
        BaseNotifications notice = getItem(position);
        ((TextView) contentView.findViewById(R.id.Time)).setText(notice.getTime().toString());
        ((TextView) contentView.findViewById(R.id.Text)).setText(notice.getBody(this.m_func_select_user));
        return contentView;
    }
}

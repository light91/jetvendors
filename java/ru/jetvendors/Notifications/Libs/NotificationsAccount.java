package ru.jetvendors.Notifications.Libs;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.View.OnClickListener;
import java.util.Iterator;
import ru.jetvendors.Data.UserCats;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class NotificationsAccount extends BaseNotifications {
    public void initByJson(JsonData jsonData) {
        super.initByJson(jsonData);
        UserInfoBase userInfo = null;
        Iterator i$ = jsonData.getObject().iterator();
        while (i$.hasNext()) {
            JsonData jsonItem = (JsonData) i$.next();
            if (jsonItem.getKey().equals("item")) {
                userInfo = new UserInfoBase(jsonItem);
            }
        }
        setUid(userInfo.getId());
    }

    public SpannableStringBuilder getBody(OnClickListener onClick) {
        UserInfoBase userInfo = UsersManager.instance().getById(getUid());
        UserCats userCats = userInfo.getUserCats();
        Spanned link = Html.fromHtml("<font color='#123456'>" + userInfo.getFullName() + "</font>");
        String catsList = userInfo.getUserCats().getCatsText(",", "|");
        SpannableStringBuilder textBuilder = new SpannableStringBuilder();
        textBuilder.append("\u0423 \u043f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a\u0430 ");
        textBuilder.append(link);
        textBuilder.append(" \u0435\u0441\u0442\u044c \u0432 \u043d\u0430\u043b\u0438\u0447\u0438\u0438 \u0442\u043e\u0432\u0430\u0440\u044b \u043a\u043e\u0442\u043e\u0440\u044b\u0435 \u0432\u044b \u0437\u0430\u043a\u0430\u0437\u044b\u0432\u0430\u043b\u0438: ");
        textBuilder.append("\n");
        textBuilder.append(catsList);
        return textBuilder;
    }
}

package ru.jetvendors.Notifications.Libs;

import android.text.SpannableStringBuilder;
import android.view.View.OnClickListener;
import java.util.Iterator;
import ru.jetvendors.Messages.Libs.Message;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class NotificationsMessages extends BaseNotifications {
    public void initByJson(JsonData jsonData) {
        super.initByJson(jsonData);
        Message message = null;
        Iterator i$ = jsonData.getObject().iterator();
        while (i$.hasNext()) {
            JsonData jsonItem = (JsonData) i$.next();
            String key = jsonItem.getKey();
            switch (key)
            {
                case "item":
                    message = new Message(jsonItem);
                    break;
            }
        }
        setUid(message.getSender());
    }

    public SpannableStringBuilder getBody(OnClickListener onClick) {
        return null;
    }
}

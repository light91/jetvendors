package ru.jetvendors.Notifications;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Notifications.Libs.BaseNotifications;
import ru.jetvendors.Notifications.Libs.EventsManager;
import ru.jetvendors.Notifications.Libs.NotificationsAdapter;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.Profile.CustomerViewFragment;
import ru.jetvendors.Profile.VendorViewFragment;
import ru.jetvendors.Application.AccountType;

public class NotificationsFragment extends Fragment
{
    private OnItemClickListener m_func_select_user = new OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {
            Fragment fragment;
            int uid = ((BaseNotifications) parent.getItemAtPosition(position)).getUid();
            if (UsersManager.instance().getById(uid).getAccountType() == AccountType.customer)
            {
                fragment = CustomerViewFragment.createInstance(uid);
            }
            else
            {
                fragment = VendorViewFragment.createInstance(uid);
            }
            ((BaseFragmentActivity) NotificationsFragment.this.getActivity()).fragmentForward(fragment);
        }
    };


    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        ((BaseUserActivity) getActivity()).setPageTitle("Уведомления");
        View view = inflater.inflate(R.layout.notifications_fragment, parent, false);
        EventsManager.instance().getNoticesManager().setReading();
        ListView listView = (ListView) view.findViewById(R.id.Notifications);
        listView.setAdapter(new NotificationsAdapter(view.getContext()));
        listView.setOnItemClickListener(m_func_select_user);
        listView.setEmptyView(view.findViewById(R.id.Empty));
        return view;
    }

    public void onStart()
    {
        super.onStart();
        NotifyDataSetChanged dataSetChanged = new NotifyDataSetChanged((BaseAdapter) ((ListView) getView().findViewById(R.id.Notifications)).getAdapter());
        EventsManager.instance().getNoticesManager().setNotifyDataSetChangeListener(dataSetChanged);
    }

    public void onPause()
    {
        super.onPause();
        EventsManager.instance().getNoticesManager().setNotifyDataSetChangeListener(null);
    }
}

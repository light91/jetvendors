package ru.jetvendors.Profile;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import ru.jetvendors.AccountSign.Libs.CitiesAdapter;
import ru.jetvendors.AccountSign.Libs.KindOfBisnessAdapter;
import ru.jetvendors.Application;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.R;
import ru.jetvendors.Data.CitiesData;
import ru.jetvendors.Data.KindOfBisnessData;
import ru.jetvendors.Data.UserInfoBase;

public class VendorEditFragment extends BaseProfileEditFragment
{
    private OnClickListener m_func_view_reviews = new OnClickListener()
    {
        public void onClick(View v)
        {
            ((BaseFragmentActivity) VendorEditFragment.this.getActivity()).fragmentForward(new VendorReviewsFragment());
        }
    };


    protected void updateEditUserInfo()
    {
        m_edit_user_info = new UserInfoBase();
        m_edit_user_info.setId(Application.instance().getUserExtra().getUid());
        m_edit_user_info.setAbout(((TextView) getView().findViewById(R.id.About)).getText().toString());
        m_edit_user_info.setEmail(((TextView) getView().findViewById(R.id.Email)).getText().toString());
        m_edit_user_info.setFamily(((TextView) getView().findViewById(R.id.Family)).getText().toString());
        m_edit_user_info.setName(((TextView) getView().findViewById(R.id.Name)).getText().toString());
        m_edit_user_info.setTown((int) ((Spinner) getView().findViewById(R.id.Town)).getSelectedItemId());
        m_edit_user_info.setBid((int) ((Spinner) getView().findViewById(R.id.Business)).getSelectedItemId());
    }

    protected void updateOwnUserInfo()
    {
        Application.instance().getUser().setAbout(this.m_edit_user_info.getAbout());
        Application.instance().getUser().setEmail(this.m_edit_user_info.getEmail());
        Application.instance().getUser().setFamily(this.m_edit_user_info.getFamily());
        Application.instance().getUser().setName(this.m_edit_user_info.getName());
        Application.instance().getUser().setTown(this.m_edit_user_info.getTid());
        Application.instance().getUser().setBisness(this.m_edit_user_info.getBisness());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0420\u0435\u0434\u0430\u043a\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u043f\u0440\u043e\u0444\u0438\u043b\u044f");
        View view = inflater.inflate(R.layout.profile_vendor_edit_fragment, container, false);
        UserInfoBase userOwn = Application.instance().getUser();
        CitiesData cities = new CitiesData();
        Log.d("myLogs-country", "userOwn.getTid() " + userOwn.getTid() + " userOwn.getCountry()  " + userOwn.getCountry());
        int activeIndex = cities.getIndexById(userOwn.getTid(), userOwn.getCountry());
        ((Spinner) view.findViewById(R.id.Town)).setAdapter(new CitiesAdapter(inflater.getContext(), cities, userOwn.getCountry()));
        ((Spinner) view.findViewById(R.id.Town)).setSelection(activeIndex);
        KindOfBisnessData kindOfBusiness = new KindOfBisnessData();
        activeIndex = kindOfBusiness.getIndexById(userOwn.getBid());
        ((Spinner) view.findViewById(R.id.Business)).setAdapter(new KindOfBisnessAdapter(inflater.getContext(), kindOfBusiness));
        ((Spinner) view.findViewById(R.id.Business)).setSelection(activeIndex);
        ((TextView) view.findViewById(R.id.About)).setText(userOwn.getAbout());
        ((TextView) view.findViewById(R.id.Email)).setText(userOwn.getEmail());
        ((TextView) view.findViewById(R.id.Family)).setText(userOwn.getFamily());
        ((TextView) view.findViewById(R.id.Name)).setText(userOwn.getName());
        BitmapManager.instance().setImageView((ImageView) view.findViewById(R.id.Avatar), String.valueOf(userOwn.getId()), userOwn.getLastTimeUpdate());
        view.findViewById(R.id.Reviews).setOnClickListener(this.m_func_view_reviews);
        onInit(view);
        return view;
    }
}
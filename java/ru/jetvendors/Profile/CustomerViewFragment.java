package ru.jetvendors.Profile;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Data.OffersManager;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.Messages.TalkMessagesFragment;
import ru.jetvendors.Offers.Libs.ViewListAdapter;

public class CustomerViewFragment extends Fragment {
    private OffersManager m_offers;
    private NotifyDataSetChanged m_func_user_loaded = new NotifyDataSetChanged(null)
    {
        public void notifyDataSetChange() {
            updateView();
        }
    };

    /* renamed from: ru.jetvendors.Profile.CustomerViewFragment.1 */
    class C03471 implements OnClickListener {
        C03471() {
        }

        public void onClick(View v) {
            Bundle bundle = new Bundle();
            bundle.putInt("interlocutor", getUid());
            Fragment messagesFragment = new TalkMessagesFragment();
            messagesFragment.setArguments(bundle);
            ((BaseFragmentActivity) getActivity()).fragmentForward(messagesFragment);
        }
    }

    class MyNotifyDataSetChangedListener extends NotifyDataSetChanged {
        public MyNotifyDataSetChangedListener(BaseAdapter adapter) {
            super(adapter);
        }

        public void notifyDataSetChange() {
            super.notifyDataSetChange();
            updateView();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u043f\u0440\u043e\u0444\u0438\u043b\u044f");
        View view = inflater.inflate(R.layout.profile_customer_view_fragment, container, false);
        BitmapManager.instance().setImageView((ImageView) view.findViewById(R.id.Avatar), String.valueOf(getUser().getId()), getUser().getLastTimeUpdate());
        view.findViewById(R.id.SendMessage).setOnClickListener(new C03471());
        m_offers = new OffersManager();
        BaseAdapter adapter = new ViewListAdapter(inflater.getContext(), m_offers);
        ((ListView) view.findViewById(R.id.Offers)).setAdapter(adapter);
        m_offers.setNotifyDataSetChanged(new MyNotifyDataSetChangedListener(adapter));
        Bundle bundle = new Bundle();
        bundle.putString("uid", String.valueOf(getUid()));
        m_offers.requestItems(bundle);
        UsersManager.instance().setNotifyDataSetChangeListener(m_func_user_loaded);
        return view;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        updateView();
    }

    public static CustomerViewFragment createInstance(int uid) {
        CustomerViewFragment fragment = new CustomerViewFragment();
        fragment.setArguments(new Bundle());
        fragment.getArguments().putInt("uid", uid);
        return fragment;
    }

    private int getUid() {
        return getArguments().getInt("uid");
    }

    private UserInfoBase getUser()
    {
        return UsersManager.instance().getById(getUid());
    }

    protected void updateView()
    {
        Log.d("myLogs-customer-0", " getUid() " + getUid() + " name: " + UsersManager.instance().getById(getUid()).getFullName() + " getView() == null " + (getView() == null) + " userLoaded " + UsersManager.instance().getById(getUid()).checkUserLoaded());

        UserInfoBase user = UsersManager.instance().getById(getUid());
        if (getView() == null)
            return;
        if (!user.checkUserLoaded())
            return;

        Log.d("myLogs-customer", "getFullName " + UsersManager.instance().getById(getUid()).getFullName());

        ((TextView) getView().findViewById(R.id.Email)).setText(user.getEmail());
        ((TextView) getView().findViewById(R.id.CompanyName)).setText(user.getBisness());
        ((TextView) getView().findViewById(R.id.Town)).setText(user.getTown());
        ((TextView) getView().findViewById(R.id.FullName)).setText(user.getFullName());
        BitmapManager.instance().setImageView((ImageView) getView().findViewById(R.id.Avatar), String.valueOf(user.getId()), user.getLastTimeUpdate());
    }
}

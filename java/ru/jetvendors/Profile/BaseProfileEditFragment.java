package ru.jetvendors.Profile;

import android.app.Fragment;
import android.appwidget.AppWidgetProvider;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.Random;

import ru.jetvendors.Application;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Profile.Libs.RequestManager;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.Data.UserInfoBase;

public abstract class BaseProfileEditFragment extends Fragment {
    public static final int SELECT_PICTURE_REQUEST_CODE = 10;
    protected UserInfoBase m_edit_user_info;
    private RequestManager m_request_manager;
    private OnClickListener m_func_avatar_select = new OnClickListener()
    {
        public void onClick(View v)
        {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE).putExtra("output", Uri.fromFile(Application.instance().getCacheImage()));
            Intent galleryIntent = new Intent();
            galleryIntent.setAction(Intent.ACTION_PICK);
            galleryIntent.setType("image/*");
            Intent chooserIntent = Intent.createChooser(galleryIntent, "\u0412\u044b\u0431\u043e\u0440 \u0430\u0432\u0430\u0442\u0430\u0440\u0430");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{cameraIntent});
            startActivityForResult(chooserIntent, BaseProfileEditFragment.SELECT_PICTURE_REQUEST_CODE);
        }
    };
    private OnClickListener m_func_on_save = new OnClickListener() {
        public void onClick(View view) {
            updateEditUserInfo();
            m_request_manager.update_profile(m_edit_user_info, on_func_update_profile);
        }
    };
    private OnUpdateProfileListener on_func_update_profile = new OnUpdateProfileListener()
    {
        public void onSuccess()
        {
            BaseProfileEditFragment.this.updateOwnUserInfo();
            ((BaseUserActivity) BaseProfileEditFragment.this.getActivity()).updateOwnUserInfo();
            Toast.makeText(BaseProfileEditFragment.this.getView().getContext(), "\u041f\u0440\u043e\u0444\u0438\u043b\u044c \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u043d", Toast.LENGTH_SHORT).show();
        }

        public void onError()
        {
            Toast.makeText(BaseProfileEditFragment.this.getView().getContext(), "\u041d\u0435 \u0443\u0434\u0430\u043b\u043e\u0441\u044c \u0438\u0437\u043c\u0435\u043d\u0438\u0442\u044c \u043f\u0440\u043e\u0444\u0438\u043b\u044c", Toast.LENGTH_SHORT).show();
        }
    };

    public interface OnUpdateProfileListener {
        void onError();

        void onSuccess();
    }

    abstract void updateEditUserInfo();

    abstract void updateOwnUserInfo();

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == -1 && requestCode == SELECT_PICTURE_REQUEST_CODE)
        {
            Uri selectedFile;
            if (data != null && (data.getAction() == null || !data.getAction().equals("android.media.action.IMAGE_CAPTURE")))
            {
                selectedFile = (data == null) ? null : data.getData();
            }
            else
            {
                selectedFile = Uri.fromFile(Application.instance().getCacheImage());
            }

            Log.i("image", "image selectedFile");
            if (selectedFile != null)
            {
                try
                {

                    Log.i("image", "image selectedFile 1");
                    String imageName = String.valueOf(Application.instance().getUser().getId());
                    AssetFileDescriptor fileDescriptor = Application.instance().getContentResolver().openAssetFileDescriptor(selectedFile, "r");
                    Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor());
                    BitmapManager.instance().saveFile(bitmap, imageName, System.currentTimeMillis());
                    ((ImageView) getView().findViewById(R.id.Avatar)).setImageBitmap(BitmapManager.instance().getImage(imageName));
                }
                catch (Exception e)
                {

                    Log.i("image", "image selectedFile Exception");

                }
            }
        }
    }

    public View onInit(View view)
    {
        m_request_manager = new RequestManager();
        view.findViewById(R.id.Avatar).setOnClickListener(this.m_func_avatar_select);
        view.findViewById(R.id.Save).setOnClickListener(this.m_func_on_save);
        return view;
    }
}

package ru.jetvendors.Profile;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Profile.Libs.VendorReviewsAdapter;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Data.ReviewsItem;
import ru.jetvendors.Data.ReviewsManager;
import ru.jetvendors.Application;

public class VendorReviewsFragment extends Fragment {
    private OnItemClickListener m_func_select_vendor;
    private ReviewsManager m_review_data;

    /* renamed from: ru.jetvendors.Profile.VendorReviewsFragment.1 */
    class C03501 implements OnItemClickListener {
        C03501() {
        }

        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ((BaseFragmentActivity) VendorReviewsFragment.this.getActivity()).fragmentForward(VendorViewFragment.createInstance(((ReviewsItem) parent.getItemAtPosition(position)).getUser().getId()));
        }
    }

    public VendorReviewsFragment() {
        this.m_func_select_vendor = new C03501();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0421\u043f\u0438\u0441\u043e\u043a \u043e\u0442\u0437\u044b\u0432\u043e\u0432");
        return inflater.inflate(R.layout.profile_vendor_reviews_fragment, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.m_review_data = new ReviewsManager(Application.instance().getUserExtra().getUid());
        BaseAdapter adapter = new VendorReviewsAdapter(getView().getContext(), this.m_review_data);
        this.m_review_data.setNotifyDataSetChanged(new NotifyDataSetChanged(adapter));
        ((ListView) getView().findViewById(R.id.Reviews)).setAdapter(adapter);
        ((ListView) getView().findViewById(R.id.Reviews)).setOnItemClickListener(this.m_func_select_vendor);
        this.m_review_data.requestItems(new Bundle());
    }
}

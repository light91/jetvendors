package ru.jetvendors.Profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import ru.jetvendors.AccountSign.Libs.CitiesAdapter;
import ru.jetvendors.Application;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.R;
import ru.jetvendors.Data.CitiesData;
import ru.jetvendors.Data.UserInfoBase;

public class CustomerEditFragment extends BaseProfileEditFragment {
    protected void updateEditUserInfo() {
        this.m_edit_user_info = new UserInfoBase();
        this.m_edit_user_info.setId(Application.instance().getUserExtra().getUid());
        this.m_edit_user_info.setEmail(((TextView) getView().findViewById(R.id.Email)).getText().toString());
        this.m_edit_user_info.setBisness(((TextView) getView().findViewById(R.id.CompanyName)).getText().toString());
        this.m_edit_user_info.setFamily(((TextView) getView().findViewById(R.id.Family)).getText().toString());
        this.m_edit_user_info.setName(((TextView) getView().findViewById(R.id.Name)).getText().toString());
        this.m_edit_user_info.setTown((int) ((Spinner) getView().findViewById(R.id.Town)).getSelectedItemId());
    }

    protected void updateOwnUserInfo() {
        Application.instance().getUser().setEmail(this.m_edit_user_info.getEmail());
        Application.instance().getUser().setBisness(this.m_edit_user_info.getBisness());
        Application.instance().getUser().setFamily(this.m_edit_user_info.getFamily());
        Application.instance().getUser().setName(this.m_edit_user_info.getName());
        Application.instance().getUser().setTown(this.m_edit_user_info.getTid());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0420\u0435\u0434\u0430\u043a\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0435\u0438 \u043f\u0440\u043e\u0444\u0438\u043b\u044f");
        View view = inflater.inflate(R.layout.profile_customer_edit_fragment, container, false);
        UserInfoBase userOwn = Application.instance().getUser();
        CitiesData cities = new CitiesData();
        int activeIndex = cities.getIndexById(userOwn.getTid(), userOwn.getCountry());
        ((Spinner) view.findViewById(R.id.Town)).setAdapter(new CitiesAdapter(inflater.getContext(), cities, userOwn.getCountry()));
        ((Spinner) view.findViewById(R.id.Town)).setSelection(activeIndex);
        ((TextView) view.findViewById(R.id.Email)).setText(userOwn.getEmail());
        ((TextView) view.findViewById(R.id.CompanyName)).setText(userOwn.getBisness());
        ((TextView) view.findViewById(R.id.Family)).setText(userOwn.getFamily());
        ((TextView) view.findViewById(R.id.Name)).setText(userOwn.getName());
        BitmapManager.instance().setImageView((ImageView) view.findViewById(R.id.Avatar), String.valueOf(userOwn.getId()), userOwn.getLastTimeUpdate());
        onInit(view);
        return view;
    }
}

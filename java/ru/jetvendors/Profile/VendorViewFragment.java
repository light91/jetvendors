package ru.jetvendors.Profile;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.Profile.Libs.RequestManager;
import ru.jetvendors.Profile.Libs.VendorReviewsAdapter;
import ru.jetvendors.R;
import ru.jetvendors.Data.CountriesData;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.ExternLibs.NotifyOperationComplete;
import ru.jetvendors.Data.ReviewsItem;
import ru.jetvendors.Data.ReviewsManager;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.Messages.TalkMessagesFragment;
import ru.jetvendors.Application.AccountType;

public class VendorViewFragment extends Fragment {
    private OnClickListener m_func_on_call;
    private NotifyOperationComplete m_func_on_get_reviews_count;
    private OnClickListener m_func_on_review_add;
    private OnItemClickListener m_func_select_reviews;
    private OnClickListener m_func_send_message;
    private NotifyDataSetChanged m_func_user_loaded = new NotifyDataSetChanged(null)
    {
        public void notifyDataSetChange() {
        updateView();
    }
    };

    private ReviewsManager m_review_data;

    /* renamed from: ru.jetvendors.Profile.VendorViewFragment.2 */
    class C03512 implements OnItemClickListener {
        C03512() {
        }

        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Fragment fragment;
            ReviewsItem review = (ReviewsItem) parent.getItemAtPosition(position);
            if (review.getUser().getAccountType() == AccountType.customer) {
                fragment = CustomerViewFragment.createInstance(review.getUser().getId());
            } else {
                fragment = VendorViewFragment.createInstance(review.getUser().getId());
            }
            ((BaseFragmentActivity) VendorViewFragment.this.getActivity()).fragmentForward(fragment);
        }
    }

    /* renamed from: ru.jetvendors.Profile.VendorViewFragment.4 */
    class C03524 implements OnClickListener {
        C03524() {
        }

        public void onClick(View v) {
            ((BaseFragmentActivity) VendorViewFragment.this.getActivity()).fragmentForward(VendorReviewAddFragment.createInstance(VendorViewFragment.this.getUid()));
        }
    }

    /* renamed from: ru.jetvendors.Profile.VendorViewFragment.5 */
    class C03535 implements OnClickListener {
        C03535() {
        }

        public void onClick(View v) {
            VendorViewFragment.this.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + (new CountriesData().getById(UsersManager.instance().getById(VendorViewFragment.this.getUid()).getCountry()).number + UsersManager.instance().getById(VendorViewFragment.this.getUid()).getPhone()))));
        }
    }

    /* renamed from: ru.jetvendors.Profile.VendorViewFragment.6 */
    class C03546 implements OnClickListener {
        C03546() {
        }

        public void onClick(View v) {
            Bundle bundle = new Bundle();
            bundle.putInt("interlocutor", VendorViewFragment.this.getUid());
            Fragment messagesFragment = new TalkMessagesFragment();
            messagesFragment.setArguments(bundle);
            ((BaseFragmentActivity) VendorViewFragment.this.getActivity()).fragmentForward(messagesFragment);
        }
    }

    /* renamed from: ru.jetvendors.Profile.VendorViewFragment.1 */
    class C05511 implements NotifyOperationComplete {
        C05511() {
        }

        public void onNotify(Object result) {
            ((TextView) VendorViewFragment.this.getView().findViewById(R.id.ReviewWithCountText)).setText("\u041e\u0442\u0437\u044b\u0432\u044b (" + ((Integer) result).intValue() + ")");
        }
    }

    public VendorViewFragment() {
        this.m_func_on_get_reviews_count = new C05511();
        this.m_func_select_reviews = new C03512();
        this.m_func_on_review_add = new C03524();
        this.m_func_on_call = new C03535();
        this.m_func_send_message = new C03546();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("myLogs-vendors", "1  getUid " + getUid());
        ((BaseUserActivity) getActivity()).setPageTitle("\u041f\u0440\u043e\u0441\u043c\u043e\u0442\u0440 \u043f\u0440\u043e\u0444\u0438\u043b\u044f");
        View view = inflater.inflate(R.layout.profile_vendor_view_fragment, container, false);
        Log.d("myLogs-vendors", "2  getUid " + getUid());
        view.findViewById(R.id.ReviewAdd).setOnClickListener(this.m_func_on_review_add);
        view.findViewById(R.id.Call).setOnClickListener(this.m_func_on_call);
        Log.d("myLogs-vendors", "3  getUid " + getUid());
        UsersManager.instance().setNotifyDataSetChangeListener(m_func_user_loaded);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        new RequestManager().get_reviews_count(getUid(), this.m_func_on_get_reviews_count);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        updateView();
    }

    protected void updateView()
    {
        Log.d("myLogs-vendors-0", " getUid() " + getUid() + " name: " + UsersManager.instance().getById(getUid()).getFullName() + " getView() == null " + (getView() == null) + " userLoaded " + UsersManager.instance().getById(getUid()).checkUserLoaded());
        if (getView() == null)
            return;

        if (!UsersManager.instance().getById(getUid()).checkUserLoaded())
            return;

        Log.d("myLogs-vendors-1", "2 " + UsersManager.instance().getById(getUid()).getFullName());
        ((TextView) getView().findViewById(R.id.FullName)).setText(UsersManager.instance().getById(getUid()).getFullName());
        ((TextView) getView().findViewById(R.id.Town)).setText(UsersManager.instance().getById(getUid()).getTown());
        ((TextView) getView().findViewById(R.id.BusinessType)).setText(UsersManager.instance().getById(getUid()).getBidString());
        ((TextView) getView().findViewById(R.id.FullCategory)).setText(UsersManager.instance().getById(getUid()).getUserCats().getCatsText());
        ((TextView) getView().findViewById(R.id.Email)).setText(UsersManager.instance().getById(getUid()).getEmail());
        ((TextView) getView().findViewById(R.id.About)).setText(UsersManager.instance().getById(getUid()).getAbout());
        BitmapManager.instance().setImageView((ImageView) getView().findViewById(R.id.Avatar), String.valueOf(UsersManager.instance().getById(getUid()).getId()), UsersManager.instance().getById(getUid()).getLastTimeUpdate());
        initReviews();
    }

    protected void initReviews() {
        if (this.m_review_data == null) {
            this.m_review_data = new ReviewsManager(getUid());
            VendorReviewsAdapter adapter = new VendorReviewsAdapter(getView().getContext(), this.m_review_data);
            this.m_review_data.setNotifyDataSetChanged(new NotifyDataSetChanged(adapter));
            ((ListView) getView().findViewById(R.id.Offers)).setAdapter(adapter);
            ((ListView) getView().findViewById(R.id.Offers)).setOnItemClickListener(this.m_func_select_reviews);
            getView().findViewById(R.id.SendMessage).setOnClickListener(this.m_func_send_message);
            this.m_review_data.requestItems(new Bundle());
        }
    }

    public static VendorViewFragment createInstance(int uid) {
        VendorViewFragment fragment = new VendorViewFragment();
        fragment.setArguments(new Bundle());
        fragment.getArguments().putInt("uid", uid);
        return fragment;
    }

    private int getUid() {
        return getArguments().getInt("uid");
    }
}

package ru.jetvendors.Profile;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Profile.Libs.RequestManager;
import ru.jetvendors.R;

public class VendorReviewAddFragment extends Fragment {
    private OnClickListener m_func_on_send_review;
    private OnSendReviewResult m_func_on_send_review_result;
    private RequestManager m_request_manager;

    /* renamed from: ru.jetvendors.Profile.VendorReviewAddFragment.1 */
    class C03491 implements OnClickListener {
        C03491() {
        }

        public void onClick(View v) {
            VendorReviewAddFragment.this.m_request_manager.add_review(VendorReviewAddFragment.this.getUid(), ((EditText) VendorReviewAddFragment.this.getView().findViewById(R.id.ReviewText)).getText().toString(), VendorReviewAddFragment.this.m_func_on_send_review_result);
        }
    }

    public interface OnSendReviewResult {
        void onError();

        void onSuccess();
    }

    /* renamed from: ru.jetvendors.Profile.VendorReviewAddFragment.2 */
    class C05502 implements OnSendReviewResult {
        C05502() {
        }

        public void onSuccess() {
            ((BaseFragmentActivity) VendorReviewAddFragment.this.getActivity()).fragmentForward(VendorViewFragment.createInstance(VendorReviewAddFragment.this.getUid()));
        }

        public void onError() {
        }
    }

    public VendorReviewAddFragment() {
        this.m_func_on_send_review = new C03491();
        this.m_func_on_send_review_result = new C05502();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u043e\u0442\u0437\u044b\u0432\u0430");
        return inflater.inflate(R.layout.profile_vendor_review_add_fragment, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.m_request_manager = new RequestManager();
        getView().findViewById(R.id.SendReview).setOnClickListener(this.m_func_on_send_review);
    }

    public static VendorReviewAddFragment createInstance(int uid) {
        VendorReviewAddFragment fragment = new VendorReviewAddFragment();
        fragment.setArguments(new Bundle());
        fragment.getArguments().putInt("uid", uid);
        return fragment;
    }

    private int getUid() {
        return getArguments().getInt("uid");
    }
}

package ru.jetvendors.Profile.Libs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.R;
import ru.jetvendors.Data.ReviewsItem;
import ru.jetvendors.Data.ReviewsManager;

public class VendorReviewsAdapter extends BaseAdapter {
    private Context m_context;
    private final ReviewsManager m_data;

    public VendorReviewsAdapter(Context context, ReviewsManager data) {
        this.m_context = context;
        this.m_data = data;
    }

    public int getCount() {
        return this.m_data.getCount();
    }

    public ReviewsItem getItem(int position) {
        return (ReviewsItem) this.m_data.get(position);
    }

    public long getItemId(int position) {
        return (long) ((ReviewsItem) this.m_data.get(position)).getId();
    }

    public View getView(int position, View contentView, ViewGroup parent) {
        if (contentView == null) {
            contentView = ((LayoutInflater) this.m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.profile_vendor_review_item, parent, false);
        }
        ReviewsItem review = getItem(position);
        ((TextView) contentView.findViewById(R.id.FullName)).setText(review.getUser().getFullName());
        ((TextView) contentView.findViewById(R.id.Town)).setText(review.getUser().getTown());
        ((TextView) contentView.findViewById(R.id.ReviewText)).setText(review.getReview());
        BitmapManager.instance().setImageView((ImageView) contentView.findViewById(R.id.Avatar), String.valueOf(review.getUser().getId()), review.getUser().getLastTimeUpdate());
        return contentView;
    }
}

package ru.jetvendors.Profile.Libs;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.httpClient.VKHttpClient;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKScopes;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import ru.jetvendors.Application;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.ExternLibs.NotifyOperationComplete;
import ru.jetvendors.Data.ReviewsManager;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.Profile.BaseProfileEditFragment.OnUpdateProfileListener;
import ru.jetvendors.Profile.VendorReviewAddFragment.OnSendReviewResult;
import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class RequestManager extends BaseAsyncRequestManager {
    private static final String host_api = "http://qazaqtv.kz/JetVendors/api.php";

    class ResponseObjectAddReview extends ResponseObject {
        ResponseObjectAddReview() {
            super();
        }

        public void doResponse(Request requestData) {
            if (data().equals("1")) {
                ((OnSendReviewResult) requestData.getObject()).onSuccess();
            } else {
                ((OnSendReviewResult) requestData.getObject()).onError();
            }
        }
    }

    class ResponseObjectGetReviews extends ResponseObject {
        ResponseObjectGetReviews() {
            super();
        }

        public void doResponse(Request requestData) {
            ((ReviewsManager) requestData.getObject()).addArray((JsonData) getDecodeData().getObject().get(0));
        }
    }

    class ResponseObjectGetReviewsCount extends ResponseObject {
        ResponseObjectGetReviewsCount() {
            super();
        }

        public void doResponse(Request requestData) {
            ((NotifyOperationComplete) requestData.getObject()).onNotify(Integer.valueOf(Integer.valueOf(data()).intValue()));
        }
    }

    class ResponseObjectUpdateProfile extends ResponseObject {
        ResponseObjectUpdateProfile() {
            super();
        }

        public void doResponse(Request requestData) {
            if (data().equals("1")) {
                ((OnUpdateProfileListener) requestData.getObject()).onSuccess();
            } else {
                ((OnUpdateProfileListener) requestData.getObject()).onError();
            }
        }
    }

    public void update_profile(UserInfoBase userInfo, OnUpdateProfileListener onUpdateProfileListener)
    {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "update_profile");
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        requestData.addData("id", String.valueOf(userInfo.getId()));
        requestData.addData("bid", String.valueOf(userInfo.getId()));
        requestData.addData(VKScopes.EMAIL, String.valueOf(userInfo.getEmail()));
        requestData.addData("company_name", String.valueOf(userInfo.getBisness()));
        requestData.addData("family", String.valueOf(userInfo.getFamily()));
        requestData.addData("name", String.valueOf(userInfo.getName()));
        requestData.addData("tid", String.valueOf(userInfo.getTid()));
        requestData.addData(VKApiUserFull.ABOUT, String.valueOf(userInfo.getAbout()));


        requestData.setFileName(String.valueOf(userInfo.getId()));
        requestData.setFilePath(BitmapManager.instance().getImagePath(String.valueOf(userInfo.getId())));

        requestData.setUrl(host_api);
        requestData.setObject(onUpdateProfileListener);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectUpdateProfile())});
        addRequest(currentRequest);
    }

    public void get_reviews_count(int uid, NotifyOperationComplete onFuncGetReviewsCount) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_reviews_count");
        requestData.addData("uid", String.valueOf(uid));
        requestData.setUrl(host_api);
        requestData.setObject(onFuncGetReviewsCount);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetReviewsCount())});
        addRequest(currentRequest);
    }

    public void get_reviews(Bundle requestBundle, ReviewsManager reviewManager) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_reviews");
        requestData.addData("uid", requestBundle.getString("uid"));
        requestData.addData("last_id", requestBundle.getString("last_id"));
        requestData.addData(VKApiConst.COUNT, requestBundle.getString(VKApiConst.COUNT));
        requestData.setUrl(host_api);
        requestData.setObject(reviewManager);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetReviews())});
        addRequest(currentRequest);
    }

    public void add_review(int uid, String text, OnSendReviewResult onSendReviewResult) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "add_review");
        requestData.addData("uid", String.valueOf(uid));
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        requestData.addData("text", text);
        requestData.setUrl(host_api);
        requestData.setObject(onSendReviewResult);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectAddReview())});
        addRequest(currentRequest);
    }
}

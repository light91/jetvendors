package ru.jetvendors;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public abstract class BaseFragmentActivity extends AppCompatActivity
{
    private Fragment getTop()
    {
        Fragment topFragment = getFragmentManager().findFragmentById(R.id.fragment);
        return topFragment;
    }

    public void fragmentClear(Fragment fragment)
    {
        FragmentManager fragmentManager = getFragmentManager();
        while (fragmentManager.getBackStackEntryCount() > 0)
            fragmentManager.popBackStackImmediate();
        fragmentForward(fragment);
    }

    public int getBackStackEntryCount()
    {
        FragmentManager fragmentManager = getFragmentManager();
        return fragmentManager.getBackStackEntryCount();
    }

    public void fragmentSetup(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.fragment, fragment);
        transaction.commit();
    }

    public void fragmentForward(Fragment fragment)
    {
        hideKeyboard();

        Fragment topFragment = getTop();
        if (topFragment != null)
        {
            topFragment.getView().setVisibility(View.GONE);
        }
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment, fragment);
        fragmentTransaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        fragmentTransaction.show(fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        if (topFragment != null)
        {
            topFragment.getView().setVisibility(View.GONE);
            topFragment.onPause();
        }
    }

    public void fragmentBack()
    {
        hideKeyboard();

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1)
        {
            fragmentManager.popBackStackImmediate();
            Fragment topFragment = getFragmentManager().findFragmentById(R.id.fragment);
            if (topFragment != null)
            {
                topFragment.onStart();
                topFragment.getView().setVisibility(View.VISIBLE);
            }
            return;
        }
        moveTaskToBack(true);
    }

    public void activityBack()
    {
        finish();
    }

    public void onBackPressed()
    {
        fragmentBack();
    }

    public void hideKeyboard()
    {
        View focusView = getCurrentFocus();
        if (focusView != null)
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
        }
    }
}

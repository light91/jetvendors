package ru.jetvendors.Offers;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import ru.jetvendors.Application;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Offers.Libs.CallbackRequest;
import ru.jetvendors.R;
import ru.jetvendors.Data.OffersItem;

public class EditMultiFragment extends AddMultiFragment {
    private OnClickListener m_func_on_done;
    private CallbackRequest m_func_on_offer_update;

    /* renamed from: ru.jetvendors.Offers.EditMultiFragment.2 */
    class C03372 implements OnClickListener {
        C03372() {
        }

        public void onClick(View v) {
            OffersItem offer = EditMultiFragment.this.getFilledOfferData();
            offer.setId(EditMultiFragment.this.getOid());
            if (offer.checkFilled()) {
                EditMultiFragment.this.m_request_manager.update_offer(offer, EditMultiFragment.this.m_func_on_offer_update);
            } else {
                Toast.makeText(EditMultiFragment.this.getView().getContext(), "\u0422\u0440\u0435\u0431\u0443\u0435\u0442\u0441\u044f \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u0437\u0430\u043a\u0430\u0437!", 0).show();
            }
        }
    }

    /* renamed from: ru.jetvendors.Offers.EditMultiFragment.1 */
    class C05471 implements CallbackRequest {
        C05471() {
        }

        public void onSuccess() {
            Toast.makeText(EditMultiFragment.this.getView().getContext(), "\u0417\u0430\u043a\u0430\u0437 \u0438\u0437\u043c\u0435\u043d\u0435\u043d", 0).show();
        }

        public void onError() {
            Toast.makeText(EditMultiFragment.this.getView().getContext(), "\u041e\u0448\u0438\u0431\u043a\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f \u0437\u0430\u043a\u0430\u0437\u0430", 0).show();
        }
    }

    public EditMultiFragment() {
        this.m_func_on_offer_update = new C05471();
        this.m_func_on_done = new C03372();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        setFilledOffersData((OffersItem) Application.instance().getOffersManager().getById(getOid()));
        View view = super.onCreateView(inflater, parent, savedInstanceState);
        ((BaseUserActivity) getActivity()).setPageTitle("\u0420\u0435\u0434\u0430\u043a\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u0437\u0430\u043a\u0430\u0437\u0430");
        view.findViewById(R.id.Done).setOnClickListener(this.m_func_on_done);
        return view;
    }

    public static Fragment createInstance(int oid) {
        Fragment fragment = new EditMultiFragment();
        fragment.setArguments(new Bundle());
        fragment.getArguments().putInt("oid", oid);
        return fragment;
    }

    public int getOid() {
        return getArguments().getInt("oid", -1);
    }
}

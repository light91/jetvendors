package ru.jetvendors.Offers.Libs;

import java.util.ArrayList;
import java.util.List;

public class UnitsData {
    private List<Unit> m_data;

    public class Unit {
        public int id;
        public String name;
    }

    public UnitsData() {
        this.m_data = new ArrayList();
        init();
    }

    private void init() {
        add(0, "г");
        add(1, "кг");
        add(2, "ц");
        add(3, "т");
        add(4, "мг");
        add(5, "л");
        add(6, "мл");
        add(7, "м");
        add(8, "дм");
        add(9, "см");
        add(10, "мм");
        add(11, "штука");
        add(12, "пачка");
        add(13, "коробка");
        add(14, "ящик");
        add(14, "бутылка");
        add(14, "блок");
    }

    public void add(int id, String name) {
        Unit unit = new Unit();
        unit.id = id;
        unit.name = name;
        this.m_data.add(unit);
    }

    public int getCount() {
        return this.m_data.size();
    }

    public Unit get(int index) {
        return (Unit) this.m_data.get(index);
    }
}

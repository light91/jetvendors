package ru.jetvendors.Offers.Libs;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import ru.jetvendors.Application;
import ru.jetvendors.BuildConfig;
import ru.jetvendors.R;
import ru.jetvendors.Data.FullCategoriesDB;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersProductItem;
import ru.jetvendors.SearchVendors.Libs.ListAdapterEx;
import ru.jetvendors.SearchVendors.Libs.ListUnits;
import ru.jetvendors.UserCategories.Libs.CategoriesAdapter;

public class AddMultiAdapter extends BaseAdapter
{
    private OffersProductItem m_product;
    private Context m_context;
    private OffersItem m_data;
    private FullCategoriesDB m_db;
    private AlertDialog m_dialog_select_subcategory;
    private OnClickListener m_func_remove_category = new OnClickListener()
    {
        public void onClick(View v)
        {
            m_data.remove(((Integer) v.getTag()).intValue());
            notifyDataSetChanged();
        }
    };
    private OnClickListener m_func_remove_subcategory = new  OnClickListener()
    {
        public void onClick(View v)
        {
            getItem(((Integer) v.getTag()).intValue()).setSubCatId(-1);
            notifyDataSetChanged();
        }
    };
    private OnClickListener m_func_select_subcategory = new OnClickListener()
    {
        private OffersProductItem m_product;
        private OnItemClickListener m_listener = new OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id)
            {
                m_product.setSubCatId((int) id);
                notifyDataSetChanged();
                m_dialog_select_subcategory.hide();
            }
        };

        public void onClick(View v)
        {
            m_product = getItem(((Integer) v.getTag()).intValue());
            Builder dialogBuilder = new Builder(m_context);
            View dialogView = ((LayoutInflater) Application.instance().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.category_select_dialog, null);
            dialogView.findViewById(R.id.Done).setVisibility(View.GONE);
            ((ListView) dialogView.findViewById(R.id.CategoriesList)).setAdapter(new CategoriesAdapter(m_context, m_product.getCatId()));
            ((ListView) dialogView.findViewById(R.id.CategoriesList)).setOnItemClickListener(m_listener);
            dialogBuilder.setView(dialogView);
            m_dialog_select_subcategory = dialogBuilder.create();
            m_dialog_select_subcategory.show();
        }
    };

    private OnItemSelectedListener m_listener = new OnItemSelectedListener()
    {
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id)
        {
            m_product.setUnitId((int) id);
        }

        public void onNothingSelected(AdapterView<?> adapterView)
        {
        }
    };

    class TextWatcherChanged implements TextWatcher
    {
        protected int m_pos;
        protected OffersProductItem m_product;

        public TextWatcherChanged(int pos, OffersProductItem product)
        {
            m_pos = pos;
            m_product = product;
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count)
        {
        }

        public void afterTextChanged(Editable s)
        {
        }
    }

    class OfferTotalChanged extends TextWatcherChanged
    {
        public OfferTotalChanged(int pos, OffersProductItem product)
        {
            super(pos, product);
        }

        public void afterTextChanged(Editable s)
        {
            super.afterTextChanged(s);
            int count = s.length() == 0 ? 0 : Integer.valueOf(s.toString()).intValue();
            m_product.setCount(count);
            Log.d("myLogs-addm-multi", " position " + String.valueOf(this.m_pos) + " product.getCount " + String.valueOf(count));
        }
    }

    class ProductPriceChanged extends TextWatcherChanged
    {
        public ProductPriceChanged(int pos, OffersProductItem product)
        {
            super(pos, product);
        }

        public void afterTextChanged(Editable s)
        {
            super.afterTextChanged(s);
            m_product.setPrice(s.toString());
        }
    }

    public AddMultiAdapter(Context context, OffersItem data)
    {
        m_context = context;
        m_data = data;
        m_db = new FullCategoriesDB();
    }

    protected void finalize() throws Throwable
    {
        m_db.close();
        super.finalize();
    }

    public int getCount()
    {
        return m_data.getCount();
    }

    public OffersProductItem getItem(int position)
    {
        return m_data.get(position);
    }

    public long getItemId(int position)
    {
        return (long) position;
    }

    public View getView(int position, View contentView, ViewGroup parent)
    {
        m_product = getItem(position);
        Log.d("myLogs-addm-multi", " position " + position + " product.getCount " + String.valueOf(m_product.getCount()));
        contentView = ((LayoutInflater) Application.instance().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.offers_multi_item, parent, false);
        ((TextView) contentView.findViewById(R.id.Category)).setText(m_db.getById(m_product.getCatId()).name);
        if (m_product.getSubCatId() != -1)
            ((TextView) contentView.findViewById(R.id.SubCategory)).setText(m_db.getById(m_product.getSubCatId()).name);

        contentView.findViewById(R.id.RemoveCategory).setTag(Integer.valueOf(position));
        contentView.findViewById(R.id.RemoveCategory).setOnClickListener(m_func_remove_category);
        contentView.findViewById(R.id.RemoveSubCategory).setTag(Integer.valueOf(position));
        contentView.findViewById(R.id.RemoveSubCategory).setOnClickListener(m_func_remove_subcategory);
        contentView.findViewById(R.id.Select).setTag(Integer.valueOf(position));
        contentView.findViewById(R.id.Select).setOnClickListener(m_func_select_subcategory);
        contentView.findViewById(R.id.Select).setVisibility(m_product.getSubCatId() != -1 ? 8 : 0);
        contentView.findViewById(R.id.SubCategory).setVisibility(m_product.getSubCatId() != -1 ? 0 : 8);
        contentView.findViewById(R.id.RemoveSubCategory).setVisibility(m_product.getSubCatId() != -1 ? 0 : 8);
        contentView.findViewById(R.id.InnerFields).setVisibility(m_product.getSubCatId() != -1 ? 0 : 8);
        ((EditText) contentView.findViewById(R.id.OfferTotal)).setText((m_product.getCount() > 0) ? String.valueOf(m_product.getCount()) : "");
        ((EditText) contentView.findViewById(R.id.OfferTotal)).addTextChangedListener(new OfferTotalChanged(position, m_product));
        ListUnits listUnits = new ListUnits();
        listUnits.addHeaderValue("\u0412\u044b\u0431\u0438\u0440\u0438\u0442\u0435 \u0432\u0435\u043b\u0447\u0438\u043d\u0443", -1);
        int selection = listUnits.getIndexById(m_product.getCountId());
        ((Spinner) contentView.findViewById(R.id.OfferUnit)).setAdapter(new ListAdapterEx(((LayoutInflater) Application.instance().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).getContext(), listUnits));
        ((Spinner) contentView.findViewById(R.id.OfferUnit)).setSelection(selection);
        ((Spinner) contentView.findViewById(R.id.OfferUnit)).setOnItemSelectedListener(m_listener);
        ((EditText) contentView.findViewById(R.id.Price)).setText(m_product.getPrice());
        ((EditText) contentView.findViewById(R.id.Price)).addTextChangedListener(new ProductPriceChanged(position, m_product));
        return contentView;
    }
}

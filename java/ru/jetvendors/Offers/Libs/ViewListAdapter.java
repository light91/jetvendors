package ru.jetvendors.Offers.Libs;

import android.content.Context;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import ru.jetvendors.BuildConfig;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.R;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersManager;
import ru.jetvendors.Data.OffersProductItem;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.UserCategories.Libs.CategoriesSpecial;

public class ViewListAdapter extends BaseAdapter {
    private Context m_context;
    private OffersManager m_data;

    public ViewListAdapter(Context context, OffersManager data) {
        this.m_context = context;
        this.m_data = data;
    }

    public Pair<Integer, Integer> getIndexesByPosition(int position) {
        int offersCount = this.m_data.getCount();
        int productsIndex = 0;
        for (int offerIndex = 0; offerIndex != offersCount; offerIndex++) {
            OffersItem offer = (OffersItem) this.m_data.get(offerIndex);
            if (position - productsIndex < offer.getCount()) {
                return new Pair(Integer.valueOf(offerIndex), Integer.valueOf(position - productsIndex));
            }
            productsIndex += offer.getCount();
        }
        return null;
    }

    public int getCount() {
        int productsCount = 0;
        for (int index = 0; index != this.m_data.getCount(); index++) {
            productsCount += ((OffersItem) this.m_data.get(index)).getCount();
        }
        Log.d("myLogs", "productsCount" + productsCount);
        return productsCount;
    }

    public OffersProductItem getItem(int position) {
        int offersCount = this.m_data.getCount();
        Pair<Integer, Integer> indexes = getIndexesByPosition(position);
        OffersItem offer = (OffersItem) this.m_data.get(((Integer) indexes.first).intValue());
        if (offer == null) {
            return null;
        }
        return (OffersProductItem) offer.get(((Integer) indexes.second).intValue());
    }

    private OffersItem getGroupItem(int position) {
        int offersCount = this.m_data.getCount();
        return (OffersItem) this.m_data.get(((Integer) getIndexesByPosition(position).first).intValue());
    }

    private boolean isFirstItem(int position) {
        return ((Integer) getIndexesByPosition(position).second).intValue() == 0;
    }

    private boolean isLastItem(int position) {
        return getGroupItem(position).getCount() == ((Integer) getIndexesByPosition(position).second).intValue();
    }

    public long getItemId(int position) {
        return (long) getGroupItem(position).getId();
    }

    private boolean isSingleOffer(int position) {
        if (getGroupItem(position).getCount() == 1) {
            return true;
        }
        return false;
    }

    public View getView(int position, View contentView, ViewGroup parent) {
        Log.d("myLogs", "ViewListAdapter");
        if (contentView == null) {
            contentView = ((LayoutInflater) this.m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.offers_view_item, parent, false);
        }
        OffersItem offer = getGroupItem(position);
        OffersProductItem product = getItem(position);
        UserInfoBase user = UsersManager.instance().getById(offer.getExtra().getUid());
        ((TextView) contentView.findViewById(R.id.FullName)).setText(user.getFullName());
        ((TextView) contentView.findViewById(R.id.Time)).setText(offer.getVisibleTime());
        List<Integer> cats = new ArrayList();
        cats.add(Integer.valueOf(product.getCatId()));
        cats.add(Integer.valueOf(product.getSubCatId()));
        Log.d("myLogs-catsId", " product.getCatId() " + product.getCatId() + " product.getSubCatId() " + product.getSubCatId());
        ((TextView) contentView.findViewById(R.id.Categories)).setText(CategoriesSpecial.getCatsText(cats, ", ", ": ", "\n"));
        UnitsData units = new UnitsData();
        String text = BuildConfig.FLAVOR + product.getCount();
        if (product.getCountId() >= 0) {
            text = text + " " + units.get(product.getCountId()).name;
        }
        ((TextView) contentView.findViewById(R.id.Units)).setText(text);
        ((TextView) contentView.findViewById(R.id.Price)).setText(product.getPrice());
        ((TextView) contentView.findViewById(R.id.About)).setText(product.getAbout());
        BitmapManager.instance().setImageView((ImageView) contentView.findViewById(R.id.Avatar), String.valueOf(user.getId()), user.getLastTimeUpdate());
        return contentView;
    }
}

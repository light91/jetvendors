package ru.jetvendors.Offers.Libs;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.text.SimpleDateFormat;

import ru.jetvendors.Application;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.Offers.EditMultiFragment;
import ru.jetvendors.Offers.EditSingleFragment;
import ru.jetvendors.R;
import ru.jetvendors.Data.FullCategoriesDB;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersManager;
import ru.jetvendors.Data.OffersProductItem;

public class EditAdapter extends BaseAdapter {
    private BaseFragmentActivity m_activity;
    private Context m_context;
    private OffersManager m_data;
    private FullCategoriesDB m_db;
    private OnClickListener m_func_on_edit;
    private OnClickListener m_func_on_remove;
    private OnClickListener m_func_on_update_time;
    RequestManager m_request_manager;

    /* renamed from: ru.jetvendors.Offers.Libs.EditAdapter.1 */
    class C03341 implements OnClickListener {
        C03341() {
        }

        public void onClick(View v) {
            EditAdapter.this.m_request_manager.update_offer_time(((Integer) v.getTag()).intValue());
        }
    }

    /* renamed from: ru.jetvendors.Offers.Libs.EditAdapter.2 */
    class C03352 implements OnClickListener {
        C03352() {
        }

        public void onClick(View v) {
            EditAdapter.this.m_request_manager.remove_offer(((Integer) v.getTag()).intValue());
        }
    }

    /* renamed from: ru.jetvendors.Offers.Libs.EditAdapter.3 */
    class C03363 implements OnClickListener {
        C03363() {
        }

        public void onClick(View v) {
            int oid = ((Integer) v.getTag()).intValue();
            if (((OffersItem) Application.instance().getOffersManager().getById(oid)).getCount() == 1) {
                EditAdapter.this.m_activity.fragmentForward(EditSingleFragment.createInstance(oid));
            } else {
                EditAdapter.this.m_activity.fragmentForward(EditMultiFragment.createInstance(oid));
            }
        }
    }

    public EditAdapter(BaseFragmentActivity activity, OffersManager offersData) {
        this.m_func_on_update_time = new C03341();
        this.m_func_on_remove = new C03352();
        this.m_func_on_edit = new C03363();
        this.m_activity = activity;
        this.m_context = activity.getBaseContext();
        this.m_data = offersData;
        this.m_db = new FullCategoriesDB();
        this.m_request_manager = new RequestManager();
    }

    protected void finalize() throws Throwable {
        this.m_db.close();
        super.finalize();
    }

    public Pair<Integer, Integer> getIndexesByPosition(int position) {
        int offersCount = this.m_data.getCount();
        int productsIndex = 0;
        for (int offerIndex = 0; offerIndex != offersCount; offerIndex++) {
            OffersItem offer = (OffersItem) this.m_data.get(offerIndex);
            if (position - productsIndex < offer.getCount()) {
                return new Pair(Integer.valueOf(offerIndex), Integer.valueOf(position - productsIndex));
            }
            productsIndex += offer.getCount();
        }
        return null;
    }

    public int getCount() {
        int productsCount = 0;
        for (int index = 0; index != this.m_data.getCount(); index++) {
            productsCount += ((OffersItem) this.m_data.get(index)).getCount();
        }
        return productsCount;
    }

    public OffersProductItem getItem(int position) {
        int offersCount = this.m_data.getCount();
        Pair<Integer, Integer> indexes = getIndexesByPosition(position);
        OffersItem offer = (OffersItem) this.m_data.get(((Integer) indexes.first).intValue());
        if (offer == null) {
            return null;
        }
        return (OffersProductItem) offer.get(((Integer) indexes.second).intValue());
    }

    private OffersItem getGroupItem(int position) {
        int offersCount = this.m_data.getCount();
        return (OffersItem) this.m_data.get(((Integer) getIndexesByPosition(position).first).intValue());
    }

    private boolean isLastItem(int position) {
        return getGroupItem(position).getCount() + -1 == ((Integer) getIndexesByPosition(position).second).intValue();
    }

    private boolean isSingleOffer(int position) {
        if (getGroupItem(position).getCount() == 1) {
            return true;
        }
        return false;
    }

    public long getItemId(int position) {
        return (long) getGroupItem(position).getId();
    }

    public View getView(int position, View contentView, ViewGroup parent) {
        if (contentView == null) {
            contentView = ((LayoutInflater) this.m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.offers_edit_item, parent, false);
        }
        OffersItem offer = getGroupItem(position);
        OffersProductItem product = getItem(position);
        String sCatsText = this.m_db.getById(product.getCatId()).name;
        if (product.getSubCatId() != -1) {
            sCatsText = sCatsText + " " + this.m_db.getById(product.getSubCatId()).name;
        }
        ((TextView) contentView.findViewById(R.id.Cats)).setText(sCatsText);
        ((TextView) contentView.findViewById(R.id.Time)).setText(new SimpleDateFormat("dd.MM.yyyy HH:mm ").format(offer.getExtra().getTime()));
        String sCount = String.valueOf(product.getCount());
        if (product.getCountId() != -1) {
            sCount = sCount + " " + new UnitsData().get(product.getCountId()).name;
        }
        ((TextView) contentView.findViewById(R.id.Count)).setText(sCount);
        ((TextView) contentView.findViewById(R.id.Price)).setText(String.valueOf(product.getPrice()));
        ((TextView) contentView.findViewById(R.id.About)).setText(String.valueOf(product.getAbout()));
        contentView.findViewById(R.id.About).setVisibility(isSingleOffer(position) ? 0 : 8);
        contentView.findViewById(R.id.BottomButtons).setVisibility(isLastItem(position) ? 0 : 8);
        contentView.findViewById(R.id.Remove).setTag(Integer.valueOf(offer.getId()));
        contentView.findViewById(R.id.Remove).setOnClickListener(this.m_func_on_remove);
        contentView.findViewById(R.id.Edit).setTag(Integer.valueOf(offer.getId()));
        contentView.findViewById(R.id.Edit).setOnClickListener(this.m_func_on_edit);
        contentView.findViewById(R.id.UpdateTime).setTag(Integer.valueOf(offer.getId()));
        contentView.findViewById(R.id.UpdateTime).setOnClickListener(this.m_func_on_update_time);
        return contentView;
    }
}

package ru.jetvendors.Offers.Libs;

import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import com.vk.sdk.api.VKApiConst;

import ru.jetvendors.Application;
import ru.jetvendors.BuildConfig;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersManager;
import ru.jetvendors.Data.OffersProductItem;
import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;
import ru.jetvendors.Offers.Libs.CallbackRequest;

public class RequestManager extends BaseAsyncRequestManager {
    private static final String host_api = "http://qazaqtv.kz/JetVendors/api.php";

    class ResponseObjectAddOffer extends ResponseObject {
        ResponseObjectAddOffer() {
            super();
        }

        public void doResponse(Request requestData) {
            Pair<CallbackRequest, OffersItem> pairData = (Pair) requestData.getObject();
            if (data().length() > 0) {
                Bundle bundle = new Bundle();
                bundle.putString("uid", String.valueOf(Application.instance().getUserExtra().getUid()));
                Application.instance().getOffersManager().requestItems(bundle);
                ((CallbackRequest) pairData.first).onSuccess();
                return;
            }
            ((CallbackRequest) pairData.first).onError();
        }
    }

    class ResponseObjectGetOffer extends ResponseObject {
        ResponseObjectGetOffer() {
            super();
        }

        public void doResponse(Request requestData) {
            ((OffersManager) requestData.getObject()).addArray((JsonData) getDecodeData().getObject().get(0));
        }
    }

    class ResponseObjectGetOffers extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
            Log.d("myLogs-GetOffers", "data: " + data());
            ((OffersManager) requestData.getObject()).addArray(getDecodeData().getObject().get(0));
        }
    }

    class ResponseObjectRemove extends ResponseObject {
        ResponseObjectRemove() {
            super();
        }

        public void doResponse(Request requestData) {
            int oid = ((Integer) requestData.getObject()).intValue();
            new Bundle().putString("uid", String.valueOf(Application.instance().getUserExtra().getUid()));
            Application.instance().getOffersManager().removeById(oid);
        }
    }

    class ResponseObjectUpdateOffer extends ResponseObject {
        ResponseObjectUpdateOffer() {
            super();
        }

        public void doResponse(Request requestData) {
            Pair<CallbackRequest, OffersItem> pairData = (Pair) requestData.getObject();
            if (data().equals("1")) {
                Bundle bundle = new Bundle();
                bundle.putString("uid", String.valueOf(Application.instance().getUserExtra().getUid()));
                Application.instance().getOffersManager().requestItems(bundle);
                ((CallbackRequest) pairData.first).onSuccess();
                return;
            }
            ((CallbackRequest) pairData.first).onError();
        }
    }

    class ResponseObjectUpdateTime extends ResponseObject {
        ResponseObjectUpdateTime() {
            super();
        }

        public void doResponse(Request requestData) {
            Bundle bundle = new Bundle();
            bundle.putString("uid", String.valueOf(Application.instance().getUserExtra().getUid()));
            Application.instance().getOffersManager().requestItems(bundle);
        }
    }

    public void add_offer(int sid, OffersItem offer, CallbackRequest callback) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "add_offer");
        for (int index = 0; index != offer.getCount(); index++) {
            OffersProductItem offerPart = (OffersProductItem) offer.get(index);
            requestData.addData("cat_id[" + index + "]", String.valueOf(offerPart.getCatId()));
            requestData.addData("sub_cat_id[" + index + "]", String.valueOf(offerPart.getSubCatId()));
            requestData.addData("count[" + index + "]", String.valueOf(offerPart.getCount()));
            requestData.addData("count_id[" + index + "]", String.valueOf(offerPart.getCountId()));
            requestData.addData("price[" + index + "]", String.valueOf(offerPart.getPrice()));
            requestData.addData("about[" + index + "]", String.valueOf(offerPart.getAbout()));
        }
        requestData.addData("sid", String.valueOf(sid));
        requestData.setUrl(host_api);
        requestData.setObject(new Pair(callback, offer));
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectAddOffer())});
        addRequest(currentRequest);
    }

    public void get_offer(int oid, OffersManager offersManager) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_offer_by_oid");
        requestData.addData("oid", String.valueOf(oid));
        requestData.setUrl(host_api);
        requestData.setObject(offersManager);
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String message = BuildConfig.FLAVOR;
        if (stackTraceElements.length >= 3) {
            StackTraceElement element = stackTraceElements[2];
            String className = element.getClassName();
            message = className + ": " + element.getMethodName();
        }
        Log.d("myLogs-stack", message);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetOffer())});
        addRequest(currentRequest);
    }

    public void update_offer(OffersItem offer, CallbackRequest callback) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "update_offer");
        requestData.addData("oid", String.valueOf(offer.getId()));
        for (int index = 0; index != offer.getCount(); index++) {
            OffersProductItem offerPart = (OffersProductItem) offer.get(index);
            requestData.addData("cat_id[" + index + "]", String.valueOf(offerPart.getCatId()));
            requestData.addData("sub_cat_id[" + index + "]", String.valueOf(offerPart.getSubCatId()));
            requestData.addData("count[" + index + "]", String.valueOf(offerPart.getCount()));
            requestData.addData("count_id[" + index + "]", String.valueOf(offerPart.getCountId()));
            requestData.addData("price[" + index + "]", String.valueOf(offerPart.getPrice()));
            requestData.addData("about[" + index + "]", String.valueOf(offerPart.getAbout()));
        }
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        requestData.setUrl(host_api);
        requestData.setObject(new Pair(callback, offer));
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectUpdateOffer())});
        addRequest(currentRequest);
    }

    public void remove_offer(int oid) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "remove_offer");
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        requestData.addData("oid", String.valueOf(oid));
        requestData.setUrl(host_api);
        requestData.setObject(Integer.valueOf(oid));
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectRemove())});
        addRequest(currentRequest);
    }

    public void update_offer_time(int oid) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "update_offer_time");
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        requestData.addData("oid", String.valueOf(oid));
        requestData.setUrl(host_api);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectUpdateTime())});
        addRequest(currentRequest);
    }

    public void get_user_offers(int uid, OffersManager offersManager) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_user_offers");
        requestData.addData("uid", String.valueOf(uid));
        requestData.setUrl(host_api);
        requestData.setObject(offersManager);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetOffers())});
        addRequest(currentRequest);
    }

    public void get_offers(int tid, int cid, int sub_cat_id, int from_id, int count)
    {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_offers");
        requestData.addData("tid", String.valueOf(tid));
        requestData.addData("cid", String.valueOf(cid));
        requestData.addData("sub_cid", String.valueOf(sub_cat_id));
        requestData.addData("from_id", String.valueOf(from_id));
        requestData.addData(VKApiConst.COUNT, String.valueOf(count));
        requestData.setUrl(host_api);
        requestData.setObject(Application.instance().getOffersManager());
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetOffers())});
        addRequest(currentRequest);
    }
}

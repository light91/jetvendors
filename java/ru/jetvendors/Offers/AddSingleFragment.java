package ru.jetvendors.Offers;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Offers.Libs.CallbackRequest;
import ru.jetvendors.Offers.Libs.RequestManager;
import ru.jetvendors.R;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersProductItem;
import ru.jetvendors.SearchVendors.Libs.ListAdapterEx;
import ru.jetvendors.SearchVendors.Libs.ListCategories;
import ru.jetvendors.SearchVendors.Libs.ListUnits;
import ru.jetvendors.Application;

public class AddSingleFragment extends Fragment
{
    private OnClickListener m_func_on_done = new OnClickListener()
    {
        public void onClick(View v)
        {
            OffersItem offer = AddSingleFragment.this.getFilledOfferData();
            if (offer.checkFilled())
            {
                m_request_manager.add_offer(Application.instance().getUserExtra().getSid(), offer, m_func_on_offer_add);
            }
            else
            {
                Toast.makeText(AddSingleFragment.this.getView().getContext(), "\u0422\u0440\u0435\u0431\u0443\u0435\u0442\u0441\u044f \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u0437\u0430\u043a\u0430\u0437!", Toast.LENGTH_LONG).show();
            }
        }
    };
    private CallbackRequest m_func_on_offer_add = new CallbackRequest()
    {
        public void onSuccess()
        {
            ((BaseFragmentActivity) getActivity()).fragmentClear(new EditFragment());
            Toast.makeText(AddSingleFragment.this.getView().getContext(), "\u0412\u0430\u0448 \u0437\u0430\u043a\u0430\u0437 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d.", Toast.LENGTH_SHORT).show();
        }

        public void onError()
        {
            Toast.makeText(AddSingleFragment.this.getView().getContext(), "\u041f\u0440\u043e\u0438\u0437\u043e\u0448\u043b\u0430 \u043e\u0448\u0438\u0431\u043a\u0430 \u043f\u0440\u0438 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u0438 \u0437\u0430\u043a\u0430\u0437\u0430.", Toast.LENGTH_LONG).show();
        }
    };
    private OnItemSelectedListener m_func_on_select_category = new OnItemSelectedListener()
    {
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id)
        {
            AddSingleFragment.this.m_list_subcategories.clear();
            AddSingleFragment.this.m_list_subcategories.initByPid((int) id);
        }

        public void onNothingSelected(AdapterView<?> adapterView)
        {
        }
    };
    private ListCategories m_list_subcategories;
    protected RequestManager m_request_manager;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u0437\u0430\u043a\u0430\u0437\u0430");
        this.m_request_manager = new RequestManager();
        View view = inflater.inflate(R.layout.offers_single_add_fragment, container, false);
        ListCategories listCategory = new ListCategories();
        listCategory.addHeaderValue("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044e", -1);
        ((Spinner) view.findViewById(R.id.Category)).setAdapter(new ListAdapterEx(inflater.getContext(), listCategory));
        ((Spinner) view.findViewById(R.id.Category)).setOnItemSelectedListener(m_func_on_select_category);
        m_list_subcategories = new ListCategories();
        m_list_subcategories.clear();
        m_list_subcategories.addHeaderValue("\u0412\u044b\u0431\u0438\u0440\u0438\u0442\u0435 \u043f\u043e\u0434\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044e", -1);
        ((Spinner) view.findViewById(R.id.SubCategory)).setAdapter(new ListAdapterEx(inflater.getContext(), m_list_subcategories));
        ListUnits listUnits = new ListUnits();
        listUnits.addHeaderValue("\u0412\u044b\u0431\u0435\u0440\u0438\u0442\u0435", -1);
        ((Spinner) view.findViewById(R.id.OfferUnit)).setAdapter(new ListAdapterEx(inflater.getContext(), listUnits));
        view.findViewById(R.id.Done).setOnClickListener(m_func_on_done);
        return view;
    }

    protected OffersItem getFilledOfferData()
    {
        OffersItem offer = new OffersItem();
        OffersProductItem product = new OffersProductItem();
        product.setCatId((int) ((Spinner) getView().findViewById(R.id.Category)).getSelectedItemId());
        product.setSubCatId((int) ((Spinner) getView().findViewById(R.id.SubCategory)).getSelectedItemId());
        product.setCount(Integer.valueOf(((TextView) getView().findViewById(R.id.OfferTotal)).getText().toString()).intValue());
        product.setUnitId((int) ((Spinner) getView().findViewById(R.id.OfferUnit)).getSelectedItemId());
        product.setPrice(((EditText) getView().findViewById(R.id.Price)).getText().toString());
        product.setAbout(((EditText) getView().findViewById(R.id.About)).getText().toString());
        offer.add(product);
        return offer;
    }

    protected void onUpdateCategory(int cid)
    {
        m_list_subcategories.clear();
        m_list_subcategories.initByPid(cid);
    }
}

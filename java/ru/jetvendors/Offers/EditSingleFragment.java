package ru.jetvendors.Offers;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import ru.jetvendors.Application;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Offers.Libs.CallbackRequest;
import ru.jetvendors.R;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersProductItem;
import ru.jetvendors.SearchVendors.Libs.ListCategories;
import ru.jetvendors.SearchVendors.Libs.ListUnits;

public class EditSingleFragment extends AddSingleFragment {
    private OnClickListener m_func_on_done;
    private CallbackRequest m_func_on_offer_update;

    /* renamed from: ru.jetvendors.Offers.EditSingleFragment.2 */
    class C03382 implements OnClickListener {
        C03382() {
        }

        public void onClick(View v) {
            OffersItem offer = EditSingleFragment.this.getFilledOfferData();
            offer.setId(EditSingleFragment.this.getOid());
            if (offer.checkFilled()) {
                EditSingleFragment.this.m_request_manager.update_offer(offer, EditSingleFragment.this.m_func_on_offer_update);
            } else {
                Toast.makeText(EditSingleFragment.this.getView().getContext(), "\u0422\u0440\u0435\u0431\u0443\u0435\u0442\u0441\u044f \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u0437\u0430\u043a\u0430\u0437!", 0).show();
            }
        }
    }

    /* renamed from: ru.jetvendors.Offers.EditSingleFragment.1 */
    class C05481 implements CallbackRequest {
        C05481() {
        }

        public void onSuccess() {
            Toast.makeText(EditSingleFragment.this.getView().getContext(), "\u0417\u0430\u043a\u0430\u0437 \u0438\u0437\u043c\u0435\u043d\u0435\u043d", 0).show();
        }

        public void onError() {
            Toast.makeText(EditSingleFragment.this.getView().getContext(), "\u041e\u0448\u0438\u0431\u043a\u0430 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f \u0437\u0430\u043a\u0430\u0437\u0430", 0).show();
        }
    }

    public EditSingleFragment() {
        this.m_func_on_offer_update = new C05481();
        this.m_func_on_done = new C03382();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, parent, savedInstanceState);
        OffersProductItem product = (OffersProductItem) ((OffersItem) Application.instance().getOffersManager().getById(getOid())).get(0);
        ListCategories categoriesAdapter = new ListCategories();
        ((Spinner) view.findViewById(R.id.Category)).setSelection(categoriesAdapter.getIndexById(product.getCatId()) + 1);
        onUpdateCategory(product.getCatId());
        categoriesAdapter.initByPid(product.getCatId());
        ((Spinner) view.findViewById(R.id.SubCategory)).setSelection(categoriesAdapter.getIndexById(product.getSubCatId()) + 1);
        ((Spinner) view.findViewById(R.id.OfferUnit)).setSelection(new ListUnits().getIndexById(product.getCountId()) + 1);
        ((EditText) view.findViewById(R.id.OfferTotal)).setText(String.valueOf(product.getCount()));
        ((EditText) view.findViewById(R.id.Price)).setText(product.getPrice());
        ((EditText) view.findViewById(R.id.About)).setText(product.getAbout());
        ((BaseUserActivity) getActivity()).setPageTitle("\u0420\u0435\u0434\u0430\u043a\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u0437\u0430\u043a\u0430\u0437\u0430");
        view.findViewById(R.id.Done).setOnClickListener(this.m_func_on_done);
        return view;
    }

    public static Fragment createInstance(int oid) {
        Fragment fragment = new EditSingleFragment();
        fragment.setArguments(new Bundle());
        fragment.getArguments().putInt("oid", oid);
        return fragment;
    }

    public int getOid() {
        return getArguments().getInt("oid", -1);
    }
}

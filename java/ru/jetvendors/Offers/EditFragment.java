package ru.jetvendors.Offers;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import ru.jetvendors.Application;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Offers.Libs.EditAdapter;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Data.OffersManager;

public class EditFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u041c\u043e\u0438 \u0437\u0430\u043a\u0430\u0437\u044b");
        return inflater.inflate(R.layout.offers_edit_fragment, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        OffersManager offersManager = Application.instance().getOffersManager();
        BaseAdapter adapter = new EditAdapter((BaseFragmentActivity) getActivity(), offersManager);
        ((ListView) getView().findViewById(R.id.Offers)).setAdapter(adapter);
        offersManager.setNotifyDataSetChanged(new NotifyDataSetChanged(adapter));
    }
}

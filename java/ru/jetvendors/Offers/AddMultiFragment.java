package ru.jetvendors.Offers;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import ru.jetvendors.Application;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Offers.Libs.AddMultiAdapter;
import ru.jetvendors.Offers.Libs.CallbackRequest;
import ru.jetvendors.Offers.Libs.RequestManager;
import ru.jetvendors.R;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersProductItem;

public class AddMultiFragment extends Fragment
{
    private final int REQUEST_SELECT_CATEGORY = 100;
    private OnClickListener m_func_on_done = new OnClickListener()
    {
        public void onClick(View v)
        {
            if (m_offer.checkFilled())
            {
                m_request_manager.add_offer(Application.instance().getUserExtra().getSid(), m_offer, m_func_on_offer_add);
            }
            else
            {
                Toast.makeText(getView().getContext(), "\u0422\u0440\u0435\u0431\u0443\u0435\u0442\u0441\u044f \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u0437\u0430\u043a\u0430\u0437!", Toast.LENGTH_LONG).show();
            }
        }
    };
    private CallbackRequest m_func_on_offer_add = new CallbackRequest()
    {
        public void onSuccess()
        {
            ((BaseFragmentActivity) getActivity()).fragmentClear(new EditFragment());
            Toast.makeText(AddMultiFragment.this.getView().getContext(), "\u0417\u0430\u043a\u0430\u0437 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d", Toast.LENGTH_SHORT).show();
        }

        public void onError()
        {
            Toast.makeText(AddMultiFragment.this.getView().getContext(), "\u041e\u0448\u0438\u0431\u043a\u0430 \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f \u0437\u0430\u043a\u0430\u0437\u0430", Toast.LENGTH_LONG).show();
        }
    };
    private OffersItem m_offer;
    protected RequestManager m_request_manager;

    private OnClickListener m_func_select_category = new OnClickListener()
    {
        public void onClick(View v)
        {
            Fragment fragment = new SelectCategoryFragment();
            fragment.setTargetFragment(AddMultiFragment.this, REQUEST_SELECT_CATEGORY);
            ((BaseFragmentActivity) getActivity()).fragmentForward(fragment);
        }
    };


    public AddMultiFragment()
    {
        setFilledOffersData(new OffersItem());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u043c\u0443\u043b\u044c\u0442\u0438\u0437\u0430\u043a\u0430\u0437\u0430");
        m_request_manager = new RequestManager();
        View view = inflater.inflate(R.layout.offers_multi_add_fragment, container, false);
        view.findViewById(R.id.SelectCategory).setOnClickListener(m_func_select_category);
        ((ListView) view.findViewById(R.id.Offers)).setAdapter(new AddMultiAdapter(view.getContext(), m_offer));
        view.findViewById(R.id.Done).setOnClickListener(m_func_on_done);
        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if (requestCode == REQUEST_SELECT_CATEGORY && resultCode == -1)
        {
            int cat_id = intent.getIntExtra("cat_id", -1);
            OffersProductItem product = new OffersProductItem();
            product.setCatId(cat_id);
            m_offer.add(product);
            ((BaseAdapter) ((ListView) getView().findViewById(R.id.Offers)).getAdapter()).notifyDataSetChanged();
        }
    }

    protected void setFilledOffersData(OffersItem offerItem)
    {
        m_offer = offerItem;
    }

    protected OffersItem getFilledOfferData()
    {
        return m_offer;
    }
}

package ru.jetvendors.Offers;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.R;
import ru.jetvendors.UserCategories.Libs.CategoriesAdapter;

public class SelectCategoryFragment extends Fragment {

    /* renamed from: ru.jetvendors.Offers.SelectCategoryFragment.1 */
    class C03391 implements OnItemClickListener {
        C03391() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Intent intent = new Intent();
            intent.putExtras(new Bundle());
            intent.putExtra("cat_id", (int) id);
            Log.d("myLogs-cat_id", " id " + id);
            Log.d("myLogs-cat_id", " position " + position);
            SelectCategoryFragment.this.getTargetFragment().onActivityResult(SelectCategoryFragment.this.getTargetRequestCode(), -1, intent);
            ((BaseFragmentActivity) SelectCategoryFragment.this.getActivity()).fragmentBack();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u043c\u0443\u043b\u044c\u0442\u0438\u0437\u0430\u043a\u0430\u0437\u0430");
        View view = inflater.inflate(R.layout.offers_multi_select_category_fragment, container, false);
        ((ListView) view.findViewById(R.id.Select)).setAdapter(new CategoriesAdapter(inflater.getContext()));
        ((ListView) view.findViewById(R.id.Select)).setOnItemClickListener(new C03391());
        return view;
    }
}

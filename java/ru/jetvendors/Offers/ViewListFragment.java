package ru.jetvendors.Offers;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import ru.jetvendors.Application;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.Offers.Libs.RequestManager;
import ru.jetvendors.Offers.Libs.ViewListAdapter;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Data.OffersItem;
import ru.jetvendors.Data.OffersProductItem;
import ru.jetvendors.Profile.CustomerViewFragment;
import ru.jetvendors.SearchVendors.Libs.ListAdapterEx;
import ru.jetvendors.SearchVendors.Libs.ListCategories;
import ru.jetvendors.SearchVendors.Libs.ListCities;

public class ViewListFragment extends Fragment
{
    private OnClickListener m_func_on_request_more = new OnClickListener()
    {
        public void onClick(View v)
        {
            int townId = (int) ((Spinner) getView().findViewById(R.id.Town)).getSelectedItemId();
            int categoryId = (int) ((Spinner) getView().findViewById(R.id.Category)).getSelectedItemId();
            int subCategoryId = (int) ((Spinner) getView().findViewById(R.id.SubCategory)).getSelectedItemId();
            if (Application.instance().getOffersManager().getCount() != 0)
            {
                int offerLast = Application.instance().getOffersManager().getCount() - 1;
                int offerLastId = Application.instance().getOffersManager().get(offerLast).get(Application.instance().getOffersManager().get(offerLast).getCount() - 1).getId();
                Log.d("myLogs-spinner", "m_func_on_request_more!");
                m_request_manager.get_offers(townId, categoryId, subCategoryId, offerLastId, 20);
            }
        }
    };
    private OnItemSelectedListener m_func_on_select_category = new OnItemSelectedListener()
    {
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id)
        {
            Log.d("myLogs-spinner", "2");
            m_list_subcategories.clear();
            m_list_subcategories.initByPid((int) id);
            int townId = (int) ((Spinner) getView().findViewById(R.id.Town)).getSelectedItemId();
            Application.instance().getOffersManager().clear();
            m_request_manager.get_offers(townId, (int) id, -1, -1, 20);
        }

        public void onNothingSelected(AdapterView<?> adapterView)
        {
        }
    };
    private OnItemClickListener m_func_on_select_customer = new OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
        {
            ((BaseFragmentActivity) getActivity()).fragmentForward(CustomerViewFragment.createInstance(Application.instance().getOffersManager().getById((int) id).getExtra().getUid()));
        }
    };
    private OnItemSelectedListener m_func_on_select_subcategory = new OnItemSelectedListener()
    {
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id)
        {
            Log.d("myLogs-spinner", "3");
            int townId = (int) ((Spinner) getView().findViewById(R.id.Town)).getSelectedItemId();
            int categoryId = (int) ((Spinner) getView().findViewById(R.id.Category)).getSelectedItemId();
            Application.instance().getOffersManager().clear();
            m_request_manager.get_offers(townId, categoryId, (int) id, -1, 20);
        }

        public void onNothingSelected(AdapterView<?> adapterView)
        {
        }
    };
    private OnItemSelectedListener m_func_on_select_town = new OnItemSelectedListener()
    {
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id)
        {
            Log.d("myLogs-spinner", "1");
            Bundle bundle = new Bundle();
            int categoryId = (int) ((Spinner) getView().findViewById(R.id.Category)).getSelectedItemId();
            int subCategoryId = (int) ((Spinner) getView().findViewById(R.id.SubCategory)).getSelectedItemId();
            Application.instance().getOffersManager().clear();
            m_request_manager.get_offers((int) id, categoryId, subCategoryId, -1, 20);
        }

        public void onNothingSelected(AdapterView<?> adapterView)
        {
        }
    };
    private ListCategories m_list_subcategories;
    private RequestManager m_request_manager;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle onSavedInstanceState)
    {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0421\u043f\u0438\u0441\u043e\u043a \u0437\u0430\u043a\u0430\u0437\u043e\u0432");
        m_request_manager = new RequestManager();
        View view = inflater.inflate(R.layout.offers_view_list_fragment, container, false);
        ((Spinner) view.findViewById(R.id.Town)).setAdapter(new ListAdapterEx(inflater.getContext(), new ListCities(Application.instance().getUser().getCountry())));
        ((Spinner) view.findViewById(R.id.Town)).setSelection(0);
        ((Spinner) view.findViewById(R.id.Town)).setOnItemSelectedListener(m_func_on_select_town);
        ListCategories listCategory = new ListCategories();
        listCategory.addHeaderValue("\u0412\u044b\u0431\u0438\u0440\u0438\u0442\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044e", -1);
        ((Spinner) view.findViewById(R.id.Category)).setAdapter(new ListAdapterEx(inflater.getContext(), listCategory));
        ((Spinner) view.findViewById(R.id.Category)).setSelection(0, false);
        ((Spinner) view.findViewById(R.id.Category)).setOnItemSelectedListener(m_func_on_select_category);
        m_list_subcategories = new ListCategories();
        m_list_subcategories.clear();
        m_list_subcategories.addHeaderValue("\u0412\u044b\u0431\u0438\u0440\u0438\u0442\u0435 \u043f\u043e\u0434\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044e", -1);
        ((Spinner) view.findViewById(R.id.SubCategory)).setAdapter(new ListAdapterEx(inflater.getContext(), m_list_subcategories));
        ((Spinner) view.findViewById(R.id.SubCategory)).setSelection(0, false);
        ((Spinner) view.findViewById(R.id.SubCategory)).setOnItemSelectedListener(m_func_on_select_subcategory);
        ((ListView) view.findViewById(R.id.ListOffers)).setAdapter(new ViewListAdapter(view.getContext(), Application.instance().getOffersManager()));
        ((ListView) view.findViewById(R.id.ListOffers)).setOnItemClickListener(m_func_on_select_customer);
        Application.instance().getOffersManager().setNotifyDataSetChanged(new NotifyDataSetChanged((BaseAdapter) ((ListView) view.findViewById(R.id.ListOffers)).getAdapter()));
        UsersManager.instance().setNotifyDataSetChangeListener(new NotifyDataSetChanged((BaseAdapter) ((ListView) view.findViewById(R.id.ListOffers)).getAdapter()));
        Log.d("myLogs-spinner", "onCreateView");
        view.findViewById(R.id.GetMore).setOnClickListener(this.m_func_on_request_more);
        return view;
    }

    public void onStart()
    {
        super.onStart();
        Log.d("myLogs-spinner", "onStart");
    }

    public void onStop()
    {
        super.onStop();
    }
}

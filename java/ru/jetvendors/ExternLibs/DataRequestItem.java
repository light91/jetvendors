package ru.jetvendors.ExternLibs;

import android.os.Bundle;

public abstract class DataRequestItem {
    private BaseAsyncRequestManager m_request_manager;

    public abstract void requestItems(Bundle bundle);
}

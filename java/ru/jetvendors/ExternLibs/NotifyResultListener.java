package ru.jetvendors.ExternLibs;

public interface NotifyResultListener {
    void onError();

    void onSuccess();
}

package ru.jetvendors.ExternLibs;

public interface NotifyDataCountChangedListener {
    void onDataCountChanged(int i);
}

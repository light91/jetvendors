package ru.jetvendors.ExternLibs;

import android.os.AsyncTask;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.util.Log;
import android.util.Pair;
import com.google.gson.GsonBuilder;
import com.vk.sdk.api.httpClient.VKHttpClient;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ru.jetvendors.BuildConfig;

public class BaseAsyncRequestManager {
    List<AsyncRequest> m_asyncRequests;

    public class AsyncRequest extends AsyncTask<Pair<Request, ResponseObject>, Void, Pair<Request, ResponseObject>> {
        protected Pair<Request, ResponseObject> doInBackground(Pair<Request, ResponseObject>... args) {
            Request request = args[0].first;
            ResponseObject response = args[0].second;
            response.data(RequestObject.doRequest(request.getUrl(), request.getData(), request.getFilePath(), request.getFileName(), "image/jpeg"));
            return new Pair(request, response);
        }

        protected void onPostExecute(Pair<Request, ResponseObject> args) {
            int index;
            try {
                args.second.doResponse(args.first);
                index = m_asyncRequests.indexOf(this);
                if (index >= 0 && index < m_asyncRequests.size()) {
                    m_asyncRequests.remove(index);
                }
            } catch (NullPointerException e) {
                Log.d("myLogs", "onPostExecute: nullPointerException");
                index = m_asyncRequests.indexOf(this);
                if (index >= 0 && index < m_asyncRequests.size()) {
                    m_asyncRequests.remove(index);
                }
            } catch (Throwable th) {
                index = m_asyncRequests.indexOf(this);
                if (index >= 0 && index < m_asyncRequests.size()) {
                    m_asyncRequests.remove(index);
                }
            }
        }
    }

    public class Request
    {
        private Map<String, String> m_encodeData = new HashMap<String, String>();
        private Object m_object;
        private String m_url = new String();

        private String m_file_path;
        private String m_file_name;

        public Request()
        {

        }

        public Request(boolean dummy)
        {

        }

        public void addData(String key, String value)
        {
            m_encodeData.put(key, value);
        }

        public Map<String, String> getData()
        {
            return m_encodeData;
        }

        public String getUrl() {
            return this.m_url;
        }

        public void setUrl(String url) {
            this.m_url = url;
        }

        public Object getObject() {
            return this.m_object;
        }

        public void setObject(Object object) {
            this.m_object = object;
        }

        public String getFilePath()
        {
            return m_file_path;
        }

        public void setFilePath(String filePath)
        {
            m_file_path = filePath;
        }

        public String getFileName()
        {
            return m_file_name;
        }

        public void setFileName(String fileName)
        {
            m_file_name = fileName;
        }
    }

    public static class RequestObject
    {
        public static String doRequest(String urlTo, Map<String, String> parmas, String filepath, String filefield, String fileMimeType)
        {
            Log.d("myLogs-doRequest", "url: " + urlTo);
            HttpURLConnection connection = null;
            DataOutputStream outputStream = null;
            InputStream inputStream = null;

            String twoHyphens = "--";
            String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
            String lineEnd = "\r\n";

            String result = "";

            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;


            try {
                URL url = new URL(urlTo);
                connection = (HttpURLConnection) url.openConnection();

                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);

                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
                connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                connection.setRequestProperty("Accept-Charset", "UTF-8");
                outputStream = new DataOutputStream(connection.getOutputStream());

                if (filepath != null)
                {
                    String[] q = filepath.split("/");
                    int idx = q.length - 1;

                    File file = new File(filepath);
                    FileInputStream fileInputStream = new FileInputStream(file);


                    outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
                    outputStream.writeBytes("Content-Type: " + fileMimeType + lineEnd);
                    outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

                    outputStream.writeBytes(lineEnd);

                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        outputStream.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }

                    outputStream.writeBytes(lineEnd);
                }

                // Upload POST Data
                Iterator<String> keys = parmas.keySet().iterator();
                while (keys.hasNext()) {
                    String key = keys.next();
                    String value = parmas.get(key);

                    outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                    outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                    outputStream.writeBytes(lineEnd);
                    outputStream.write(value.getBytes("UTF-8"));
                    Log.d("myLogs", " value " + value.getBytes("UTF-8"));
                    //outputStream.writeUTF(value);//URLEncoder.encode(value, "UTF-8"));
                    outputStream.writeBytes(lineEnd);
                }

                outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


                if (200 != connection.getResponseCode()) {
                    return null;
                }

                inputStream = connection.getInputStream();
                int readBytesTotal = 0;
                int bytesSize = AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT;
                byte[] bytes = new byte[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
                while (true) {
                    int readBytes = inputStream.read(bytes, readBytesTotal, bytesSize - readBytesTotal);
                    if (readBytes > 0) {
                        readBytesTotal += readBytes;
                        if (bytesSize - readBytesTotal <= 100) {
                            byte[] _bytes = bytes;
                            bytesSize *= 5;
                            bytes = new byte[bytesSize];
                            int index = -1;
                            for (byte _byte : _bytes) {
                                index++;
                                bytes[index] = _byte;
                            }
                        }
                    } else {
                        inputStream.close();
                        connection.disconnect();
                        String str2 = new String(bytes, 0, readBytesTotal, "UTF-8");
                        Log.d("myLogs", "response: " + str2);
                        return str2;
                    }
                }
            } catch (IOException e) {
                Log.d("myLogs", "error: " + urlTo + "e " + e.getMessage() + "e.getStackTrace " + e.getStackTrace().toString());
                return null;
            }

        }
        /*
        public static String doRequest(String url_link, String data, boolean isGetReqest)
        {
            try {
                URL url = new URL(url_link + (isGetReqest ? "?" + data : BuildConfig.FLAVOR));
                Log.d("myLogs-route", url_link + "?" + data);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setDoInput(true);
                if (!isGetReqest) {
                    conn.setRequestMethod("POST");
                    conn.setDoOutput(data != null);
                    if (data != null) {
                        OutputStream os = conn.getOutputStream();
                        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, VKHttpClient.sDefaultStringEncoding));
                        writer.write(data);
                        writer.flush();
                        writer.close();
                        os.close();
                    }
                }
                conn.connect();
                InputStream in = conn.getInputStream();
                int readBytesTotal = 0;
                int bytesSize = AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT;
                byte[] bytes = new byte[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
                while (true) {
                    int readBytes = in.read(bytes, readBytesTotal, bytesSize - readBytesTotal);
                    if (readBytes > 0) {
                        readBytesTotal += readBytes;
                        if (bytesSize - readBytesTotal <= 100) {
                            byte[] _bytes = bytes;
                            bytesSize *= 5;
                            bytes = new byte[bytesSize];
                            int index = -1;
                            for (byte _byte : _bytes) {
                                index++;
                                bytes[index] = _byte;
                            }
                        }
                    } else {
                        in.close();
                        conn.disconnect();
                        String str2 = new String(bytes, 0, readBytesTotal, "cp1251");
                        Log.d("myLogs", "response: " + str2);
                        return str2;
                    }
                }
            } catch (IOException e) {
                Log.d("myLogs", "error: " + url_link + "e " + e.getMessage() + "e.getStackTrace " + e.getStackTrace().toString());
                return null;
            }
        }*/
    }

    public abstract class ResponseObject {
        private String m_data;

        public abstract void doResponse(Request request);

        public void data(String data) {
            this.m_data = data;
        }

        public String data() {
            return this.m_data;
        }

        public void invalidateData() {
            data(null);
        }

        protected JsonDeserializeData.JsonData getDecodeData() {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(JsonDeserializeData.JsonData.class, new JsonDeserializeData());
            return (JsonDeserializeData.JsonData) gsonBuilder.create().fromJson(data(), JsonDeserializeData.JsonData.class);
        }

        protected int getNodeIndexByName(List<JsonDeserializeData.JsonData> list, String nodeName) {
            for (int index = 0; index != list.size(); index++) {
                if (((JsonDeserializeData.JsonData) list.get(index)).getKey().equals(nodeName)) {
                    return index;
                }
            }
            return list.size();
        }
    }

    protected BaseAsyncRequestManager() {
        this.m_asyncRequests = new ArrayList();
    }

    protected void addRequest(AsyncRequest request) {
        this.m_asyncRequests.add(request);
    }
}

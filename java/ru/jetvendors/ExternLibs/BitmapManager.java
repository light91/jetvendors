package ru.jetvendors.ExternLibs;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v7.graphics.drawable.DrawableUtils;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import ru.jetvendors.Application;
import ru.jetvendors.R;

public class BitmapManager
{
    private static BitmapManager m_instance;
    private LruCache<String, Bitmap> m_cache;

    public static BitmapManager instance()
    {
        if (m_instance == null)
            m_instance = new BitmapManager();

        return m_instance;
    }

    public BitmapManager()
    {
        m_cache = new LruCache(AccessibilityNodeInfoCompat.ACTION_DISMISS);
        Log.d("myLogs-memory", "memory " + Runtime.getRuntime().maxMemory());
    }

    public Bitmap getImage(String imageName)
    {
        Bitmap bitmap = m_cache.get(imageName);

        if (bitmap == null)
        {

            Log.d("myLogs-file", "fil..." + getImagePath(imageName));
            bitmap = BitmapFactory.decodeFile(getImagePath(imageName));
            if (bitmap != null)
                m_cache.put(imageName, bitmap);
        }
        return bitmap;
    }

    public String getImagePath(String imageName)
    {
        ContextWrapper cw = new ContextWrapper(Application.instance());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File file = new File(directory.getAbsolutePath() + "/" + imageName + ".jpg");
        if (!file.exists())
            return null;

        return file.getAbsolutePath();//directory.getAbsolutePath() + "/" + imageName + ".jpg";
    }
/*
    public void saveImage(String imageName, Uri savingFile)
    {
        try
        {
            ContextWrapper cw = new ContextWrapper(Application.instance());
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

            FileOutputStream output = new FileOutputStream(directory.getAbsolutePath() + "/" + imageName + ".jpg");
            AssetFileDescriptor fileDescriptor = Application.instance().getContentResolver().openAssetFileDescriptor(savingFile, "r");
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, output);
        }
        catch (Exception e)
        {

        }
    }*/

    public void setImageView(final ImageView imageView, final String imageName, final long lastTimeUpdate)
    {
        ContextWrapper cw = new ContextWrapper(Application.instance());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File myImageFile = new File(directory, imageName + ".jpg");

        Log.d("myLogs-image-1", "getImageUrl(imageName) "  + getImageUrl(imageName));

        if (myImageFile.exists() && myImageFile.lastModified() / 1000 >= lastTimeUpdate)
        {
            Picasso.with(Application.instance()).load(myImageFile).into(imageView);
        }
        else
        {
            Log.d("myLogs-image-2", "getImageUrl(imageName) "  + getImageUrl(imageName));
            Picasso.with(Application.instance()).invalidate(getImageUrl(imageName));
            Picasso.with(Application.instance()).load(getImageUrl(imageName)).error(Application.instance().getResources().getDrawable(R.drawable.avatar_default)).into(imageView, new Callback()
            {
                @Override
                public void onSuccess()
                {
                    Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                    saveFile(bitmap, imageName, lastTimeUpdate);
                }

                @Override
                public void onError() {

                }
            });
        }
    }


    private String getImageUrl(String imageName)
    {
        return "http://qazaqtv.kz/JetVendors/image.php?uid=" + imageName;
    }

    public void saveFile(final Bitmap bitmap, String imageName, long lastTimeUpdate)
    {
        Log.i("image", "image saveFile");
        ContextWrapper cw = new ContextWrapper(Application.instance());
        final File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        final File myImageFile = new File(directory, imageName + ".jpg");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(myImageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        long time = lastTimeUpdate * 1000;
        myImageFile.setLastModified(time);
        Log.i("image", "image saved result " + " lastModified: " + myImageFile.lastModified());
    }
}

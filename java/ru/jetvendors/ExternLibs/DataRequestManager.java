package ru.jetvendors.ExternLibs;

import android.os.Bundle;
import com.vk.sdk.api.VKApiConst;

import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public abstract class DataRequestManager<DataType extends DataItem> extends DataManager<DataType>
{
    private static final int GET_REQUEST_ITEMS_COUNT = 20;
    private static final int INVALID_ITEM_ID = -1;
    private NotifyOperationComplete m_func_receive_every;
    private DataRequestItem m_request;
    private Bundle m_request_data;

    protected void setRequest(DataRequestItem request) {
        this.m_request = request;
    }

    public void addArray(JsonData jsonData)
    {
        int beforeItemsCount = getCount();
        super.addArray(jsonData);
        notifyReceiveEveryListener(beforeItemsCount, getCount());
    }

    public void requestItems(Bundle requestData) {
        clear();
        this.m_request_data = requestData;
        requestItemsInner(INVALID_ITEM_ID);
    }

    public void requestItemsPrevious() {
        DataItem lastDataItem = getCount() != 0 ? get(getCount() + INVALID_ITEM_ID) : null;
        requestItemsInner(lastDataItem != null ? lastDataItem.getId() : INVALID_ITEM_ID);
    }

    public void requestItemsNext() {
        DataItem lastDataItem = getCount() != 0 ? get(getCount() + INVALID_ITEM_ID) : null;
        requestItemsInner(lastDataItem != null ? lastDataItem.getId() : INVALID_ITEM_ID);
    }

    private void requestItemsInner(int lastItemId) {
        this.m_request_data.putString("last_id", String.valueOf(lastItemId));
        this.m_request_data.putString(VKApiConst.COUNT, String.valueOf(GET_REQUEST_ITEMS_COUNT));
        onPrepareRequestData(this.m_request_data);
        this.m_request.requestItems(this.m_request_data);
    }

    protected void onPrepareRequestData(Bundle requestData) {
    }

    public void setNotifyReceiveEveryListener(NotifyOperationComplete receiveEveryListener) {
        this.m_func_receive_every = receiveEveryListener;
    }

    private void notifyReceiveEveryListener(int beforeItemsCount, int afterItemsCount) {
        if (this.m_func_receive_every != null) {
            this.m_func_receive_every.onNotify(Boolean.valueOf(afterItemsCount - beforeItemsCount == GET_REQUEST_ITEMS_COUNT));
        }
    }
}

package ru.jetvendors.ExternLibs;

public interface NotifyOperationComplete
{
    void onNotify(Object result);
}

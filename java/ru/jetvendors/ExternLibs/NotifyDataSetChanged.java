package ru.jetvendors.ExternLibs;

import android.widget.BaseAdapter;

public class NotifyDataSetChanged {
    private final BaseAdapter m_adapter;

    public NotifyDataSetChanged(BaseAdapter adapter) {
        this.m_adapter = adapter;
    }

    public void notifyDataSetChange() {
        this.m_adapter.notifyDataSetChanged();
    }
}

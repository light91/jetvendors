package ru.jetvendors.ExternLibs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public abstract class DataManager<DataType extends DataItem> extends DataItem {
    private List<DataType> m_data;
    private NotifyDataSetChanged m_func_notify_data_set_changed;

    public abstract DataType onInitByJson(JsonData jsonData);

    public DataManager() {
        this.m_data = new ArrayList();
    }

    public final List<DataType> get() {
        return this.m_data;
    }

    public DataType get(int index) {
        return m_data.get(index);
    }

    public DataType getById(int id) {
        for (int index = 0; index != getCount(); index++) {
            if (get(index).getId() == id) {
                return get(index);
            }
        }
        return null;
    }

    public int getCount() {
        return this.m_data.size();
    }

    public void addArray(JsonData jsonData) {
        Iterator i$ = jsonData.getObject().iterator();
        while (i$.hasNext()) {
            add((JsonData) i$.next());
        }
    }

    public void add(JsonData jsonData) {
        add(onInitByJson(jsonData));
    }

    public void add(DataType item) {
        this.m_data.add(item);
        notifyDataSetChanged();
    }

    public void remove(int index) {
        this.m_data.remove(index);
        notifyDataSetChanged();
    }

    public void removeById(int id) {
        for (int index = 0; index != this.m_data.size(); index++) {
            if (((DataItem) this.m_data.get(index)).getId() == id) {
                this.m_data.remove(index);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public void clear() {
        this.m_data.clear();
        notifyDataSetChanged();
    }

    public void setNotifyDataSetChanged(NotifyDataSetChanged notifyDataSetChanged) {
        this.m_func_notify_data_set_changed = notifyDataSetChanged;
    }

    public void notifyDataSetChanged() {
        if (this.m_func_notify_data_set_changed != null) {
            this.m_func_notify_data_set_changed.notifyDataSetChange();
        }
    }
}

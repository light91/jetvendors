package ru.jetvendors.ExternLibs;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map.Entry;

public class JsonDeserializeData implements JsonDeserializer<JsonDeserializeData.JsonData> {

    public static class JsonData {
        private String m_key;
        private ArrayList<JsonData> m_object;
        private String m_value;

        public void setKey(String key) {
            this.m_key = key;
        }

        public void setValue(String value) {
            this.m_value = value;
        }

        public void setObject(ArrayList<JsonData> object) {
            this.m_object = object;
        }

        public String getKey() {
            return this.m_key;
        }

        public String getValue() {
            return this.m_value;
        }

        public ArrayList<JsonData> getObject() {
            return this.m_object;
        }
    }

    public JsonData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonData jsonData = new JsonData();
        jsonData.setObject(new ArrayList());
        if (json.isJsonPrimitive()) {
            jsonData.getObject().add(new JsonData());
            ((JsonData) jsonData.getObject().get(0)).setValue(json.getAsJsonPrimitive().getAsString());
        } else {
            deserializeInternal(jsonData.getObject(), json, typeOfT, context);
        }
        return jsonData;
    }

    private void deserializeInternal(ArrayList<JsonData> jsonData, JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        for (Entry<String, JsonElement> entry : json.getAsJsonObject().entrySet()) {
            jsonData.add(new JsonData());
            ((JsonData) jsonData.get(jsonData.size() - 1)).setKey((String) entry.getKey());
            JsonElement innerJson = (JsonElement) entry.getValue();
            if (innerJson.isJsonArray()) {
                ((JsonData) jsonData.get(jsonData.size() - 1)).setObject(new ArrayList());
                JsonArray jsonArray = innerJson.getAsJsonArray();
                for (int index = 0; index != jsonArray.size(); index++) {
                    deserializeInternal(((JsonData) jsonData.get(jsonData.size() - 1)).getObject(), jsonArray.get(index), typeOfT, context);
                }
            } else if (innerJson.isJsonObject()) {
                ((JsonData) jsonData.get(jsonData.size() - 1)).setObject(new ArrayList());
                deserializeInternal(((JsonData) jsonData.get(jsonData.size() - 1)).getObject(), innerJson, typeOfT, context);
            } else if (innerJson.isJsonPrimitive()) {
                ((JsonData) jsonData.get(jsonData.size() - 1)).setValue(innerJson.getAsString());
            }
        }
    }
}

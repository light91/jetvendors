package ru.jetvendors;

import android.content.Context;
import android.content.Intent;
import java.io.File;
import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import ru.jetvendors.Data.OffersManager;
import ru.jetvendors.Data.SettingManager;
import ru.jetvendors.Data.UserCats;
import ru.jetvendors.Data.UserExtra;
import ru.jetvendors.Data.UserInfoBase;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.Messages.Libs.MessagesManager;
import ru.jetvendors.Notifications.Libs.EventsManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

@ReportsCrashes(formUri = "http://www.qazaqtv.kz/JetVendors/report.php")
public class Application extends android.app.Application
{
    private static Application m_instance;
    private OffersManager m_offers_manager;
    private SettingManager m_setting_manager;
    private UserExtra m_user_extra;

    public enum AccountType {
        provider,
        customer
    }

    public static Application instance()
    {
        return m_instance;
    }

    public void onCreate() {
        super.onCreate();
        m_instance = this;
    }

    public void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        ACRA.init(this);
    }

    public void init(Context context) {
        this.m_setting_manager = new SettingManager();
        this.m_user_extra = new UserExtra();
    }

    public File getCacheImage() {
        return new File(getApplicationContext().getExternalCacheDir(), "image");
    }

    public void onLogin(JsonData jsonData)
    {
        this.m_offers_manager = new OffersManager();
        getUserExtra().initByJson(jsonData);
        MessagesManager.instance().onLogin();
        EventsManager.instance();

        Intent intent = new Intent(this, Service.class);
        intent.putExtra(Service.EXTRA_KEY_MSG, Service.EXTRA_VALUE_LOGIN);
        intent.putExtra(Service.EXTRA_VALUE_SID, getUserExtra().getSid());
        getBaseContext().startService(intent);
    }

    public void onLogout()
    {
        init(getBaseContext());
        EventsManager.instance().onLogout();
        MessagesManager.instance().onLogout();

        Intent intent = new Intent(this, Service.class);
        intent.putExtra(Service.EXTRA_KEY_MSG, Service.EXTRA_VALUE_LOGOUT);
        getBaseContext().startService(intent);
    }

    public UserExtra getUserExtra() {
        return this.m_user_extra;
    }

    public UserInfoBase getUser() {
        return UsersManager.instance().getById(getUserExtra().getUid());
    }

    public UserCats getUserCats() {
        return getUser().getUserCats();
    }

    public MessagesManager getMessages() {
        return MessagesManager.instance();
    }

    public OffersManager getOffersManager() {
        return this.m_offers_manager;
    }

    public SettingManager getSettingManager() {
        return this.m_setting_manager;
    }
}

package ru.jetvendors;

        import android.os.Bundle;
        import ru.jetvendors.AccountSign.StartFragment;

public class StartActivity extends BaseFragmentActivity
{
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        fragmentSetup(new StartFragment());
    }
}

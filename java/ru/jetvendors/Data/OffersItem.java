package ru.jetvendors.Data;

import ru.jetvendors.ExternLibs.DataManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class OffersItem extends DataManager<OffersProductItem>
{
    private OffersItemExtra m_extra;

    public OffersItem()
    {
        m_extra = new OffersItemExtra();
        setId(-1);
    }

    public void update(OffersItem offer)
    {
        setId(offer.getId());
        m_extra = offer.m_extra;
    }

    public OffersProductItem onInitByJson(JsonData jsonData)
    {
        m_extra.initByJson(jsonData);
        return OffersProductItem.initByJson(jsonData);
    }

    public String getVisibleTime()
    {
        return getExtra().getVisibleTime();
    }

    public boolean checkFilled()
    {
        for (int index = 0; index != getCount(); index++)
        {
            OffersProductItem product = get(index);
            if (product.getCatId() < 0 || product.getSubCatId() < 0 || product.getCount() <= 0 || product.getCountId() < 0 || product.getPrice().length() <= 0)
                return false;
        }
        return true;
    }

    public OffersItemExtra getExtra()
    {
        return m_extra;
    }
}

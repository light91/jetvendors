package ru.jetvendors.Data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import ru.jetvendors.R;
import ru.jetvendors.Application;

public class FullCategoriesDB extends SQLiteOpenHelper {
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_PID = "pid";
    private static final String DATABASE_NAME = "JetVendors.db";
    private static final int DATABASE_VERSION = 1;
    public static final int INVALID_ID = -1;
    private static final String TABLE_NAME = "Categories";

    public class Category {
        public int id;
        public String name;
        public int pid;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Categories (id INTEGER PRIMARY KEY, name TEXT, pid INTEGER);");
        db.execSQL("CREATE INDEX pid_index  ON Categories(pid);");
        initDB(db);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Categories");
        onCreate(db);
    }

    public FullCategoriesDB() {
        super(Application.instance().getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    public long addCategory(int id, String name) {
        return addSubCategory(id, INVALID_ID, name);
    }

    public long addCategory(SQLiteDatabase db, int id, String name) {
        return addSubCategory(db, id, INVALID_ID, name);
    }

    public long addSubCategory(SQLiteDatabase db, int id, int pid, String name) {
        ContentValues row = new ContentValues();
        row.clear();
        row.put(COLUMN_ID, Integer.valueOf(id));
        row.put(COLUMN_PID, Integer.valueOf(pid));
        row.put(COLUMN_NAME, name);
        return db.insert(TABLE_NAME, null, row);
    }

    public long addSubCategory(int id, int pid, String name) {
        return addSubCategory(getWritableDatabase(), id, pid, name);
    }

    public Category getById(int id) {
        Cursor cursor = getReadableDatabase().query(TABLE_NAME, null, "id = " + id, null, null, null, null);
        if (cursor.getCount() == 0) {
            return null;
        }
        cursor.moveToFirst();
        Category category = new Category();
        category.id = id;
        category.pid = cursor.getInt(cursor.getColumnIndex(COLUMN_PID));
        category.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        cursor.close();
        return category;
    }

    public Category get(int index) {
        return get(index, INVALID_ID);
    }

    public Category get(int index, int pid) {
        Cursor cursor = getReadableDatabase().query(TABLE_NAME, null, "pid = " + pid, null, null, null, null);
        if (cursor.getCount() == 0 || cursor.getCount() <= index) {
            return null;
        }
        cursor.moveToFirst();
        for (int i = 0; i != index; i += DATABASE_VERSION) {
            cursor.moveToNext();
        }
        Category category = new Category();
        category.id = cursor.getInt(cursor.getColumnIndex(COLUMN_ID));
        category.pid = pid;
        category.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
        cursor.close();
        return category;
    }

    public int getCount() {
        return getCount(INVALID_ID);
    }

    public int getCount(int pid) {
        Cursor cursor = getReadableDatabase().query(TABLE_NAME, null, "pid = " + pid, null, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    void initDB(SQLiteDatabase db) {
        ContentValues row = new ContentValues();
        addCategory(db, 0, "\u041c\u044f\u0441\u043e");
        addSubCategory(db, DATABASE_VERSION, 0, "\u0433\u043e\u0432\u044f\u0434\u0438\u043d\u0430");
        addSubCategory(db, 2, 0, "\u0442\u0435\u043b\u044f\u0442\u0438\u043d\u0430");
        addSubCategory(db, 3, 0, "\u043a\u043e\u043d\u0438\u043d\u0430");
        addSubCategory(db, 4, 0, "\u0441\u0432\u0438\u043d\u0438\u043d\u0430");
        addSubCategory(db, 5, 0, "\u0431\u0430\u0440\u0430\u043d\u0438\u043d\u0430");
        addSubCategory(db, 6, 0, "\u043a\u0440\u043e\u043b\u0438\u043a");
        addCategory(db, 7, "\u041f\u0442\u0438\u0446\u0430");
        addSubCategory(db, 8, 7, "\u043a\u0443\u0440\u0438\u0446\u0430");
        addSubCategory(db, 9, 7, "\u0438\u043d\u0434\u0435\u0439\u043a\u0430");
        addSubCategory(db, 10, 7, "\u0443\u0442\u043a\u0430");
        addSubCategory(db, 11, 7, "\u043f\u0435\u0440\u0435\u043f\u0435\u043b");
        addSubCategory(db, 12, 7, "\u0441\u0443\u0431\u043f\u0440\u043e\u0434\u0443\u043a\u0442\u044b");
        addSubCategory(db, 13, 7, "\u044f\u0438\u0446\u043e");
        addCategory(db, 14, "\u0420\u044b\u0431\u0430, \u043c\u043e\u0440\u0435\u043f\u0440\u043e\u0434\u0443\u043a\u0442\u044b");
        addSubCategory(db, 15, 14, "\u0412\u0435\u0441\u043e\u0432\u0430\u044f \u0440\u044b\u0431\u0430");
        addSubCategory(db, 16, 14, "\u0418\u043a\u0440\u0430");
        addSubCategory(db, 17, 14, "\u041a\u0440\u0430\u0431\u043e\u0432\u044b\u0435 \u043f\u0430\u043b\u043e\u0447\u043a\u0438");
        addSubCategory(db, 18, 14, "\u041a\u0440\u0435\u0432\u0435\u0442\u043a\u0438");
        addSubCategory(db, 19, 14, "\u041c\u043e\u0440\u0441\u043a\u0438\u0435 \u0434\u0435\u043b\u0438\u043a\u0430\u0442\u0435\u0441\u044b");
        addCategory(db, 20, "\u041e\u0432\u043e\u0449\u0438-\u0444\u0440\u0443\u043a\u0442\u044b, \u0437\u0435\u043b\u0435\u043d\u044c");
        addSubCategory(db, 21, 20, "\u0412\u0435\u0441\u043e\u0432\u044b\u0435 \u043e\u0432\u043e\u0449\u0438/\u044f\u0433\u043e\u0434\u044b/\u0433\u0440\u0438\u0431\u044b");
        addSubCategory(db, 22, 20, "\u0421\u0432\u0435\u0436\u0438\u0435 \u043e\u0432\u043e\u0449\u0438/\u044f\u0433\u043e\u0434\u044b/\u0444\u0440\u0443\u043a\u0442\u044b");
        addSubCategory(db, 23, 20, "\u0424\u0430\u0441\u043e\u0432\u0430\u043d\u043d\u044b\u0435 \u043e\u0432\u043e\u0449\u0438/\u044f\u0433\u043e\u0434\u044b/\u0433\u0440\u0438\u0431\u044b");
        addCategory(db, 24, "\u0412\u044b\u043f\u0435\u0447\u043a\u0430, \u0445\u043b\u0435\u0431\u043e\u0431\u0443\u043b\u043e\u0447\u043d\u044b\u0435/\u043a\u043e\u043d\u0434\u0438\u0442\u0435\u0440\u0441\u043a\u0438\u0435 \u0438\u0437\u0434\u0435\u043b\u0438\u044f");
        addSubCategory(db, 25, 24, "\u0445\u043b\u0435\u0431");
        addSubCategory(db, 26, 24, "\u0431\u0430\u0442\u043e\u043d");
        addSubCategory(db, 27, 24, "\u0431\u0430\u0433\u0435\u0442");
        addSubCategory(db, 28, 24, "\u0431\u0443\u043b\u043e\u0447\u043a\u0438");
        addSubCategory(db, 29, 24, "\u043a\u0440\u0443\u0430\u0441\u0430\u043d\u044b");
        addSubCategory(db, 30, 24, "\u041f\u0438\u0440\u043e\u0433\u0438");
        addSubCategory(db, 31, 24, "\u0422\u043e\u0440\u0442\u044b");
        addSubCategory(db, 32, 24, "\u0422\u0430\u0440\u0442\u0430\u0435\u0442\u043a\u0438");
        addCategory(db, 33, "\u0411\u0430\u043a\u0430\u043b\u0435\u044f");
        addSubCategory(db, 34, 33, "\u0413\u0430\u0441\u0442\u0440\u043e\u043d\u043e\u043c\u0438\u044f/\u041a\u043e\u043d\u0441\u0435\u0440\u0432\u044b");
        addSubCategory(db, 35, 33, "\u041a\u0440\u0443\u043f\u044b");
        addSubCategory(db, 36, 33, "\u041c\u0430\u0439\u043e\u043d\u0435\u0437/\u041a\u0435\u0442\u0447\u0443\u043f/\u0413\u043e\u0440\u0447\u0438\u0446\u0430/\u0421\u043e\u0443\u0441\u044b");
        addSubCategory(db, 37, 33, "\u041c\u0430\u043a\u0430\u0440\u043e\u043d\u043d\u044b\u0435 \u0438\u0437\u0434\u0435\u043b\u0438\u044f");
        addSubCategory(db, 38, 33, "\u041c\u0443\u043a\u0430/\u0421\u0430\u0445\u0430\u0440/\u0421\u043e\u043b\u044c");
        addSubCategory(db, 39, 33, "\u041e\u0440\u0435\u0445\u0438/\u0421\u0443\u0445\u043e\u0444\u0440\u0443\u043a\u0442\u044b");
        addSubCategory(db, 40, 33, "\u041f\u0440\u043e\u0434\u0443\u043a\u0446\u0438\u044f \u041a\u043d\u043e\u0440\u0440, \u0413\u0430\u043b\u0438\u043d\u0430 \u0431\u043b\u0430\u043d\u043a\u0430");
        addSubCategory(db, 41, 33, "\u0421\u043e\u043a\u0438/\u0412\u043e\u0434\u044b/\u041b\u0438\u043c\u043e\u043d\u0430\u0434\u044b");
        addSubCategory(db, 42, 33, "\u0421\u043f\u0435\u0446\u0438\u0438");
        addSubCategory(db, 43, 33, "\u0422\u043e\u043f\u0438\u043d\u0433\u0438/\u041c\u0435\u0434/\u0412\u0430\u0440\u0435\u043d\u044c\u0435/\u0414\u0436\u0435\u043c");
        addSubCategory(db, 44, 33, "\u0427\u0430\u0439/\u043a\u043e\u0444\u0435");
        addCategory(db, 45, "\u0412\u0438\u043d\u043e- \u0432\u043e\u0434\u043e\u0447\u043d\u0430\u044f \u043f\u0440\u0434\u0443\u043a\u0446\u0438\u044f");
        addSubCategory(db, 46, 45, "\u041f\u0438\u0432\u043e");
        addSubCategory(db, 47, 45, "\u0412\u043e\u0434\u043a\u0430");
        addSubCategory(db, 48, 45, "\u041a\u043e\u043d\u044c\u044f\u043a");
        addSubCategory(db, 49, 45, "\u0428\u0430\u043c\u043f\u0430\u043d\u0441\u043a\u043e\u0435");
        addSubCategory(db, 50, 45, "\u0412\u0438\u043d\u043e");
        addSubCategory(db, 51, 45, "\u0412\u0438\u0441\u043a\u0438");
        addCategory(db, 52, "\u0422\u0430\u0431\u0430\u0447\u043d\u044b\u0435 \u0438\u0437\u0434\u0435\u043b\u0438\u044f");
        addSubCategory(db, 53, 52, "\u0421\u0438\u0433\u0430\u0440\u0435\u0442\u044b");
        addSubCategory(db, 54, 52, "\u0421\u0438\u0433\u0430\u0440\u044b");
        addSubCategory(db, 55, 52, "\u0417\u0430\u0436\u0438\u0433\u0430\u043b\u043a\u0438");
        addCategory(db, 56, "\u041c\u043e\u043b\u043e\u0447\u043d\u044b\u0435 \u043f\u0440\u043e\u0434\u0443\u043a\u0442\u044b, \u0441\u044b\u0440\u044b, \u043c\u0430\u0441\u043b\u043e");
        addSubCategory(db, 57, 56, "\u041c\u0430\u0441\u043b\u043e");
        addSubCategory(db, 58, 56, "\u041c\u043e\u043b\u043e\u043a\u043e");
        addSubCategory(db, 59, 56, "\u0421\u043b\u0438\u0432\u043a\u0438");
        addSubCategory(db, 60, 56, "\u0421\u043c\u0435\u0442\u0430\u043d\u0430");
        addSubCategory(db, 61, 56, "\u0421\u044b\u0440\u044b");
        addSubCategory(db, 62, 56, "\u0422\u0432\u043e\u0440\u043e\u0433/\u041a\u0435\u0444\u0438\u0440/\u0419\u043e\u0433\u0443\u0440\u0442");
        addSubCategory(db, 63, 56, "\u041c\u043e\u0440\u043e\u0436\u0435\u043d\u043e\u0435");
        addCategory(db, 64, "\u041f\u043e\u043b\u0443\u0444\u0430\u0431\u0440\u0438\u043a\u0430\u0442\u044b");
        addSubCategory(db, 65, 64, "\u041a\u0430\u0440\u0442\u043e\u0444\u0435\u043b\u044c-\u0444\u0440\u0438");
        addSubCategory(db, 66, 64, "\u0431\u043b\u0438\u043d\u044b");
        addSubCategory(db, 67, 64, "\u043a\u043e\u0442\u043b\u0435\u0442\u044b");
        addSubCategory(db, 68, 64, "\u0432\u0430\u0440\u0435\u043d\u0438\u043a\u0438");
        addSubCategory(db, 69, 64, "\u043f\u0435\u043b\u044c\u043c\u0435\u043d\u0438");
        addSubCategory(db, 70, 64, "\u0442\u0435\u0441\u0442\u043e");
        addCategory(db, 71, "\u0421\u043e\u043f\u0443\u0442\u0441\u0442\u0432\u0443\u044e\u0449\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u044b");
        addSubCategory(db, 72, 71, "\u0427\u0438\u0441\u0442\u044f\u0449\u0438\u0435/\u043c\u043e\u044e\u0449\u0438\u0435 \u0441\u0440\u0435\u0434\u0441\u0442\u0432\u0430");
        addSubCategory(db, 73, 71, "\u0421\u0430\u043b\u0444\u0435\u0442\u043a\u0438 \u0431\u0443\u043c\u0430\u0436\u043d\u044b\u0435");
        addSubCategory(db, 74, 71, "\u0441\u0430\u043b\u0444\u0435\u0442\u043a\u0438 \u0432\u0430\u0436\u043d\u044b\u0435");
        addSubCategory(db, 75, 71, "\u0441\u0430\u043b\u0444\u0435\u0442\u043a\u0438 \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043e\u0447\u043d\u044b\u0435");
        addSubCategory(db, 76, 71, "\u0441\u0430\u043b\u0444\u0435\u0442\u043a\u0438 \u043f\u0440\u043e\u0442\u0438\u0440\u043e\u0447\u043d\u044b\u0435");
        addSubCategory(db, 77, 71, "\u0437\u0443\u0431\u043e\u0447\u0438\u0441\u0442\u043a\u0438");
        addSubCategory(db, 78, 71, "\u0448\u043f\u0430\u0436\u043a\u0438");
        addSubCategory(db, 79, 71, "\u0436\u0438\u0434\u043a\u043e\u0435 \u043c\u044b\u043b\u043e");
        addSubCategory(db, 80, 71, "\u043f\u0435\u0440\u0447\u0430\u0442\u043a\u0438 \u0440\u0435\u0437\u0438\u043d\u043e\u0432\u044b\u0435/\u043b\u0430\u0442\u0435\u043a\u0441");
        addSubCategory(db, 81, 71, "\u043f\u0430\u043a\u0435\u0442\u044b \u0434\u043b\u044f \u043c\u0443\u0441\u043e\u0440\u0430");
        addSubCategory(db, 82, 71, "\u043f\u0430\u043a\u0435\u0442\u044b \u0444\u0430\u0441\u043e\u0432\u043e\u0447\u043d\u044b\u0435");
        addSubCategory(db, 83, 71, "\u043f\u0438\u0449\u0435\u0432\u0430\u044f \u043f\u043b\u0435\u043d\u043a\u0430");
        addSubCategory(db, 84, 71, "\u043f\u0438\u0449\u0435\u0432\u0430\u044f \u0444\u043e\u043b\u044c\u0433\u0430");
        addSubCategory(db, 85, 71, "\u043f\u0435\u0440\u0433\u0430\u043c\u0435\u043d\u0442");
        addSubCategory(db, 86, 71, "\u0442\u0440\u044f\u043f\u043a\u0430 \u043f\u043e\u043b\u043e\u0432\u0430\u044f");
        addSubCategory(db, 87, 71, "\u0433\u0443\u0431\u043a\u0438 \u0434\u043b\u044f \u043f\u043e\u0441\u0443\u0434\u044b");
        addSubCategory(db, 88, 71, "\u041f\u043e\u043b\u043e\u0442\u0435\u043d\u0446\u0430");
        addSubCategory(db, 89, 71, "\u041e\u0441\u0432\u0435\u0436\u0438\u0442\u0435\u043b\u044c \u0432\u043e\u0437\u0434\u0443\u0445\u0430");
        addSubCategory(db, 90, 71, "\u0422\u0440\u0443\u0431\u043e\u0447\u043a\u0438 \u0434\u043b\u044f \u043a\u043e\u043a\u0442\u0435\u0439\u043b\u044f");
        addSubCategory(db, 91, 71, "\u041c\u044b\u043b\u043e \u0442\u0443\u0430\u043b\u0435\u0442\u043d\u043e\u0435");
        addSubCategory(db, 92, 71, "\u041c\u044b\u043b\u043e \u0445\u043e\u0437\u044f\u0439\u0441\u0442\u0432\u0435\u043d\u043d\u043e\u0435");
        addSubCategory(db, 93, 71, "\u0421\u0442\u0438\u0440\u0430\u043b\u044c\u043d\u044b\u0439 \u043f\u043e\u0440\u043e\u0448\u043e\u043a");
        addSubCategory(db, 94, 71, "\u0411\u0435\u043b\u0438\u0437\u043d\u0430");
        addSubCategory(db, 95, 71, "\u043c\u043e\u044e\u0449\u0435\u0435 \u0441\u0440-\u0432\u043e \u0434\u043b\u044f \u043f/\u043c");
        addCategory(db, 96, "\u0422\u0435\u043a\u0441\u0442\u0438\u043b\u044c");
        addSubCategory(db, 97, 96, "\u0421\u0430\u043b\u0444\u0435\u0442\u043a\u0438");
        addSubCategory(db, 98, 96, "\u0414\u043e\u0440\u043e\u0436\u043a\u0438");
        addSubCategory(db, 99, 96, "\u0427\u0435\u0445\u043b\u044b \u0434\u043b\u044f \u0441\u0442\u0443\u043b\u044c\u0435\u0432");
        addSubCategory(db, 100, 96, "\u0421\u043a\u0430\u0442\u0435\u0440\u0442\u0438");
        addSubCategory(db, R.styleable.Theme_buttonStyleSmall, 96, "\u0424\u0443\u0440\u0448\u0435\u0442\u043d\u044b\u0435 \u044e\u0431\u043a\u0438");
        addSubCategory(db, R.styleable.Theme_checkboxStyle, 96, "\u041a\u0443\u0432\u0435\u0440\u0442\u044b");
        addSubCategory(db, R.styleable.Theme_checkedTextViewStyle, 96, "\u0414\u043e\u0440\u043e\u0436\u043a\u0438");
        addSubCategory(db, R.styleable.Theme_editTextStyle, 96, "\u041f\u043b\u0435\u0439\u0441\u043c\u0435\u0442\u044b");
        addSubCategory(db, R.styleable.Theme_radioButtonStyle, 96, "\u0428\u0442\u043e\u0440\u044b");
        addSubCategory(db, R.styleable.Theme_ratingBarStyle, 96, "\u041f\u043e\u043b\u043e\u0442\u0435\u043d\u0446\u0430");
        addCategory(db, R.styleable.Theme_seekBarStyle, "\u041a\u0443\u0445\u043e\u043d\u043d\u0430\u044f \u0443\u0442\u0432\u0430\u0440\u044c");
        addSubCategory(db, R.styleable.Theme_spinnerStyle, R.styleable.Theme_seekBarStyle, "\u0432\u0438\u043b\u043a\u0438");
        addSubCategory(db, R.styleable.Theme_switchStyle, R.styleable.Theme_seekBarStyle, "\u043b\u043e\u0436\u043a\u0438");
        addSubCategory(db, 110, R.styleable.Theme_seekBarStyle, "\u043d\u043e\u0436\u0438");
        addSubCategory(db, 111, R.styleable.Theme_seekBarStyle, "\u0442\u0430\u0440\u0435\u043b\u043a\u0438");
        addSubCategory(db, 112, R.styleable.Theme_seekBarStyle, "\u0431\u043e\u043a\u0430\u043b\u044b");
        addSubCategory(db, 113, R.styleable.Theme_seekBarStyle, "\u0441\u0442\u0430\u043a\u0430\u043d\u044b");
        addSubCategory(db, 114, R.styleable.Theme_seekBarStyle, "\u0447\u0430\u0448\u043a\u0438");
        addSubCategory(db, 115, R.styleable.Theme_seekBarStyle, "\u0441\u0430\u043b\u0430\u0442\u043d\u0438\u0446\u044b");
        addSubCategory(db, 116, R.styleable.Theme_seekBarStyle, "\u0431\u043b\u044e\u0434\u0430");
        addCategory(db, 117, "\u041e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u0435");
        addSubCategory(db, 118, 117, "\u043a\u0443\u0445\u043e\u043d\u043d\u043e\u0435 \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u0435");
        addSubCategory(db, 119, 117, "\u0411\u0430\u0440\u043d\u043e\u0435 \u043e\u0431\u043e\u0440\u0443\u0434\u043e\u0432\u0430\u043d\u0438\u0435");
    }
}

package ru.jetvendors.Data;

import android.util.Log;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKScopes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import ru.jetvendors.Application.AccountType;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class UserInfoBase {
    private String m_about;
    private AccountType m_account_type;
    private int m_bid;
    private String m_bisness;
    private UserCats m_cats;
    private int m_cid;
    private String m_email;
    private String m_family;
    private int m_id;
    private String m_name;
    private String m_phone;
    private int m_tid;
    private long m_update_time;

    public UserInfoBase()
    {
        m_id = -1;
        m_cats = new UserCats();
    }

    public UserInfoBase(JsonData jsonArray) {
        this();
        initByJson(jsonArray);
    }

    public void update(UserInfoBase user) {
        m_account_type = user.m_account_type;
        m_id = user.m_id;
        m_tid = user.m_tid;
        m_name = user.m_name;
        m_family = user.m_family;
        m_cats = user.m_cats;
        m_cid = user.m_cid;
        m_phone = user.m_phone;
        m_email = user.m_email;
        m_bisness = user.m_bisness;
        m_bid = user.m_bid;
        m_about = user.m_about;
    }

    public void initByJson(JsonData jsonArray) {
        Iterator i$ = jsonArray.getObject().iterator();
        while (i$.hasNext()) {
            JsonData jsonItem = (JsonData) i$.next();
            Log.d("myLogs-jsonItem-+", "jsonItem " + jsonItem.getKey() + "jsonItem.v " + jsonItem.getValue());
            String key = jsonItem.getKey();
            switch (key)
            {
                case "id":
                    m_id = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                case "tid":
                    setTown(Integer.valueOf(jsonItem.getValue()).intValue());
                    break;
                case "name":
                    m_name = jsonItem.getValue();
                    break;
                case "family":
                    m_family = jsonItem.getValue();
                    break;
                case "account_type":
                    m_account_type = AccountType.values()[Integer.valueOf(jsonItem.getValue()).intValue()];
                    break;
                case "cats":
                    getUserCats().initByJSON(jsonItem);
                    break;
                case "cid":
                    setCountry(Integer.valueOf(jsonItem.getValue()).intValue());
                    break;
                case "phone":
                    setPhone(String.valueOf(jsonItem.getValue()));
                    break;
                case "optional_company_name":
                    setBisness(jsonItem.getValue());
                    break;
                case VKScopes.EMAIL:
                    setEmail(String.valueOf(jsonItem.getValue()));
                    break;
                case "optional_bid":
                    setBid(Integer.valueOf(jsonItem.getValue()).intValue());
                    break;
                case VKApiUserFull.ABOUT:
                    setAbout(jsonItem.getValue());
                    break;
                case "update_time":
                    try
                    {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        m_update_time = dateFormat.parse(jsonItem.getValue()).getTime();
                    }
                    catch (ParseException e)
                    {

                    }
                    break;
                default:
                    break;
            }
        }
        Log.d("myLogs-m_id", "m_i d" + m_id);
        Log.d("myLogs-m_id", "getUserCats " + getUserCats().getCount());
    }

    public void setId(int id) {
        m_id = id;
    }

    public void setName(String name) {
        m_name = name;
    }

    public void setFamily(String family) {
        m_family = family;
    }

    public void setAccountType(int accountType) {
        setAccountType(AccountType.values()[accountType]);
    }

    public void setAccountType(AccountType accountType) {
        m_account_type = accountType;
    }

    public void setTown(int tid) {
        m_tid = tid;
    }

    public void setCountry(int cid) {
        m_cid = cid;
    }

    public void setPhone(String phone) {
        m_phone = phone;
    }

    public void setEmail(String email) {
        m_email = email;
    }

    public void setBid(int bid) {
        m_bid = bid;
    }

    public void setAbout(String about) {
        m_about = about;
    }


    public String getPhone() {
        return m_phone;
    }

    public int getId() {
        return m_id;
    }

    public String getFullName() {
        return m_name + " " + m_family;
    }

    public String getName() {
        return m_name;
    }

    public String getFamily() {
        return m_family;
    }

    public String getTown() {
        return new CitiesData().getById(getTid()).name;
    }

    public int getTid() {
        return m_tid;
    }

    public AccountType getAccountType() {
        return m_account_type;
    }

    public UserCats getUserCats() {
        return m_cats;
    }

    public int getCountry() {
        return m_cid;
    }

    public void setBisness(String bisness) {
        m_bisness = bisness;
    }

    public String getEmail() {
        return m_email;
    }

    public String getBisness() {
        return m_bisness;
    }

    public int getBid() {
        return m_bid;
    }

    public String getBidString() {
        return new KindOfBisnessData().getItem(getBid()).name;
    }

    public String getAbout() {
        return m_about;
    }

    public long getLastTimeUpdate()
    {
        return m_update_time;
    }

    public boolean checkUserLoaded()
    {
        return (m_name.length() > 0);
    }
}

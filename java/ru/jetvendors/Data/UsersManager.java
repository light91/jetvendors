package ru.jetvendors.Data;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

import ru.jetvendors.ExternLibs.NotifyDataSetChanged;

public class UsersManager {
    private static UsersManager m_instance;
    private NotifyDataSetChanged m_notify_data_set_change_listener;
    private UsersRequest m_request_manager;
    private List<UserInfoBase> m_users;

    public static UsersManager instance() {
        if (m_instance == null) {
            m_instance = new UsersManager();
        }
        return m_instance;
    }

    private UsersManager() {
        this.m_users = new ArrayList();
        this.m_request_manager = new UsersRequest();
    }

    public void setNotifyDataSetChangeListener(NotifyDataSetChanged notifyDataSetChanged) {
        Log.d("myLogs-vendors", "user:: setNotifyDataSetChanged ");
        this.m_notify_data_set_change_listener = notifyDataSetChanged;
    }

    public UserInfoBase get(int id) {
        return m_users.get(id);
    }

    public UserInfoBase getById(int uid)
    {
        for (UserInfoBase user : this.m_users)
            if (user.getId() == uid)
                return user;

        addEmpty(uid);
        return get(this.m_users.size() - 1);
    }

    public void add(UserInfoBase user) {
        m_users.add(user);
        notifyDataSetChanged();
    }

    private void addEmpty(int uid) {
        UserInfoBase user = new UserInfoBase();
        user.setId(uid);
        m_users.add(user);
        requestData(uid);
    }

    private void requestData(int uid)
    {
        Log.d("myLogs", "getUserData: " + uid);
        m_request_manager.get_user_data(uid);
    }

    public void onAvatarLoaded(int uid) {
        Log.d("myLogs-onAvatarLoaded", "uid " + uid);
        notifyDataSetChanged();
    }

    public void update(int uid, UserInfoBase user)
    {
        getById(uid).update(user);
        notifyDataSetChanged();
    }

    private void notifyDataSetChanged() {
        if (this.m_notify_data_set_change_listener != null) {
            this.m_notify_data_set_change_listener.notifyDataSetChange();
        }
    }
}

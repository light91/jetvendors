package ru.jetvendors.Data;

import android.os.Bundle;
import android.util.Log;

import ru.jetvendors.ExternLibs.DataRequestItem;
import ru.jetvendors.ExternLibs.DataRequestManager;
import ru.jetvendors.Offers.Libs.RequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class OffersManager extends DataRequestManager<OffersItem> {
    private DataRequestItem m_request;

    /* renamed from: ru.jetvendors.Data.OffersManager.1 */
    class C05401 extends DataRequestItem {
        private RequestManager m_request_manager;

        C05401() {
            this.m_request_manager = new RequestManager();
        }

        public void requestItems(Bundle bundle) {
            this.m_request_manager.get_user_offers(Integer.valueOf(bundle.getString("uid")).intValue(), OffersManager.this);
        }
    }

    public OffersManager() {
        this.m_request = new C05401();
        setRequest(this.m_request);
    }

    public OffersItem onInitByJson(JsonData jsonData) {
        OffersItem offersItem = new OffersItem();
        offersItem.addArray(jsonData);
        offersItem.setId(Integer.valueOf(jsonData.getKey()).intValue());
        return offersItem;
    }

    protected void onPrepareRequestData(Bundle requestData) {
    }

    public void update(int id, OffersItem updateItem) {
        Log.d("myLogs-add", " 2 `" + id);
        OffersItem item = getById(id);
        Log.d("myLogs-add", " 2 ``");
        item.update(updateItem);
        Log.d("myLogs-add", " 2 ```");
        notifyDataSetChanged();
        Log.d("myLogs-add", " 2 ````");
    }
}

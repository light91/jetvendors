package ru.jetvendors.Data;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import ru.jetvendors.Data.FullCategoriesDB.Category;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;
import ru.jetvendors.UserCategories.Libs.CategoriesSpecial;

public class UserCats {
    private List<Category> m_categories;
    private FullCategoriesDB m_db;

    public UserCats() {
        this.m_db = new FullCategoriesDB();
        this.m_categories = new ArrayList();
    }

    protected void finalize() throws Throwable {
        this.m_db.close();
        super.finalize();
    }

    public void initByJSON(JsonData jsonData) {
        if (jsonData != null && jsonData.getObject() != null) {
            Iterator i$ = jsonData.getObject().iterator();
            while (i$.hasNext()) {
                add(Integer.valueOf(((JsonData) i$.next()).getValue()).intValue());
            }
        }
    }

    public void add(int id) {
        Category category = this.m_db.getById(id);
        Log.d("myLogs-add Cats", "id " + id);
        if (get(id) == null) {
            this.m_categories.add(category);
        }
    }

    public Category getById(int id) {
        for (int i = 0; i != this.m_categories.size(); i++) {
            if (((Category) this.m_categories.get(i)).id == id) {
                return (Category) this.m_categories.get(i);
            }
        }
        return null;
    }

    public String getCatsText() {
        return getCatsText(": ", ", ");
    }

    public String getCatsText(String subCatsDivider, String catsDivider) {
        List<Integer> cats = new ArrayList();
        for (Category category : this.m_categories) {
            cats.add(Integer.valueOf(category.id));
        }
        return CategoriesSpecial.getCatsText(cats, subCatsDivider, catsDivider, "\n");
    }

    public boolean checkExists(int id) {
        for (Category category : this.m_categories) {
            if (category.id == id) {
                return true;
            }
        }
        return false;
    }

    public Category get(int index) {
        return get(-1, index);
    }

    public Category get(int pid, int index) {
        int itemNumber = 0;
        for (int i = 0; i != this.m_categories.size(); i++) {
            if (((Category) this.m_categories.get(i)).pid == pid) {
                int itemNumber2 = itemNumber + 1;
                if (itemNumber == index) {
                    itemNumber = itemNumber2;
                    return (Category) this.m_categories.get(i);
                }
                itemNumber = itemNumber2;
            }
        }
        return null;
    }

    public int getCount() {
        return getCount(-1);
    }

    public int getCount(int pid) {
        int itemCount = 0;
        for (int i = 0; i != this.m_categories.size(); i++) {
            if (((Category) this.m_categories.get(i)).pid == pid) {
                itemCount++;
            }
        }
        return itemCount;
    }

    public void remove(int id) {
        for (int i = 0; i != this.m_categories.size(); i++) {
            if (((Category) this.m_categories.get(i)).id == id) {
                this.m_categories.remove(i);
                return;
            }
        }
    }
}

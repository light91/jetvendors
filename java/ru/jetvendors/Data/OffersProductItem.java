package ru.jetvendors.Data;

import android.util.Log;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.model.VKApiUserFull;
import java.util.Iterator;
import ru.jetvendors.BuildConfig;
import ru.jetvendors.ExternLibs.DataItem;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class OffersProductItem extends DataItem {
    public static final int INVALID_ID = -1;
    private String m_about;
    private int m_cats_id;
    private int m_count;
    private int m_count_id;
    private String m_price;
    private int m_sub_cats_id;

    public OffersProductItem() {
        this.m_cats_id = INVALID_ID;
        this.m_sub_cats_id = INVALID_ID;
        this.m_count = 0;
        this.m_count_id = INVALID_ID;
        this.m_about = BuildConfig.FLAVOR;
        setId(INVALID_ID);
    }

    public static OffersProductItem initByJson(JsonData jsonArray) {
        OffersProductItem product = new OffersProductItem();
        Log.d("myLogs-jsonArray", "/");
        Iterator i$ = jsonArray.getObject().iterator();
        while (i$.hasNext())
        {
            JsonData jsonItem = (JsonData) i$.next();
            String key = jsonItem.getKey();
            switch (key)
            {
                case "sub_cat_id":
                {
                    product.m_sub_cats_id = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                }
                case "cat_id":
                {
                    product.m_cats_id = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                }
                case "count_id":
                {
                    product.m_count_id = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                }
                case "id":
                {
                    product.setId(Integer.valueOf(jsonItem.getValue()).intValue());
                    break;
                }
                case VKApiUserFull.ABOUT:
                {
                    product.m_about = String.valueOf(jsonItem.getValue());
                    break;
                }
                case VKApiConst.COUNT:
                {
                    product.m_count = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                }
                case "price":
                {
                    product.m_price = String.valueOf(jsonItem.getValue());
                    break;
                }
            }
        }
        Log.d("myLogs-jsonArray", "/");
        return product;
    }

    public int getCatId() {
        return this.m_cats_id;
    }

    public int getSubCatId() {
        return this.m_sub_cats_id;
    }

    public int getCount() {
        return this.m_count;
    }

    public int getCountId() {
        return this.m_count_id;
    }

    public String getPrice() {
        return this.m_price;
    }

    public String getAbout() {
        return this.m_about;
    }

    public void setCatId(int id) {
        this.m_cats_id = id;
    }

    public void setSubCatId(int id) {
        this.m_sub_cats_id = id;
    }

    public void setCount(int count) {
        this.m_count = count;
    }

    public void setUnitId(int unitId) {
        this.m_count_id = unitId;
    }

    public void setPrice(String price) {
        this.m_price = price;
    }

    public void setAbout(String about) {
        this.m_about = about;
    }
}

package ru.jetvendors.Data;

import java.util.ArrayList;
import java.util.List;
import ru.jetvendors.BuildConfig;

public class CitiesData {
    private List<City> m_list;
    private OnDataChange m_on_data_change;

    public static class City {
        public int cid;
        public int id;
        public String name;

        public City(int id, int cid, String name) {
            this.id = id;
            this.cid = cid;
            this.name = name;
        }
    }

    interface OnDataChange {
        void onDataChange();
    }

    public CitiesData() {
        this.m_on_data_change = null;
        this.m_list = new ArrayList();
        init();
    }

    public int getCount() {
        return this.m_list.size();
    }

    public int getCount(int cid) {
        int count = 0;
        for (int index = 0; index != this.m_list.size(); index++) {
            if (((City) this.m_list.get(index)).cid == cid) {
                count++;
            }
        }
        return count;
    }

    public int getCountByCid(int cid) {
        int count = 0;
        for (int index = 0; index != this.m_list.size(); index++) {
            if (((City) this.m_list.get(index)).cid == cid) {
                count++;
            }
        }
        return count;
    }

    public int getId(int index) {
        if (index < 0 || index >= this.m_list.size()) {
            return -1;
        }
        return ((City) this.m_list.get(index)).id;
    }

    public int getIndexById(int id, int cid) {
        int count = -1;
        for (int index = 0; index != this.m_list.size(); index++) {
            if (((City) this.m_list.get(index)).cid == cid) {
                count++;
                if (((City) this.m_list.get(index)).id == id) {
                    return count;
                }
            }
        }
        return -1;
    }

    public City getCity(int index) {
        if (index < 0 || index >= this.m_list.size()) {
            return null;
        }
        return (City) this.m_list.get(index);
    }

    public City getById(int id) {
        for (int index = 0; index != this.m_list.size(); index++) {
            if (((City) this.m_list.get(index)).id == id) {
                return (City) this.m_list.get(index);
            }
        }
        return null;
    }

    public City getById(int cid, int id) {
        int number = 0;
        for (int index = 0; index != this.m_list.size(); index++) {
            if (((City) this.m_list.get(index)).cid == cid) {
                int number2 = number + 1;
                if (number == id) {
                    number = number2;
                    return (City) this.m_list.get(index);
                }
                number = number2;
            }
        }
        return null;
    }

    public void clear() {
        this.m_list.clear();
    }

    public void add(City data) {
        this.m_list.add(data);
        onDataChange();
    }

    public void init() {
        add(new City(0, 1, "\u0411\u0430\u043a\u0443"));
        add(new City(1, 5, "\u0412\u0430\u043d\u0430\u0434\u0437\u043e\u0440"));
        add(new City(2, 5, "\u0413\u044e\u043c\u0440\u0438"));
        add(new City(3, 5, "\u0415\u0440\u0435\u0432\u0430\u043d"));
        add(new City(4, 2, "\u0411\u0430\u0440\u0430\u043d\u043e\u0432\u0438\u0447\u0438"));
        add(new City(5, 2, "\u0411\u0435\u0440\u0435\u0437\u0438\u043d\u043e"));
        add(new City(6, 2, "\u0411\u043e\u0431\u0440\u0443\u0439\u0441\u043a"));
        add(new City(7, 2, "\u0411\u0440\u0430\u0433\u0438\u043d"));
        add(new City(8, 2, "\u0411\u0440\u0430\u0441\u043b\u0430\u0432"));
        add(new City(9, 2, "\u0411\u0440\u0435\u0441\u0442"));
        add(new City(10, 2, "\u0412\u0438\u0442\u0435\u0431\u0441\u043a"));
        add(new City(11, 2, "\u0412\u043e\u043b\u043e\u0436\u0438\u043d"));
        add(new City(12, 2, "\u0413\u043e\u043c\u0435\u043b\u044c"));
        add(new City(13, 2, "\u0413\u0440\u043e\u0434\u043d\u043e"));
        add(new City(14, 2, "\u0416\u043b\u043e\u0431\u0438\u043d"));
        add(new City(15, 2, "\u041a\u043e\u043f\u044b\u043b\u044c"));
        add(new City(16, 2, "\u041c\u0438\u043d\u0441\u043a"));
        add(new City(17, 2, "\u041c\u043e\u0433\u0438\u043b\u0451\u0432"));
        add(new City(18, 2, "\u041c\u043e\u0437\u044b\u0440\u044c"));
        add(new City(19, 2, "\u041c\u0441\u0442\u0438\u0441\u043b\u0430\u0432\u043b\u044c"));
        add(new City(20, 2, "\u041c\u044f\u0434\u0435\u043b\u044c"));
        add(new City(21, 2, "\u041e\u0440\u0448\u0430"));
        add(new City(22, 2, "\u041e\u0441\u0438\u043f\u043e\u0432\u0438\u0447\u0438"));
        add(new City(23, 2, "\u041f\u0438\u043d\u0441\u043a"));
        add(new City(24, 2, "\u041f\u043e\u043b\u043e\u0446\u043a"));
        add(new City(25, 2, "\u0421\u0432\u0438\u0441\u043b\u043e\u0447\u044c"));
        add(new City(26, 2, "\u0421\u043b\u043e\u043d\u0438\u043c"));
        add(new City(27, 2, "\u0421\u043c\u0435\u0442\u0430\u043d\u0438\u0447\u0438"));
        add(new City(28, 2, "\u0421\u043e\u043b\u0438\u0433\u043e\u0440\u0441\u043a"));
        add(new City(29, 4, "\u0410\u0445\u0430\u043b\u0446\u0438\u0445\u0435"));
        add(new City(31, 4, "\u0411\u0430\u0442\u0443\u043c\u0438"));
        add(new City(32, 4, "\u0413\u043e\u0440\u0438"));
        add(new City(33, 4, "\u0417\u0435\u0441\u0442\u0430\u0444\u043e\u043d\u0438"));
        add(new City(34, 4, "\u0417\u0443\u0433\u0434\u0438\u0434\u0438"));
        add(new City(35, 4, "\u041a\u0443\u0442\u0430\u0438\u0441\u0438"));
        add(new City(36, 4, "\u041f\u043e\u0442\u0438"));
        add(new City(37, 4, "\u0421\u0430\u043c\u0442\u0440\u0435\u0434\u0438\u0430"));
        add(new City(38, 4, "\u0421\u0443\u0445\u0443\u043c\u0438"));
        add(new City(39, 4, "\u0422\u0431\u0438\u043b\u0438\u0441\u0438"));
        add(new City(40, 4, "\u0426\u0445\u0438\u043d\u0432\u0430\u043b\u0438"));
        add(new City(41, 0, "\u0410\u043a\u0441\u0430\u0439"));
        add(new City(42, 0, "\u0410\u043a\u0442\u0430\u0443"));
        add(new City(43, 0, "\u0410\u043a\u0442\u044e\u0431\u0438\u043d\u0441\u043a"));
        add(new City(44, 0, "\u0410\u043b\u043c\u0430-\u0410\u0442\u0430"));
        add(new City(45, 0, "\u0410\u0440\u043a\u0430\u043b\u044b\u043a"));
        add(new City(46, 0, "\u0410\u0441\u0442\u0430\u043d\u0430"));
        add(new City(47, 0, "\u0410\u0442\u0431\u0430\u0441\u0430\u0440"));
        add(new City(48, 0, "\u0411\u0430\u0439\u043a\u043e\u043d\u0443\u0440"));
        add(new City(49, 0, "\u0411\u0430\u043b\u0445\u0430\u0448"));
        add(new City(50, 0, "\u0413\u0443\u0440\u044c\u0435\u0432"));
        add(new City(51, 0, "\u0414\u0436\u0430\u043c\u0431\u0443\u043b"));
        add(new City(52, 0, "\u0414\u0436\u0435\u0437\u043a\u0430\u0437\u0433\u0430\u043d"));
        add(new City(53, 0, "\u0414\u0436\u0435\u0442\u044b\u0433\u0430\u0440\u0430"));
        add(new City(54, 0, "\u0417\u044b\u0440\u044f\u043d\u043e\u0432\u0441\u043a"));
        add(new City(55, 0, "\u041a\u0430\u0440\u0430\u0431\u0430\u043b\u044b\u043a"));
        add(new City(56, 0, "\u041a\u0430\u0440\u0430\u0433\u0430\u043d\u0434\u0430"));
        add(new City(57, 0, "\u041a\u0437\u044b\u043b-\u041e\u0440\u0434\u0430"));
        add(new City(58, 0, "\u041a\u043e\u043a\u0447\u0435\u0442\u0430\u0432"));
        add(new City(59, 0, "\u041a\u0443\u0441\u0442\u0430\u043d\u0430\u0439"));
        add(new City(60, 0, "\u041b\u0438\u0441\u0430\u043a\u043e\u0432\u0441\u043a"));
        add(new City(61, 0, "\u041f\u0430\u0432\u043b\u043e\u0434\u0430\u0440"));
        add(new City(62, 0, "\u041f\u0435\u0442\u0440\u043e\u043f\u0430\u0432\u043b\u043e\u0432\u0441\u043a"));
        add(new City(63, 0, "\u0420\u0438\u0434\u0434\u0435\u0440"));
        add(new City(64, 0, "\u0420\u0443\u0434\u043d\u044b\u0439"));
        add(new City(65, 0, "\u0421\u0430\u0440\u044b\u0430\u0433\u0430\u0447"));
        add(new City(66, 0, "\u0421\u0435\u043c\u0438\u043f\u0430\u043b\u0430\u0442\u0438\u043d\u0441\u043a"));
        add(new City(67, 0, "\u0421\u0442\u0435\u043f\u043d\u043e\u0433\u043e\u0440\u0441\u043a"));
        add(new City(68, 0, "\u0422\u0430\u043b\u0434\u044b-\u041a\u0443\u0440\u0433\u0430\u043d"));
        add(new City(69, 0, "\u0422\u0435\u043c\u0438\u0440\u0442\u0430\u0443"));
        add(new City(70, 0, "\u0422\u0443\u0440\u043a\u0435\u0441\u0442\u0430\u043d"));
        add(new City(71, 0, "\u0423\u0440\u0430\u043b\u044c\u0441\u043a"));
        add(new City(72, 0, "\u0423\u0441\u0442\u044c-\u041a\u0430\u043c\u0435\u043d\u043e\u0433\u043e\u0440\u0441\u043a"));
        add(new City(74, 0, "\u0425\u0440\u043e\u043c\u0442\u0430\u0443"));
        add(new City(75, 0, "\u0427\u0438\u043c\u043a\u0435\u043d\u0442"));
        add(new City(76, 0, "\u0429\u0443\u0447\u0438\u043d\u0441\u043a"));
        add(new City(77, 0, "\u042d\u043a\u0438\u0431\u0430\u0441\u0442\u0443\u0437"));
        add(new City(78, 6, "\u0411\u0438\u0448\u043a\u0435\u043a"));
        add(new City(79, 6, "\u0414\u0436\u0435\u043b\u0430\u043b-\u0410\u0431\u0430\u0434"));
        add(new City(80, 6, "\u041a\u0430\u0440\u0430\u043a\u043e\u043b"));
        add(new City(81, 6, "\u041d\u0430\u0440\u044b\u043d"));
        add(new City(82, 6, "\u041e\u0448"));
        add(new City(83, 6, "\u0420\u044b\u0431\u0430\u0447\u044c\u0435"));
        add(new City(84, 6, "\u0422\u0430\u043b\u0430\u0441"));
        add(new City(85, 6, "\u0427\u043e\u043b\u043f\u043e\u043d-\u0410\u0442\u0430"));
        add(new City(86, 9, "\u0414\u0443\u0448\u0430\u043d\u0431\u0435"));
        add(new City(87, 9, "\u0425\u0443\u0434\u0436\u0430\u043d\u0434"));
        add(new City(88, 10, BuildConfig.FLAVOR));
        add(new City(89, 12, "\u0410\u043d\u0434\u0438\u0436\u0430\u043d"));
        add(new City(90, 12, "\u0411\u0443\u0445\u0430\u0440\u0430"));
        add(new City(91, 12, "\u041a\u0430\u0440\u0448\u0438"));
        add(new City(92, 12, "\u041a\u043e\u043a\u0430\u043d\u0434"));
        add(new City(93, 12, "\u041d\u0430\u0432\u043e\u0438"));
        add(new City(94, 12, "\u0421\u0430\u043c\u0430\u0440\u043a\u0430\u043d\u0434"));
        add(new City(95, 12, "\u0422\u0430\u0448\u043a\u0435\u043d\u0442"));
        add(new City(96, 12, "\u0422\u0435\u0440\u043c\u0435\u0437"));
        add(new City(97, 12, "\u0423\u0440\u0433\u0435\u043d\u0447"));
    }

    public void setOnDataChange(OnDataChange onDataChange) {
        this.m_on_data_change = onDataChange;
    }

    public void onDataChange() {
        if (this.m_on_data_change != null) {
            this.m_on_data_change.onDataChange();
        }
    }
}

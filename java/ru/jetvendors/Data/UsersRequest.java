package ru.jetvendors.Data;

import android.util.Log;
import android.util.Pair;
import java.nio.Buffer;
import java.nio.ByteBuffer;

import ru.jetvendors.Application;
import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class UsersRequest extends BaseAsyncRequestManager {
    private static final String host_api = "http://qazaqtv.kz/JetVendors/api.php";

    class ResponseObjectGetUserData extends ResponseObject
    {
        public void doResponse(Request requestData)
        {
            JsonData jsonListData = getDecodeData().getObject().get(0);
            UserInfoBase user = new UserInfoBase();
            user.initByJson(jsonListData);
            UsersManager.instance().update(user.getId(), user);
        }
    }

    class ResponseObjectUserAvatarUpload extends ResponseObject {
        ResponseObjectUserAvatarUpload() {
            super();
        }

        public void doResponse(Request requestData) {
        }
    }

    public void get_user_data(int uid) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_user_data");
        requestData.addData("uid", String.valueOf(uid));
        requestData.setUrl(host_api);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetUserData())});
        addRequest(currentRequest);
    }
/*
    public void user_avatar_upload(String name) {
        AsyncRequest currentRequest = new AsyncRequest();
        Buffer avatarData = ByteBuffer.allocate(BitmapManager.instance().get(name).getByteCount());
        BitmapManager.instance().get(name).copyPixelsToBuffer(avatarData);
        Request requestData = new Request(false);
        requestData.addData("action", "user_avatar_load");
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        requestData.addData("name", name);
        requestData.addData("avatar", String.valueOf(avatarData));
        requestData.setUrl(host_api);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectUserAvatarUpload())});
        addRequest(currentRequest);
    }*/
}

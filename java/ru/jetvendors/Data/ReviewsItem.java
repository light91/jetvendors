package ru.jetvendors.Data;

import java.util.Iterator;

import ru.jetvendors.ExternLibs.DataItem;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class ReviewsItem extends DataItem {
    private String m_review;
    private int m_uid;

    public ReviewsItem(int id, int uid, String review) {
        setId(id);
        this.m_uid = uid;
        this.m_review = review;
    }

    public ReviewsItem(JsonData jsonData) {
        Iterator i$ = jsonData.getObject().iterator();
        while (i$.hasNext()) {
            JsonData jsonItem = (JsonData) i$.next();
            String key = jsonItem.getKey();
            switch (key)
            {
                case "id":
                    setId(Integer.valueOf(jsonItem.getValue()).intValue());
                    break;
                case "text":
                    this.m_review = jsonItem.getValue();
                    break;
                case "uid":
                    this.m_uid = Integer.valueOf(jsonItem.getValue()).intValue();
                    UsersManager.instance().getById(this.m_uid);
                    break;
                default:
                    break;
            }
        }
    }

    public String getReview() {
        return this.m_review;
    }

    public UserInfoBase getUser() {
        return UsersManager.instance().getById(this.m_uid);
    }
}

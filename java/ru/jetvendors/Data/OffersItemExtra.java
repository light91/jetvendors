package ru.jetvendors.Data;

import java.sql.Timestamp;
import java.util.Iterator;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class OffersItemExtra {
    private Timestamp m_time;
    private int m_uid;

    public void initByJson(JsonData jsonListData) {
        Iterator i$ = jsonListData.getObject().iterator();
        while (i$.hasNext()) {
            JsonData jsonItem = (JsonData) i$.next();
            String key = jsonItem.getKey();
            switch (key)
            {
                case "time":
                    setTime(Timestamp.valueOf(jsonItem.getValue()));
                    break;
                case "uid":
                    m_uid = Integer.valueOf(jsonItem.getValue()).intValue();
                    break;
                default:
                    break;
            }
        }
    }

    public Timestamp getTime() {
        return this.m_time;
    }

    public String getVisibleTime() {
        return this.m_time.toString();
    }

    public void setTime(Timestamp time) {
        this.m_time = time;
    }

    public UserInfoBase getUserInfo() {
        return UsersManager.instance().getById(this.m_uid);
    }

    public int getUid() {
        return this.m_uid;
    }
}

package ru.jetvendors.Data;

import android.os.Bundle;

import ru.jetvendors.ExternLibs.DataRequestItem;
import ru.jetvendors.ExternLibs.DataRequestManager;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.Profile.Libs.RequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class ReviewsManager extends DataRequestManager<ReviewsItem> {
    DataRequestItem m_request;
    private int m_uid;

    /* renamed from: ru.jetvendors.Data.ReviewsManager.1 */
    class C05411 extends DataRequestItem {
        private RequestManager m_requests;

        C05411() {
            this.m_requests = new RequestManager();
        }

        public void requestItems(Bundle bundle) {
            this.m_requests.get_reviews(bundle, ReviewsManager.this);
        }
    }

    public ReviewsManager(int uid) {
        this.m_request = new C05411();
        setRequest(this.m_request);
        this.m_uid = uid;
    }

    public void onPrepareRequestData(Bundle requestData) {
        requestData.putString("uid", String.valueOf(this.m_uid));
    }

    public ReviewsItem onInitByJson(JsonData jsonData) {
        return new ReviewsItem(jsonData);
    }

    public void setNotifyDataSetChanged(NotifyDataSetChanged notifyDataSetChanged) {
        super.setNotifyDataSetChanged(notifyDataSetChanged);
        UsersManager.instance().setNotifyDataSetChangeListener(notifyDataSetChanged);
    }
}

package ru.jetvendors.Data;

import java.util.ArrayList;
import java.util.List;

public class KindOfBisnessData {
    private List<KindOfBisness> m_list;
    private OnDataChange m_on_data_change;

    public static class KindOfBisness {
        public int id;
        public String name;

        public KindOfBisness(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    interface OnDataChange {
        void onDataChange();
    }

    public KindOfBisnessData() {
        this.m_on_data_change = null;
        this.m_list = new ArrayList();
        init();
    }

    public int getCount() {
        return this.m_list.size();
    }

    public int getId(int index) {
        if (index < 0 || index >= this.m_list.size()) {
            return -1;
        }
        return ((KindOfBisness) this.m_list.get(index)).id;
    }

    public int getIndexById(int id) {
        for (int index = 0; index != this.m_list.size(); index++) {
            if (((KindOfBisness) this.m_list.get(index)).id == id) {
                return index;
            }
        }
        return -1;
    }

    public KindOfBisness getItem(int index) {
        if (index < 0 || index >= this.m_list.size()) {
            return null;
        }
        return (KindOfBisness) this.m_list.get(index);
    }

    public void clear() {
        this.m_list.clear();
    }

    public void add(KindOfBisness data) {
        this.m_list.add(data);
        onDataChange();
    }

    public void init() {
        add(new KindOfBisness(0, "\u0422\u043e\u0447\u043a\u0430 \u043f\u0438\u0442\u0430\u043d\u0438\u044f"));
        add(new KindOfBisness(1, "\u041c\u0430\u0433\u0430\u0437\u0438\u043d"));
    }

    public void setOnDataChange(OnDataChange onDataChange) {
        this.m_on_data_change = onDataChange;
    }

    public void onDataChange() {
        if (this.m_on_data_change != null) {
            this.m_on_data_change.onDataChange();
        }
    }
}

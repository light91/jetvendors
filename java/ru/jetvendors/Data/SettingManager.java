package ru.jetvendors.Data;

public class SettingManager {
    private boolean m_show_notifications;

    public SettingManager() {
        this.m_show_notifications = true;
    }

    public void setShowNotifications(boolean value) {
        this.m_show_notifications = value;
    }

    public boolean getShowNotifications() {
        return this.m_show_notifications;
    }
}

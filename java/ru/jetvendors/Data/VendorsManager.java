package ru.jetvendors.Data;

import android.os.Bundle;
import android.util.Log;

import ru.jetvendors.ExternLibs.DataRequestItem;
import ru.jetvendors.ExternLibs.DataRequestManager;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class VendorsManager extends DataRequestManager<VendorsItem> {
    private DataRequestItem m_request;

    /* renamed from: ru.jetvendors.Data.VendorsManager.1 */
    class C05421 extends DataRequestItem {
        private VendorsRequestManager m_requests;

        C05421() {
            this.m_requests = new VendorsRequestManager();
        }

        public void requestItems(Bundle bundle) {
            this.m_requests.get_vendors(bundle, VendorsManager.this);
        }
    }

    public VendorsManager() {
        this.m_request = new C05421();
        setRequest(this.m_request);
    }

    public VendorsItem onInitByJson(JsonData jsonData) {
        VendorsItem item = new VendorsItem();
        item.initByJson(jsonData);
        return item;
    }

    public void setNotifyDataSetChanged(NotifyDataSetChanged notifyDataSetChanged) {
        super.setNotifyDataSetChanged(notifyDataSetChanged);
        Log.d("myLogs-vendors", " setNotifyDataSetChanged ");
        UsersManager.instance().setNotifyDataSetChangeListener(notifyDataSetChanged);
    }
}

package ru.jetvendors.Data;

import java.util.ArrayList;
import java.util.List;
import ru.jetvendors.R;

public class CountriesData {
    private List<Country> m_list;
    private OnDataChange m_on_data_change;

    public static class Country {
        public int id;
        public String name;
        public String number;
        public int previewRes;

        public Country(int id, String name, int previewRes, String number) {
            this.id = id;
            this.name = name;
            this.previewRes = previewRes;
            this.number = number;
        }
    }

    interface OnDataChange {
        void onDataChange();
    }

    public CountriesData() {
        this.m_on_data_change = null;
        this.m_list = new ArrayList();
        init();
    }

    public int getCount() {
        return this.m_list.size();
    }

    public int getId(int index) {
        if (index < 0 || index >= this.m_list.size()) {
            return -1;
        }
        return ((Country) this.m_list.get(index)).id;
    }

    public Country getCountry(int index) {
        if (index < 0 || index >= this.m_list.size()) {
            return null;
        }
        return (Country) this.m_list.get(index);
    }

    public Country getById(int id) {
        for (int index = 0; index != this.m_list.size(); index++) {
            if (((Country) this.m_list.get(index)).id == id) {
                return (Country) this.m_list.get(index);
            }
        }
        return null;
    }

    public int getPreview(int id) {
        Country country = getById(id);
        if (country == null) {
            return -1;
        }
        return country.previewRes;
    }

    public int getPreviewByIndex(int index) {
        if (index < 0 || index >= this.m_list.size()) {
            return -1;
        }
        return ((Country) this.m_list.get(index)).previewRes;
    }

    public void clear() {
        this.m_list.clear();
    }

    public void add(Country data) {
        this.m_list.add(data);
        onDataChange();
    }

    public void init() {
        add(new Country(0, "\u041a\u0430\u0437\u0430\u0445\u0441\u0442\u0430\u043d", R.drawable.kazakstan, "+7"));
        add(new Country(1, "\u0410\u0437\u0435\u0440\u0431\u0430\u0439\u0434\u0436\u0430\u043d", R.drawable.azerbaijan, "+994"));
        add(new Country(2, "\u0411\u0435\u043b\u043e\u0440\u0443\u0441\u0441\u0438\u044f", R.drawable.belorussia, "+375"));
        add(new Country(3, "\u041a\u0438\u0442\u0430\u0439", R.drawable.china, "86"));
        add(new Country(4, "\u0413\u0440\u0443\u0437\u0438\u044f", R.drawable.gruzia, "+995"));
        add(new Country(5, "\u0410\u0440\u043c\u0435\u043d\u0438\u044f", R.drawable.armenia, "+374"));
        add(new Country(6, "\u041a\u0438\u0440\u0433\u0438\u0437\u0438\u044f", R.drawable.kirgiztan, "+996"));
        add(new Country(7, "\u041c\u043e\u043b\u0434\u043e\u0432\u0438\u044f", R.drawable.moldavia, "+373"));
        add(new Country(8, "\u0420\u043e\u0441\u0441\u0438\u044f", R.drawable.russia, "+7"));
        add(new Country(9, "\u0422\u0430\u0434\u0436\u0438\u043a\u0438\u0441\u0442\u0430\u043d", R.drawable.tadjikistan, "+992"));
        add(new Country(10, "\u0422\u0443\u0440\u043a\u043c\u0435\u043d\u0438\u0441\u0442\u0430\u043d", R.drawable.turkmenistan, "+993"));
        add(new Country(11, "\u0423\u043a\u0440\u0430\u043d\u0438\u0430", R.drawable.untitled, "+38"));
        add(new Country(12, "\u0423\u0437\u0431\u0435\u043a\u0438\u0441\u0442\u0430\u043d", R.drawable.uzbekistan, "+998"));
    }

    public void setOnDataChange(OnDataChange onDataChange) {
        this.m_on_data_change = onDataChange;
    }

    public void onDataChange() {
        if (this.m_on_data_change != null) {
            this.m_on_data_change.onDataChange();
        }
    }
}

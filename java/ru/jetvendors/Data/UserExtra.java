package ru.jetvendors.Data;

import com.vk.sdk.api.model.VKScopes;
import java.util.Iterator;

import ru.jetvendors.Application;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class UserExtra {
    private String m_code;
    private String m_password;
    private int m_sid;
    private int m_uid;

    public void initByJson(JsonData jsonArray) {
        Iterator i$ = jsonArray.getObject().iterator();
        while (i$.hasNext())
        {
            JsonData jsonItem = (JsonData) i$.next();
            String key = jsonItem.getKey();
            switch (key)
            {
                case VKScopes.OFFERS:
                {
                    Application.instance().getOffersManager().addArray(jsonItem);
                    break;
                }
                case "sid":
                {
                    setSid(Integer.valueOf(jsonItem.getValue()).intValue());
                    break;
                }
            }
        }
        UserInfoBase user = new UserInfoBase(jsonArray);
        this.m_uid = user.getId();
        UsersManager.instance().add(user);
    }

    public int getSid() {
        return this.m_sid;
    }

    public int getUid() {
        return this.m_uid;
    }

    public String getPassword() {
        return this.m_password;
    }

    public String getCode() {
        return this.m_code;
    }

    public void setSid(int sid) {
        this.m_sid = sid;
    }

    public void setPassword(String password) {
        this.m_password = password;
    }

    public void setCode(String code) {
        this.m_code = code;
    }
}

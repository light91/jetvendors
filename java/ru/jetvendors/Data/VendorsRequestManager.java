package ru.jetvendors.Data;

import android.os.Bundle;
import android.util.Pair;
import com.vk.sdk.api.VKApiConst;
import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;

public class VendorsRequestManager extends BaseAsyncRequestManager {
    private static final String host_api = "http://qazaqtv.kz/JetVendors/api.php";

    class ResponseObjectGetVendors extends ResponseObject {
        ResponseObjectGetVendors() {
            super();
        }

        public void doResponse(Request requestData) {
            ((VendorsManager) requestData.getObject()).addArray((JsonData) getDecodeData().getObject().get(0));
        }
    }

    public void get_vendors(Bundle requestVendors, VendorsManager vendors) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "get_vendors");
        requestData.addData("last_id", requestVendors.getString("last_id"));
        requestData.addData(VKApiConst.COUNT, requestVendors.getString(VKApiConst.COUNT));
        requestData.addData("town_id", requestVendors.getString("town_id"));
        requestData.addData("cats_id", requestVendors.getString("cats_id"));
        requestData.addData("sub_cats_id", requestVendors.getString("sub_cats_id"));
        requestData.setUrl(host_api);
        requestData.setObject(vendors);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectGetVendors())});
        addRequest(currentRequest);
    }
}

package ru.jetvendors.Data;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ru.jetvendors.ExternLibs.DataItem;
import ru.jetvendors.Application;
import ru.jetvendors.ExternLibs.JsonDeserializeData.JsonData;
import ru.jetvendors.UserCategories.Libs.CategoriesSpecial;

public class VendorsItem extends DataItem {
    private List<Integer> m_cats;

    public VendorsItem() {
        this.m_cats = new ArrayList();
    }

    public void initByJson(JsonData jsonListData)
    {
        Iterator it = jsonListData.getObject().iterator();
        while (it.hasNext()) {
            JsonData vendorData = (JsonData) it.next();
            String key = vendorData.getKey();
            switch (key)
            {
                case "id":
                    setId(Integer.valueOf(vendorData.getValue()).intValue());
                    break;
                case "cat_id":
                    if (vendorData.getObject() == null) {
                        break;
                    }
                    Iterator i$ = vendorData.getObject().iterator();
                    while (i$.hasNext()) {
                        this.m_cats.add(Integer.valueOf(((JsonData) i$.next()).getValue()));
                    }
                    break;
                default:
                    break;
            }
        }
        Log.d("myLogs-vendors", " getId() " + getId());
        getUser();
    }

    public UserInfoBase getUser() {
        return UsersManager.instance().getById(getId());
    }

    public String getCatsText(String subCatsDivider, String catsDivider) {
        Context context = Application.instance().getBaseContext();
        return CategoriesSpecial.getCatsText(this.m_cats, subCatsDivider, catsDivider, "\n");
    }
}

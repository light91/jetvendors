package ru.jetvendors;

import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import java.util.Timer;
import java.util.TimerTask;
import ru.jetvendors.Notifications.Libs.EventsManager;

public class Service extends android.app.Service
{
    public static final String EXTRA_KEY_MSG = "msg";

    public static final String EXTRA_VALUE_LOGIN = "login";
    public static final String EXTRA_VALUE_LOGOUT = "logout";

    public static final String EXTRA_VALUE_SID = "sid";

    private int m_sid;
    private Timer m_timer;


    public Service()
    {
        m_sid = -1;
        Log.d("myLogs-login-construct", "login: sid " + this.m_sid);
    }

    @Nullable
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public void onCreate()
    {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent == null)
            return super.onStartCommand(null, flags, startId);

        switch (intent.getStringExtra(EXTRA_KEY_MSG))
        {
            case EXTRA_VALUE_LOGIN:
            {
                m_sid = intent.getIntExtra(EXTRA_VALUE_SID, -1);
                m_timer = new Timer();
                m_timer.schedule(new EventsTimerTask(), 0, 60 * 1000);
                break;
            }
            case EXTRA_VALUE_LOGOUT:
            {
                m_timer.cancel();
                break;
            }
        }

        return START_STICKY;
    }


    class EventsTimerTask extends TimerTask
    {
        public void run()
        {
            Log.d("myLogs-login-run", "login: sid " + m_sid);
            EventsManager.instance().checkNotifications(m_sid);
        }
    }
}

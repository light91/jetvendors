package ru.jetvendors.SearchVendors.Libs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ru.jetvendors.ExternLibs.BitmapManager;
import ru.jetvendors.R;
import ru.jetvendors.Data.VendorsItem;
import ru.jetvendors.Data.VendorsManager;

public class SearchVendorsAdapter extends BaseAdapter {
    private Context m_context;
    private VendorsManager m_data;

    public SearchVendorsAdapter(Context context, VendorsManager data) {
        this.m_context = context;
        this.m_data = data;
    }

    public int getCount() {
        return this.m_data.getCount();
    }

    public VendorsItem getItem(int i) {
        return (VendorsItem) this.m_data.get(i);
    }

    public long getItemId(int i) {
        return (long) getItem(i).getId();
    }

    public View getView(int position, View contentView, ViewGroup parent) {
        return getGeneralView(getItem(position), contentView, parent);
    }

    public View getDropDownView(int position, View contentView, ViewGroup parent) {
        return getGeneralView(getItem(position), contentView, parent);
    }

    private View getGeneralView(VendorsItem vendor, View contentView, ViewGroup parent) {
        View view = contentView;
        if (view == null)
            view = ((LayoutInflater) m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.search_vendors_item, parent, false);

        ((TextView) view.findViewById(R.id.Name)).setText(vendor.getUser().getFullName());
        ((TextView) view.findViewById(R.id.BusinessType)).setText(vendor.getUser().getBisness());
        ((TextView) view.findViewById(R.id.CategoriesList)).setText(vendor.getCatsText(", ", ": "));
        TextView textView = (TextView) view.findViewById(R.id.About);
        textView.setText(vendor.getUser().getAbout());
        textView.setVisibility(textView.getText().length() != 0 ? View.VISIBLE : View.GONE);
        BitmapManager.instance().setImageView((ImageView) view.findViewById(R.id.Avatar), String.valueOf(vendor.getUser().getId()), vendor.getUser().getLastTimeUpdate());
        return view;
    }

    protected Context getContext() {
        return this.m_context;
    }
}

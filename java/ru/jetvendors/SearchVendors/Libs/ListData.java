package ru.jetvendors.SearchVendors.Libs;

import android.util.Pair;
import java.util.ArrayList;
import java.util.List;

public class ListData {
    private List<Object> m_data;
    private Pair<Integer, String> m_header_value;

    public ListData() {
        this.m_data = new ArrayList();
    }

    public int getCount() {
        return this.m_data.size() + getAddValueCount();
    }

    public int getAddValueCount() {
        if (this.m_header_value != null) {
            return 1;
        }
        return 0;
    }

    public Object get(int index) {
        if (index < 0 || index >= this.m_data.size()) {
            return null;
        }
        return this.m_data.get(index);
    }

    protected void add(Object data) {
        this.m_data.add(data);
    }

    public void addHeaderValue(String value, int id) {
        this.m_header_value = new Pair(Integer.valueOf(id), value);
    }

    public void clear() {
        this.m_data.clear();
    }

    public String toString(int index) {
        if (index != 0 || this.m_header_value == null) {
            return toStringInner(index - getAddValueCount());
        }
        return (String) this.m_header_value.second;
    }

    public int getId(int index) {
        if (index != 0 || this.m_header_value == null) {
            return getIdInner(index - getAddValueCount());
        }
        return ((Integer) this.m_header_value.first).intValue();
    }

    public int getIndexById(int id) {
        for (int index = 0; index != getCount(); index++) {
            if (getIdInner(index) == id) {
                return index;
            }
        }
        return 0;
    }

    public String toStringInner(int index) {
        return null;
    }

    public int getIdInner(int index) {
        return index;
    }
}

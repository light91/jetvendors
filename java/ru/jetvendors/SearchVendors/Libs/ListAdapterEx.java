package ru.jetvendors.SearchVendors.Libs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import ru.jetvendors.R;
import ru.jetvendors.SearchVendors.Libs.ListData;

public class ListAdapterEx extends BaseAdapter
{
    private Context m_context;
    private ListData m_data;


    public ListAdapterEx(Context context, ListData data)
    {
        m_context = context;
        m_data = data;
    }

    public int getCount() {
        return this.m_data.getCount();
    }

    public Object getItem(int i) {
        return this.m_data.get(i);
    }

    public long getItemId(int i) {
        return (long) this.m_data.getId(i);
    }

    public View getView(int position, View contentView, ViewGroup parent)
    {
        return getView(contentView, parent, this.m_data.toString(position));
    }

    private View getView(View contentView, ViewGroup parent, String text)
    {
        if (contentView == null)
            contentView = ((LayoutInflater) m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.listex_item, parent, false);

        ((TextView) contentView.findViewById(R.id.name)).setText(text);
        return contentView;
    }

    protected Context getContext() {
        return this.m_context;
    }
}

package ru.jetvendors.SearchVendors.Libs;

import ru.jetvendors.Offers.Libs.UnitsData;
import ru.jetvendors.Offers.Libs.UnitsData.Unit;

public class ListUnits extends ListData {
    public ListUnits() {
        init();
    }

    private void init() {
        clear();
        UnitsData data = new UnitsData();
        int count = data.getCount();
        for (int i = 0; i != count; i++) {
            add(data.get(i));
        }
    }

    public String toStringInner(int index) {
        return ((Unit) get(index)).name;
    }

    public int getIdInner(int index) {
        return ((Unit) get(index)).id;
    }

    public int getIndexById(int id) {
        for (int index = 0; index != getCount(); index++) {
            if (getId(index) == id) {
                return index;
            }
        }
        return -1;
    }
}

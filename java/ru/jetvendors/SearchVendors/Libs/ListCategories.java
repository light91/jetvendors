package ru.jetvendors.SearchVendors.Libs;

import ru.jetvendors.Data.FullCategoriesDB;
import ru.jetvendors.Data.FullCategoriesDB.Category;
import ru.jetvendors.SearchVendors.Libs.ListData;

public class ListCategories extends ListData {
    private FullCategoriesDB m_db;
    private int m_pid;

    public ListCategories() {
        this.m_db = new FullCategoriesDB();
        initByPid(-1);
    }

    public void initByPid(int pid) {
        clear();
        this.m_pid = pid;
        int count = this.m_db.getCount(this.m_pid);
        for (int i = 0; i != count; i++) {
            add(this.m_db.get(i, this.m_pid));
        }
    }

    protected void finalize() throws Throwable {
        this.m_db.close();
        super.finalize();
    }

    public String toStringInner(int index) {
        return ((Category) get(index)).name;
    }

    public int getIdInner(int index) {
        return ((Category) get(index)).id;
    }
}

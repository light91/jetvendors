package ru.jetvendors.SearchVendors.Libs;

import ru.jetvendors.Data.CitiesData;
import ru.jetvendors.Data.CitiesData.City;

public class ListCities extends ListData {
    private int m_cid;
    private CitiesData m_cities;

    public ListCities(int cid) {
        this.m_cities = new CitiesData();
        init(cid);
    }

    private void init(int cid) {
        clear();
        this.m_cid = cid;
        int count = this.m_cities.getCount(this.m_cid);
        for (int i = 0; i != count; i++) {
            add(this.m_cities.getById(this.m_cid, i));
        }
    }

    public String toStringInner(int index) {
        return ((City) get(index)).name;
    }

    public int getIdInner(int index) {
        return ((City) get(index)).id;
    }
}

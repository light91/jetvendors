package ru.jetvendors.SearchVendors;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import ru.jetvendors.Application;
import ru.jetvendors.BaseFragmentActivity;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.Data.UsersManager;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyDataSetChanged;
import ru.jetvendors.ExternLibs.NotifyOperationComplete;
import ru.jetvendors.Data.VendorsManager;
import ru.jetvendors.Profile.VendorViewFragment;
import ru.jetvendors.SearchVendors.Libs.ListAdapterEx;
import ru.jetvendors.SearchVendors.Libs.ListCategories;
import ru.jetvendors.SearchVendors.Libs.ListCities;
import ru.jetvendors.SearchVendors.Libs.SearchVendorsAdapter;

public class SearchVendorsFragment extends Fragment {
    private NotifyOperationComplete m_func_on_receive_every;
    private OnClickListener m_func_on_request_more;
    private OnItemSelectedListener m_func_on_select_category;
    private OnItemSelectedListener m_func_on_select_subcategory;
    private OnItemSelectedListener m_func_on_select_town;
    private OnItemClickListener m_func_on_select_vendor;
    private ListCategories m_list_subcategories;
    private int m_spinner_event_index;
    private VendorsManager m_vendors;

    class C03561 implements OnItemSelectedListener {
        C03561() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            if (SearchVendorsFragment.access$004(SearchVendorsFragment.this) > 2) {
                Bundle bundle = new Bundle();
                int categoryId = (int) ((Spinner) getView().findViewById(R.id.Category)).getSelectedItemId();
                int subCategoryId = (int) ((Spinner) getView().findViewById(R.id.SubCategory)).getSelectedItemId();
                Bundle filter = new Bundle();
                filter.putString("town_id", String.valueOf(id));
                filter.putString("cats_id", String.valueOf(categoryId));
                filter.putString("sub_cats_id", String.valueOf(subCategoryId));
                m_vendors.requestItems(filter);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: ru.jetvendors.SearchVendors.SearchVendorsFragment.2 */
    class C03572 implements OnItemSelectedListener {
        C03572() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            if (SearchVendorsFragment.access$004(SearchVendorsFragment.this) > 2) {
                m_list_subcategories.clear();
                m_list_subcategories.initByPid((int) id);
                int townId = (int) ((Spinner) getView().findViewById(R.id.Town)).getSelectedItemId();
                Bundle filter = new Bundle();
                filter.putString("town_id", String.valueOf(townId));
                filter.putString("cats_id", String.valueOf(id));
                filter.putString("sub_cats_id", String.valueOf(-1));
                m_vendors.requestItems(filter);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: ru.jetvendors.SearchVendors.SearchVendorsFragment.3 */
    class C03583 implements OnItemSelectedListener {
        C03583() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            if (SearchVendorsFragment.access$004(SearchVendorsFragment.this) > 2) {
                int townId = (int) ((Spinner) getView().findViewById(R.id.Town)).getSelectedItemId();
                int categoryId = (int) ((Spinner) getView().findViewById(R.id.Category)).getSelectedItemId();
                Bundle filter = new Bundle();
                filter.putString("town_id", String.valueOf(townId));
                filter.putString("cats_id", String.valueOf(categoryId));
                filter.putString("sub_cats_id", String.valueOf(id));
                m_vendors.requestItems(filter);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: ru.jetvendors.SearchVendors.SearchVendorsFragment.4 */
    class C03594 implements OnItemClickListener {
        C03594() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            ((BaseFragmentActivity) getActivity()).fragmentForward(VendorViewFragment.createInstance((int) id));
        }
    }

    /* renamed from: ru.jetvendors.SearchVendors.SearchVendorsFragment.6 */
    class C03606 implements OnClickListener {
        C03606() {
        }

        public void onClick(View v) {
            m_vendors.requestItemsNext();
        }
    }

    /* renamed from: ru.jetvendors.SearchVendors.SearchVendorsFragment.5 */
    class C05535 implements NotifyOperationComplete {
        C05535() {
        }

        public void onNotify(Object result) {
            getView().findViewById(R.id.GetMore).setVisibility(View.GONE);
        }
    }

    public SearchVendorsFragment()
    {
        m_spinner_event_index = 0;
        m_func_on_select_town = new C03561();
        m_func_on_select_category = new C03572();
        m_func_on_select_subcategory = new C03583();
        m_func_on_select_vendor = new C03594();
        m_func_on_receive_every = new C05535();
        m_func_on_request_more = new C03606();
    }

    static /* synthetic */ int access$004(SearchVendorsFragment x0) {
        int i = x0.m_spinner_event_index + 1;
        x0.m_spinner_event_index = i;
        return i;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle onSavedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u041f\u043e\u0438\u0441\u043a \u043f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a\u043e\u0432");
        View view = inflater.inflate(R.layout.search_vendors_fragment, container, false);
        ListCities listCities = new ListCities(Application.instance().getUser().getCountry());
        int selectionCity = 0;
        for (int index = 0; index != listCities.getCount(); index++) {
            if (listCities.getId(index) == Application.instance().getUser().getTid()) {
                selectionCity = index;
            }
        }
        ((Spinner) view.findViewById(R.id.Town)).setAdapter(new ListAdapterEx(inflater.getContext(), listCities));
        ((Spinner) view.findViewById(R.id.Town)).setSelection(selectionCity);
        ((Spinner) view.findViewById(R.id.Town)).setOnItemSelectedListener(m_func_on_select_town);
        ListCategories listCategory = new ListCategories();
        listCategory.addHeaderValue("\u0412\u044b\u0431\u0438\u0440\u0438\u0442\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044e", -1);
        ((Spinner) view.findViewById(R.id.Category)).setAdapter(new ListAdapterEx(inflater.getContext(), listCategory));
        ((Spinner) view.findViewById(R.id.Category)).setOnItemSelectedListener(m_func_on_select_category);
        m_list_subcategories = new ListCategories();
        m_list_subcategories.clear();
        m_list_subcategories.addHeaderValue("\u0412\u044b\u0431\u0438\u0440\u0438\u0442\u0435 \u043f\u043e\u0434\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044e", -1);
        ((Spinner) view.findViewById(R.id.SubCategory)).setAdapter(new ListAdapterEx(inflater.getContext(), m_list_subcategories));
        ((Spinner) view.findViewById(R.id.SubCategory)).setOnItemSelectedListener(m_func_on_select_subcategory);
        m_vendors = new VendorsManager();
        ListView listView = (ListView) view.findViewById(R.id.ListVendors);
        BaseAdapter adapter = new SearchVendorsAdapter(inflater.getContext(), m_vendors);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(m_func_on_select_vendor);
        m_vendors.setNotifyDataSetChanged(new NotifyDataSetChanged(adapter));
        UsersManager.instance().setNotifyDataSetChangeListener(new NotifyDataSetChanged(adapter));
        m_vendors.setNotifyReceiveEveryListener(m_func_on_receive_every);
        view.findViewById(R.id.GetMore).setOnClickListener(m_func_on_request_more);
        return view;
    }
}

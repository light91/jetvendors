package ru.jetvendors;

import android.os.Bundle;
import ru.jetvendors.UserCategories.SelectCategoriesFragment;

public class VendorFirstStartActivity extends BaseFragmentActivity
{
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        fragmentSetup(new SelectCategoriesFragment());
    }
}

package ru.jetvendors.General;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyResultListener;

public class ContactFragment extends Fragment {
    private OnClickListener m_func_on_send;
    private NotifyResultListener m_func_on_send_result;

    /* renamed from: ru.jetvendors.General.ContactFragment.1 */
    class C03121 implements OnClickListener {
        C03121() {
        }

        public void onClick(View v) {
            String text = ((EditText) ContactFragment.this.getView().findViewById(R.id.Message)).getText().toString();
            if (text.length() < 20) {
                Toast.makeText(ContactFragment.this.getView().getContext(), "\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435 \u043d\u0435 \u043c\u043e\u0436\u0435\u0442 \u0441\u043e\u0434\u0435\u0440\u0436\u0430\u0442\u044c \u043c\u0435\u043d\u0435\u0435 20 \u0441\u0438\u043c\u0432\u043e\u043b\u043e\u0432", 1).show();
            } else {
                new RequestManager().feedback(text, ContactFragment.this.m_func_on_send_result);
            }
        }
    }

    /* renamed from: ru.jetvendors.General.ContactFragment.2 */
    class C05432 implements NotifyResultListener {
        C05432() {
        }

        public void onSuccess() {
            Toast.makeText(ContactFragment.this.getView().getContext(), "\u0412\u0430\u0448\u0435 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u043e \u0430\u0434\u043b\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438", 1).show();
        }

        public void onError() {
            Toast.makeText(ContactFragment.this.getView().getContext(), "\u041f\u0440\u043e\u0438\u0437\u043e\u0448\u043b\u0430 \u043e\u0448\u0438\u0431\u043a\u0430 \u043f\u0440\u0438 \u043e\u0442\u043f\u0440\u0430\u0432\u043a\u0435 \u043e\u0431\u0440\u0430\u0449\u0435\u043d\u0438\u044f", 1).show();
        }
    }

    public ContactFragment() {
        this.m_func_on_send = new C03121();
        this.m_func_on_send_result = new C05432();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0421\u043f\u0438\u0441\u043e\u043a \u041a\u043e\u043d\u0442\u0430\u043a\u0442\u043e\u0432");
        View view = inflater.inflate(R.layout.general_contact_fragment, container, false);
        view.findViewById(R.id.Send).setOnClickListener(this.m_func_on_send);
        return view;
    }
}

package ru.jetvendors.General;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.R;

public class ShareFragment extends Fragment
{
    private OnClickListener m_func_share_fb;
    private OnClickListener m_func_share_vk;
    private OnClickListener m_func_share_wa;

    /* renamed from: ru.jetvendors.General.ShareFragment.1 */
    class C03141 implements OnClickListener {
        C03141() {
        }

        public void onClick(View v) {
            ShareFragment.this.share("com.vkontakte.android");
        }
    }

    /* renamed from: ru.jetvendors.General.ShareFragment.2 */
    class C03152 implements OnClickListener {
        C03152() {
        }

        public void onClick(View v) {
            ShareFragment.this.share("com.facebook.katana");
        }
    }

    /* renamed from: ru.jetvendors.General.ShareFragment.3 */
    class C03163 implements OnClickListener {
        C03163() {
        }

        public void onClick(View v) {
            ShareFragment.this.share("com.whatsapp");
        }
    }

    public ShareFragment() {
        this.m_func_share_vk = new C03141();
        this.m_func_share_fb = new C03152();
        this.m_func_share_wa = new C03163();
    }

    private boolean share(String pkg) {
        Uri imageUri = getUriFromDrawable();
        Intent sendIntent = new Intent();
        sendIntent.setAction("android.intent.action.SEND");
        sendIntent.setType("image/jpg");
        sendIntent.setPackage(pkg);
        sendIntent.putExtra("android.intent.extra.TITLE", "This is my text to send. 1");
        sendIntent.putExtra("android.intent.extra.SUBJECT", "This is my text to send. 2");
        sendIntent.putExtra("android.intent.extra.TEXT", "This is my text to send. 3");
        sendIntent.putExtra("android.intent.extra.STREAM", imageUri);
        if (getActivity().getPackageManager().resolveActivity(sendIntent, AccessibilityNodeInfoCompat.ACTION_CUT) == null) {
            return false;
        }
        startActivity(sendIntent);
        return true;
    }

    private Uri getUriFromDrawable()
    {
        File file = new File(Environment.getExternalStorageDirectory(), "logo.jpg");
        if (!file.exists())
        {
            FileOutputStream output = null;
            InputStream input = null;
            try
            {
                FileOutputStream output2 = new FileOutputStream(file);
                try
                {
                    input = getView().getContext().getResources().openRawResource(R.drawable.logo);
                    byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
                    while (true)
                    {
                        int copied = input.read(buffer);
                        if (copied == -1) {
                            break;
                        }
                        output2.write(buffer, 0, copied);
                    }
                    input.close();
                    output2.close();
                    }
                catch (IOException e)
                {
                }
            }
            catch (IOException e)
            {
            }
        }
        return Uri.fromFile(file);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0420\u0430\u0441\u0441\u043a\u0430\u0437\u0430\u0442\u044c \u043e \u043d\u0430\u0441");
        View view = inflater.inflate(R.layout.general_share_fragment, container, false);
        view.findViewById(R.id.ShareVk).setOnClickListener(this.m_func_share_vk);
        view.findViewById(R.id.ShareFb).setOnClickListener(this.m_func_share_fb);
        view.findViewById(R.id.ShareWa).setOnClickListener(this.m_func_share_wa);
        return view;
    }
}

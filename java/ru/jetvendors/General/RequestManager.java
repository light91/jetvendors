package ru.jetvendors.General;

import android.util.Pair;
import ru.jetvendors.ExternLibs.NotifyResultListener;
import ru.jetvendors.Application;
import ru.jetvendors.ExternLibs.BaseAsyncRequestManager;

public class RequestManager extends BaseAsyncRequestManager {
    private static final String host_api = "http://qazaqtv.kz/JetVendors/api.php";

    class ResponseObjectChangePassword extends ResponseObject {
        ResponseObjectChangePassword() {
            super();
        }

        public void doResponse(Request requestData) {
            NotifyResultListener notifyListener = (NotifyResultListener) requestData.getObject();
            if (data().equals("1")) {
                notifyListener.onSuccess();
            } else {
                notifyListener.onError();
            }
        }
    }

    class ResponseObjectFeedback extends ResponseObject {
        ResponseObjectFeedback() {
            super();
        }

        public void doResponse(Request requestData) {
            NotifyResultListener notifyListener = (NotifyResultListener) requestData.getObject();
            if (data().equals("1")) {
                notifyListener.onSuccess();
            } else {
                notifyListener.onError();
            }
        }
    }

    public void feedback(String text, NotifyResultListener notifyListener) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "feedback");
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        requestData.addData("text", text);
        requestData.setUrl(host_api);
        requestData.setObject(notifyListener);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectFeedback())});
        addRequest(currentRequest);
    }

    public void change_password(String password, NotifyResultListener notifyListener) {
        AsyncRequest currentRequest = new AsyncRequest();
        Request requestData = new Request(false);
        requestData.addData("action", "change_password");
        requestData.addData("sid", String.valueOf(Application.instance().getUserExtra().getSid()));
        requestData.addData("password", password);
        requestData.setUrl(host_api);
        requestData.setObject(notifyListener);
        currentRequest.execute(new Pair[]{new Pair(requestData, new ResponseObjectChangePassword())});
        addRequest(currentRequest);
    }
}

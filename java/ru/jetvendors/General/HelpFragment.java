package ru.jetvendors.General;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.R;

public class HelpFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u0421\u043f\u0440\u0430\u0432\u043a\u0430");
        return inflater.inflate(R.layout.general_help_fragment, container, false);
    }
}

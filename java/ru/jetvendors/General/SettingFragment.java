package ru.jetvendors.General;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import ru.jetvendors.Application;
import ru.jetvendors.BaseUserActivity;
import ru.jetvendors.R;
import ru.jetvendors.ExternLibs.NotifyResultListener;

public class SettingFragment extends Fragment {
    private OnClickListener m_func_on_send;
    private NotifyResultListener m_func_on_send_result;

    /* renamed from: ru.jetvendors.General.SettingFragment.1 */
    class C03131 implements OnClickListener {
        C03131() {
        }

        public void onClick(View v) {
            String password = ((EditText) SettingFragment.this.getView().findViewById(R.id.Password)).getText().toString();
            if (password.length() != 0) {
                new RequestManager().change_password(password, SettingFragment.this.m_func_on_send_result);
            } else {
                SettingFragment.this.m_func_on_send_result.onSuccess();
            }
        }
    }

    /* renamed from: ru.jetvendors.General.SettingFragment.2 */
    class C05442 implements NotifyResultListener {
        C05442() {
        }

        public void onSuccess() {
            Application.instance().getSettingManager().setShowNotifications(((Switch) SettingFragment.this.getView().findViewById(R.id.Notifications)).isChecked());
            Toast.makeText(SettingFragment.this.getView().getContext(), "\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438 \u0443\u0441\u043f\u0435\u0448\u043d\u043e \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u044b", 1).show();
        }

        public void onError() {
            Toast.makeText(SettingFragment.this.getView().getContext(), "\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", 1).show();
        }
    }

    public SettingFragment() {
        this.m_func_on_send = new C03131();
        this.m_func_on_send_result = new C05442();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((BaseUserActivity) getActivity()).setPageTitle("\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438");
        View view = inflater.inflate(R.layout.general_setting_fragment, container, false);
        ((Switch) view.findViewById(R.id.Notifications)).setChecked(Application.instance().getSettingManager().getShowNotifications());
        view.findViewById(R.id.Save).setOnClickListener(this.m_func_on_send);
        return view;
    }
}

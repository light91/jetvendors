package ru.jetvendors;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import ru.jetvendors.General.ContactFragment;
import ru.jetvendors.General.HelpFragment;
import ru.jetvendors.General.SettingFragment;
import ru.jetvendors.General.ShareFragment;
import ru.jetvendors.Messages.TalksFragment;
import ru.jetvendors.Notifications.NotificationsFragment;
import ru.jetvendors.Offers.ViewListFragment;
import ru.jetvendors.Profile.VendorEditFragment;
import ru.jetvendors.UserCategories.SelectCategoriesFragment;

public class VendorActivity extends BaseUserActivity
{
    private OnClickListener m_func_edit_profile = new OnClickListener()
    {
        public void onClick(View view)
        {
            VendorActivity.this.fragmentForward(new VendorEditFragment());
            ((DrawerLayout) VendorActivity.this.findViewById(R.id.DrawerLayout)).closeDrawers();
        }
    };
    private OnNavigationItemSelectedListener m_func_on_menu_select_item = new OnNavigationItemSelectedListener()
    {
        public boolean onNavigationItemSelected(MenuItem item)
        {
            Log.d("myLogs", "onNavigationItemSelected");
            switch (item.getItemId())
            {
                case R.id.Messages:
                    Log.d("myLogs", "Messages");
                    fragmentForward(new TalksFragment());
                    break;
                case R.id.Notifications:
                    fragmentForward(new NotificationsFragment());
                    break;
                case R.id.Categories:
                    fragmentForward(new SelectCategoriesFragment());
                    break;
                case R.id.Contact:
                    fragmentForward(new ContactFragment());
                    break;
                case R.id.Help:
                    fragmentForward(new HelpFragment());
                    break;
                case R.id.Setting:
                    fragmentForward(new SettingFragment());
                    break;
                case R.id.Share:
                    fragmentForward(new ShareFragment());
                    break;
                case R.id.Logout:
                    logout();
                    break;
                case R.id.OffersNew:
                    fragmentForward(new ViewListFragment());
                    break;
                default:
                    return false;
            }
            ((DrawerLayout) VendorActivity.this.findViewById(R.id.DrawerLayout)).closeDrawers();
            return true;
        }
    };

    protected OnNavigationItemSelectedListener getNavigationListener()
    {
        return m_func_on_menu_select_item;
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        initView(R.layout.activity_vendor);
        getNavigationHeaderView().findViewById(R.id.DrawerProfile).setOnClickListener(m_func_edit_profile);
        getNavigationListener().onNavigationItemSelected(((NavigationView) findViewById(R.id.NavigationView)).getMenu().findItem(R.id.Categories));
    }
}
